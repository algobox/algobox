/***************************************************************************
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301, USA.                                                  *
 ***************************************************************************/

#ifndef INPUT_VARIABLE_H
#define INPUT_VARIABLE_H

#include "codeitem.h"

class InputVariable : public CodeItem
{

public:

    explicit InputVariable(const QString &pName = QString(), CodeItem *pParent = 0) : CodeItem(pParent), mName(pName)
    {
    }

    const QString& name() const
    {
        return mName;
    }

    void setName(const QString &pName)
    {
        mName = pName;
    }

    QVariant data(int role) const;

    CodeItem* duplicate() const
    {
        return new InputVariable(*this);
    }

private:

    QString mName;
};

#endif
