/***************************************************************************
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301, USA.                                                  *
 ***************************************************************************/

#ifndef IF_ITEM_H
#define IF_ITEM_H

#include "codeitem.h"
#include "itemBlock.h"

class IfItem : public CodeItem
{

public:

    explicit IfItem(const QString &pCondition, CodeItem *pParent = 0) :
        CodeItem(pParent), mCondition(pCondition),
        mThenBody(CodeModelDefinitions::CodeBlockElement, this),
        mElseBody(CodeModelDefinitions::CodeBlockElement, this)
    {
    }

    const QString& condition() const
    {
        return mCondition;
    }

    void setCondition(const QString &pCondition)
    {
        mCondition = pCondition;
    }

    QVariant data(int role) const;

    CodeItem* duplicate() const
    {
        return new IfItem(*this);
    }

    int getChildIndex(const CodeItem * const child) const;

    const CodeItem* getNthChild(int n) const
    {
        if (n == 0) {
            return &mThenBody;
        } else {
            return &mElseBody;
        }
    }

    CodeItem* getNthChild(int n)
    {
        if (n == 0) {
            return &mThenBody;
        } else {
            return &mElseBody;
        }
    }

    int childCount() const
    {
        return 2;
    }

private:

    QString mCondition;

    ItemBlock mThenBody;

    ItemBlock mElseBody;

};

inline int IfItem::getChildIndex(const CodeItem *const child) const
{
    if (child == &mThenBody) {
        return 0;
    } else {
        if (child == &mElseBody) {
            return 1;
        } else {
            return -1;
        }
    }
}

#endif
