/***************************************************************************
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301, USA.                                                  *
 ***************************************************************************/

#include "itemBlock.h"
#include "codeModelGlobal.h"

ItemBlock::ItemBlock(CodeModelDefinitions::ElementTypeDefinitions pCurrentType, CodeItem *pParent) : CodeItem(pParent), mCodeListing(), mCurentType(pCurrentType)
{
}

void ItemBlock::insertCodeItem(const CodeItem* const codeItem, const CodeItem* const precedentCodeItem)
{
    QScopedPointer<CodeItem> newItem(codeItem->duplicate());
    newItem->setParent(this);
    for (auto itCodeItem = mCodeListing.begin(); itCodeItem != mCodeListing.end(); ++itCodeItem) {
        if (itCodeItem->data() == precedentCodeItem) {
            ++itCodeItem;
            if (itCodeItem != mCodeListing.end()) {
                mCodeListing.insert(itCodeItem, QSharedPointer<CodeItem>(newItem.take()));
            } else {
                mCodeListing.append(QSharedPointer<CodeItem>(newItem.take()));
            }
            return;
        }
    }
    mCodeListing.append(QSharedPointer<CodeItem>(newItem.take()));
}

QVariant ItemBlock::data(int role) const
{
    switch(role)
    {
    case CodeModelCustomRoles::ElementType:
        return mCurentType;
    case Qt::DisplayRole:
        return QString("BEGIN");
    };
    return QVariant();
}
