/***************************************************************************
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301, USA.                                                  *
 ***************************************************************************/

#ifndef CODEITEM_H
#define CODEITEM_H

#include <QVariant>

class CodeItem
{
    CodeItem *mParent;

public:

    explicit CodeItem(CodeItem *pParent = 0) : mParent(pParent)
    {
    }

    void setParent(CodeItem *pParent)
    {
        mParent = pParent;
    }

    const CodeItem* parent() const
    {
        return mParent;
    }

    CodeItem* parent()
    {
        return mParent;
    }

    virtual const CodeItem* getNthChild(int n) const;

    virtual CodeItem* getNthChild(int n);

    virtual int childCount() const;

    virtual int getChildIndex(const CodeItem * const child) const;

    virtual QVariant data(int role) const;

    virtual CodeItem* duplicate() const = 0;
};

inline const CodeItem* CodeItem::getNthChild(int n) const
{
    return 0;
}

inline CodeItem* CodeItem::getNthChild(int n)
{
    return 0;
}

inline int CodeItem::childCount() const
{
    return 0;
}

inline int CodeItem::getChildIndex(const CodeItem * const child) const
{
    return -1;
}

inline QVariant CodeItem::data(int role) const
{
    return QVariant();
}

#endif // CODEITEM_H
