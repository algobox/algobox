/***************************************************************************
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301, USA.                                                  *
 ***************************************************************************/

#ifndef ROOTITEM_H
#define ROOTITEM_H

#include "itemBlock.h"
#include "variableDeclaration.h"
#include "commentItem.h"

#include <QList>
#include <QSharedPointer>

class RootItem : CodeItem
{
    ItemBlock mVariableDeclarations;

    ItemBlock mCodeItems;

    CommentItem mEndCodeComment;

public:

    explicit RootItem();

    const ItemBlock& getVariableDeclarations() const
    {
        return mVariableDeclarations;
    }

    ItemBlock& getVariableDeclarations()
    {
        return mVariableDeclarations;
    }

    const ItemBlock& getCodeItems() const
    {
        return mCodeItems;
    }

    ItemBlock& getCodeItems()
    {
        return mCodeItems;
    }

    int getChildIndex(const CodeItem * const child) const;

    const CodeItem* getNthChild(int n) const;

    CodeItem* getNthChild(int n);

    int childCount() const
    {
        return 3;
    }

    QVariant data(int role) const;

    CodeItem* duplicate() const
    {
        return new RootItem(*this);
    }

};

inline const CodeItem* RootItem::getNthChild(int n) const
{
    switch (n)
    {
    case 0:
        return &mVariableDeclarations;
    case 1:
        return &mCodeItems;
    case 2:
        return &mEndCodeComment;
    default:
        return 0;
    }
}

inline CodeItem* RootItem::getNthChild(int n)
{
    switch (n)
    {
    case 0:
        return &mVariableDeclarations;
    case 1:
        return &mCodeItems;
    case 2:
        return &mEndCodeComment;
    default:
        return 0;
    }
}

inline int RootItem::getChildIndex(const CodeItem * const child) const
{
    if (child == &mVariableDeclarations) {
        return 0;
    } else {
        if (child == &mCodeItems) {
            return 1;
        } else {
            if (child == &mEndCodeComment) {
                return 2;
            } else {
                return -1;
            }
        }
    }
}

#endif // ROOTITEM_H
