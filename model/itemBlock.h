/***************************************************************************
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301, USA.                                                  *
 ***************************************************************************/

#ifndef CODE_BLOCK_H
#define CODE_BLOCK_H

#include "codeitem.h"
#include "codeModelGlobal.h"

#include <QList>
#include <QSharedPointer>

class ItemBlock : public CodeItem
{

    QList<QSharedPointer<CodeItem> > mCodeListing;

public:

    explicit ItemBlock(CodeModelDefinitions::ElementTypeDefinitions pCurrentType, CodeItem *pParent);

    int getChildIndex(const CodeItem * const child) const;

    const CodeItem* getNthChild(int n) const;

    CodeItem* getNthChild(int n);

    int childCount() const
    {
        return mCodeListing.count();
    }

    void insertCodeItem(const CodeItem* const codeItem, const CodeItem* const precedentCodeItem);

    QVariant data(int role) const;

    CodeItem* duplicate() const
    {
        return new ItemBlock(*this);
    }

private:

    CodeModelDefinitions::ElementTypeDefinitions mCurentType;

};

inline const CodeItem* ItemBlock::getNthChild(int n) const
{
    if (n > -1 && n < mCodeListing.count()) {
        return mCodeListing.at(n).data();
    } else {
        return 0;
    }
}

inline CodeItem* ItemBlock::getNthChild(int n)
{
    if (n > -1 && n < mCodeListing.count()) {
        return mCodeListing.at(n).data();
    } else {
        return 0;
    }
}

inline int ItemBlock::getChildIndex(const CodeItem *const child) const
{
    int childCounter = 1;
    for (auto itCodeItem = mCodeListing.begin(); itCodeItem != mCodeListing.end(); ++childCounter, ++itCodeItem) {
        if (itCodeItem->data() == child) {
            return childCounter;
        }
    }
    return -1;
}

#endif
