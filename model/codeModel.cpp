/***************************************************************************
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301, USA.                                                  *
 ***************************************************************************/

#include "codeModel.h"

#include "variableDeclaration.h"
#include "rootItem.h"
#include "itemBlock.h"
#include "inputVariable.h"
#include "printVariable.h"
#include "printText.h"
#include "commentItem.h"
#include "sleepItem.h"

#include <QDebug>

CodeModel::CodeModel()
{
    mRootItem = new RootItem;
}

int CodeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 1;
    } else {
        return 1;
    }
}

QVariant CodeModel::data(const QModelIndex &index, int role) const
{
    if (index.isValid()) {
        return reinterpret_cast<CodeItem*>(index.internalPointer())->data(role);
    } else {
        return QVariant();
    }
}

QModelIndex CodeModel::index(int row, int column, const QModelIndex &parent) const
{
    qDebug() << "CodeModel::index" << row << column << parent;
    if (parent.isValid() && column == 0) {
        if (parent.internalPointer()) {
            CodeItem * const childItem = reinterpret_cast<CodeItem*>(parent.internalPointer())->getNthChild(row);
            if (childItem) {
                return createIndex(row, column, childItem);
            } else {
                return QModelIndex();
            }
        } else {
            return QModelIndex();
        }
    } else {
        if (!parent.isValid()) {
          return createIndex(0, 0, mRootItem);
        } else {
          return QModelIndex();
        }
    }
}

QModelIndex CodeModel::parent(const QModelIndex &index) const
{
    if (index.isValid()) {
        CodeItem *const parentPointer = reinterpret_cast<CodeItem*>(index.internalPointer())->parent();
        if (parentPointer) {
            if (parentPointer->parent()) {
                const int parentIndex = parentPointer->parent()->getChildIndex(parentPointer);
                if (parentIndex >= 0) {
                    return createIndex(parentPointer->parent()->getChildIndex(parentPointer), 0, parentPointer);
                } else {
                    return QModelIndex();
                }
            } else {
                return createIndex(0, 0, mRootItem);
            }
        } else {
            return QModelIndex();
        }
    } else {
        return QModelIndex();
    }
}

int CodeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return reinterpret_cast<CodeItem*>(parent.internalPointer())->childCount();
    } else {
        return 1;
    }
}

void CodeModel::addVariableDeclaration(const QModelIndex &precedentItem, const QString &pName, CodeModelDefinitions::VariableType pType)
{
    if (precedentItem.isValid() && (precedentItem.data(CodeModelCustomRoles::ElementType) == CodeModelDefinitions::VariableDeclarationElement)) {
        int newRowIndex = precedentItem.row() + 1;
        beginInsertRows(createIndex(0, 0, &mRootItem->getVariableDeclarations()), newRowIndex, newRowIndex);
        VariableDeclaration newVar;
        newVar.setName(pName);
        newVar.setType(pType);
        mRootItem->getVariableDeclarations().insertCodeItem(&newVar, reinterpret_cast<const VariableDeclaration*>(precedentItem.internalPointer()));
        endInsertRows();
    } else {
        int newRowIndex = mRootItem->getVariableDeclarations().childCount();
        beginInsertRows(createIndex(0, 0, &mRootItem->getVariableDeclarations()), newRowIndex, newRowIndex);
        VariableDeclaration newVar;
        newVar.setName(pName);
        newVar.setType(pType);
        mRootItem->getVariableDeclarations().insertCodeItem(&newVar, 0);
        endInsertRows();
    }
}

void CodeModel::insertCodeItem(const QModelIndex &precedentItem, CodeItem *newItem)
{
    if (precedentItem.isValid()) {
        if (precedentItem.parent().data(CodeModelCustomRoles::ElementType) == CodeModelDefinitions::CodeBlockElement) {
            ItemBlock *theBlock = reinterpret_cast<ItemBlock*>(precedentItem.parent().internalPointer());
            newItem->setParent(theBlock);
            int newRowIndex = precedentItem.row() + 1;
            beginInsertRows(precedentItem.parent(), newRowIndex, newRowIndex);
            theBlock->insertCodeItem(newItem, reinterpret_cast<CodeItem*>(precedentItem.internalPointer()));
            endInsertRows();
        } else {
            if (precedentItem.parent().data(CodeModelCustomRoles::ElementType) == CodeModelDefinitions::RootItemElement) {
                RootItem *theBlock = reinterpret_cast<RootItem*>(precedentItem.parent().internalPointer());
                newItem->setParent(&theBlock->getCodeItems());
                int newRowIndex = 0;
                beginInsertRows(createIndex(1, 0, &theBlock->getCodeItems()), newRowIndex, newRowIndex);
                theBlock->getCodeItems().insertCodeItem(newItem, reinterpret_cast<CodeItem*>(precedentItem.internalPointer()));
                endInsertRows();
            }
        }
    }
}

void CodeModel::addInputVariableValue(const QModelIndex &precedentItem, const QString &name)
{
    insertCodeItem(precedentItem, new InputVariable(name, 0));
}

void CodeModel::addPrintVariable(const QModelIndex &precedentItem, const QString &name)
{
    insertCodeItem(precedentItem, new PrintVariable(name, 0));
}

void CodeModel::addPrintMessage(const QModelIndex &precedentItem, const QString &message)
{
    insertCodeItem(precedentItem, new PrintText(message, 0));
}

void CodeModel::addIf(const QModelIndex &precedentItem)
{
}

void CodeModel::addFor(const QModelIndex &precedentItem)
{
}

void CodeModel::addWhile(const QModelIndex &precedentItem)
{
}

void CodeModel::addSleep(const QModelIndex &precedentItem, int pDuration)
{
    insertCodeItem(precedentItem, new SleepItem(pDuration, 0));
}

void CodeModel::addComment(const QModelIndex &precedentItem, const QString &commentMessage)
{
    insertCodeItem(precedentItem, new CommentItem(commentMessage, false, 0));
}

bool CodeModel::variablesExists() const
{
    return true;
}

QStringList CodeModel::getAllVariablesName() const
{
    QStringList res;

    for (int i = 0; i < mRootItem->getVariableDeclarations().childCount(); ++i) {
        res += dynamic_cast<VariableDeclaration*>(mRootItem->getVariableDeclarations().getNthChild(i))->name();
    }

    return res;
}

bool CodeModel::checkVariableExists(const QString &pVariableName) const
{
    return true;
}

int CodeModel::getVariableType(const QString &pVariableName) const
{
    return 0;
}
