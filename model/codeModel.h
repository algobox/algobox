/***************************************************************************
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301, USA.                                                  *
 ***************************************************************************/

#ifndef CODEMODEL_H
#define CODEMODEL_H

#include "rootItem.h"
#include "codeModelGlobal.h"

#include <QAbstractItemModel>
#include <QStringList>

class CodeModel : public QAbstractItemModel
{
    Q_OBJECT

public:

    CodeModel();

    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;

    QModelIndex parent(const QModelIndex &index) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    bool variablesExists() const;

    QStringList getAllVariablesName() const;

    bool checkVariableExists(const QString&) const;

    int getVariableType(const QString&) const;

public slots:

    void addVariableDeclaration(const QModelIndex &precedentItem, const QString &name, CodeModelDefinitions::VariableType);

    void addInputVariableValue(const QModelIndex &precedentItem, const QString &name);

    void addPrintVariable(const QModelIndex &precedentItem, const QString &name);

    void addPrintMessage(const QModelIndex &precedentItem, const QString &message);

    void addIf(const QModelIndex &precedentItem);

    void addFor(const QModelIndex &precedentItem);

    void addWhile(const QModelIndex &precedentItem);

    void addSleep(const QModelIndex &precedentItem, int pDuration);

    void addComment(const QModelIndex &precedentItem, const QString &commentMessage);

    void insertCodeItem(const QModelIndex &precedentItem, CodeItem*);

private:

    RootItem *mRootItem;

};

#endif // CODEMODEL_H
