/***************************************************************************
 *   copyright       : (C) 2009-2011 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "modifierlignedialog.h"

ModifierLigneDialog::ModifierLigneDialog(QWidget *parent)
    :QDialog( parent)
{
ui.setupUi(this);
setModal(true);
}

ModifierLigneDialog::~ModifierLigneDialog(){
}
