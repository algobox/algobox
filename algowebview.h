/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ALGOWEBVIEW_H
#define ALGOWEBVIEW_H

#include <QWebView>

class AlgoWebView : public QWebView
{
    Q_OBJECT
 
public:
    AlgoWebView(QWidget *parent = 0);
    ~AlgoWebView();
protected:
    void contextMenuEvent(QContextMenuEvent *event);
public slots:
void imprimer();
void exporterPdf();
};


#endif
