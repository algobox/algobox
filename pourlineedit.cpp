/***************************************************************************
 *   copyright       : (C) 2009-2011 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "pourlineedit.h"
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QDebug>
#include <QMimeData>

PourLineEdit::PourLineEdit(QWidget *parent)
 : QLineEdit(parent)
{}

PourLineEdit::~PourLineEdit()
{}

void PourLineEdit::dragEnterEvent(QDragEnterEvent *event)
{
if (event->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist")) event->acceptProposedAction();
}

void PourLineEdit::dropEvent(QDropEvent *event)
{
QByteArray encoded = event->mimeData()->data("application/x-qabstractitemmodeldatalist");
QDataStream stream(&encoded, QIODevice::ReadOnly);
QMap<int,  QVariant> roleDataMap;
while (!stream.atEnd())
{
    int row, col;
    stream >> row >> col >> roleDataMap;
}

QString role="";
role=roleDataMap.value(32).toString();
QString code="";
code=roleDataMap.value(0).toString();
if (!role.isEmpty())
  {
  QStringList tagList= role.split("#");
  int pos=cursorPosition();
  int dx=tagList.at(1).toInt();
  insert(tagList.at(0));
  setCursorPosition(pos+dx);
  }
else if (!code.isEmpty()) insert(code);
setFocus();
event->acceptProposedAction();
}
