/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TANTQUEDIALOG_H
#define TANTQUEDIALOG_H

#include "ui_tantquedialog.h"
#include <QListWidget>
#include <QListWidgetItem>

class TantqueDialog : public QDialog  {
   Q_OBJECT
public:
	TantqueDialog(QWidget *parent=0,QString variables="", QString types="");
	~TantqueDialog();
	Ui::TantqueDialog ui;
private :
QStringList listeVariables, listeTypes, listeVarNombre, listeVarListe, listeVarChaine;
private slots:
void InsertVariable(QListWidgetItem *item);
void InsertOperation(QListWidgetItem *item);
};


#endif
