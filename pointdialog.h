/***************************************************************************
 *   copyright       : (C) 2009-2011 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef POINTDIALOG_H
#define POINTDIALOG_H

#include "ui_pointdialog.h"

class PointDialog : public QDialog  {
   Q_OBJECT
public:
	PointDialog(QWidget *parent=0,QString variables="", QString types="");
	~PointDialog();
	Ui::PointDialog ui;
private :
QStringList listeVariables, listeTypes, listeVarNombre, listeVarListe, listeVarChaine;
};


#endif
