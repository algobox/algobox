/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LIREDIALOG_H
#define LIREDIALOG_H

#include "ui_liredialog.h"

class LireDialog : public QDialog  {
   Q_OBJECT
public:
	LireDialog(QWidget *parent=0,QString variables="", QString types="");
	~LireDialog();
	Ui::LireDialog ui;
private :
QStringList listeVariables, listeTypes;
private slots:
void accept();
void ActualiserWidget(int index);
};
#endif
