/***************************************************************************
 *   copyright       : (C) 2009-2011 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef POURLINEEDIT_H
#define POURLINEEDIT_H

#include <QLineEdit>

class PourLineEdit : public QLineEdit
{
  Q_OBJECT

public:
  PourLineEdit(QWidget *parent = 0);
  ~PourLineEdit();
protected:
void dragEnterEvent(QDragEnterEvent *event);
void dropEvent(QDropEvent *event);
};
#endif