/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "variabledialog.h"

VariableDialog::VariableDialog(QWidget *parent)
    :QDialog( parent)
{
ui.setupUi(this);
setModal(true);
}

VariableDialog::~VariableDialog(){
}
