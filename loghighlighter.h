/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef LOGHIGHLIGHTER_H
#define LOGHIGHLIGHTER_H

#include <QSyntaxHighlighter>

#include <QHash>
#include <QTextCharFormat>
#include <QColor>

class QTextDocument;

class LogHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    LogHighlighter(QTextDocument *parent = 0, bool blackschema=true);
    QTextCharFormat MessageFormat, PasAPasFormat, ConditionFormat;

protected:
    void highlightBlock(const QString &text);
};


#endif
