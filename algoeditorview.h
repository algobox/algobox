/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ALGOEDITORVIEW_H
#define ALGOEDITORVIEW_H

#include <QWidget>
#include <QFont>
#include <QColor>
#include <QPointer>
#include "algoeditor.h"
#include "linenumberwidget.h"
#include "findwidget.h"
#include "replacedialog.h"

class AlgoEditorView : public QWidget  {
   Q_OBJECT
public: 
AlgoEditorView(QWidget *parent);
~AlgoEditorView();
AlgoEditor *editor;
FindWidget *findwidget;
void setFontSize(int size);
private:
LineNumberWidget* m_lineNumberWidget;
QPointer<ReplaceDialog> replaceDialog;
QFont efont;
private slots:
void setLineNumberWidgetVisible( bool );
void editFind();
void editReplace();
};

#endif
