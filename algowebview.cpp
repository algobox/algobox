/***************************************************************************
 *   copyright       : (C) 2006-2009 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "algowebview.h"

#include <QPrintDialog>
#include <QPrinter>
#include <QContextMenuEvent>
#include <QMenu>
#include <QAction>
#include <QWebFrame>
#include <QFileDialog>
#include <QDir>
#include <QDebug>

AlgoWebView::AlgoWebView(QWidget *parent)
    : QWebView(parent)
{

}
AlgoWebView::~AlgoWebView()
{

}
void AlgoWebView::contextMenuEvent(QContextMenuEvent *event)
{
QMenu *menu = new QMenu(this);
menu->addAction(QString::fromUtf8("Imprimer"), this, SLOT(imprimer()));  
menu->addAction(QString::fromUtf8("Exporter en Pdf"), this, SLOT(exporterPdf()));  
menu->exec(mapToGlobal(event->pos()));
delete menu;
}

void AlgoWebView::imprimer()
{
QPrinter printer;
QPrintDialog *dlg = new QPrintDialog(&printer,this);
if (dlg->exec() != QDialog::Accepted) return;
page()->mainFrame()->print(&printer);
}

void AlgoWebView::exporterPdf()
{
QPrinter printer(QPrinter::HighResolution);
#if defined( Q_WS_MACX )
printer.setOutputFormat(QPrinter::NativeFormat);
#else
printer.setOutputFormat(QPrinter::PdfFormat);
#endif
printer.setPaperSize(QPrinter::A4);
QString rep=QDir::homePath();
QString fn = QFileDialog::getSaveFileName(this,QString::fromUtf8("Exporter en Pdf"),rep,QString::fromUtf8("Fichier pdf (*.pdf)"));
if (fn.isEmpty()) return;
if (!fn.contains('.')) fn += ".pdf";
printer.setOutputFileName(fn);
//QPrintDialog *dlg = new QPrintDialog(&printer,this);
//if (dlg->exec() != QDialog::Accepted) return;
page()->mainFrame()->print(&printer);
}
