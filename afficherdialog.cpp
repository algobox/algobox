/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "afficherdialog.h"

#include <QMessageBox>

AfficherDialog::AfficherDialog(QWidget *parent, QString variables, QString types )
    :QDialog( parent)
{
ui.setupUi(this);
setModal(true);
listeVariables=variables.split("#");
listeTypes=types.split("#");
ui.comboBoxAfficher->addItems(listeVariables);
ui.labelAfficher2->setEnabled(false);
ui.lineEditAfficher->setEnabled(false);
if (ui.comboBoxAfficher->count()>0) ActualiserWidget(ui.comboBoxAfficher->currentIndex());
connect(ui.comboBoxAfficher, SIGNAL(activated(int)),this,SLOT(ActualiserWidget(int)));
}

AfficherDialog::~AfficherDialog(){
}

void AfficherDialog::accept()
{
if ( ui.lineEditAfficher->isEnabled() && ui.lineEditAfficher->text().isEmpty()) 
	{
	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Rectifiez le rang du terme de la liste : il est vide"));
	return;
	}
QDialog::accept();
}

void AfficherDialog::ActualiserWidget(int index)
{
if (listeTypes.at(index)=="LISTE")
    {
    ui.lineEditAfficher->clear();
    ui.labelAfficher2->setEnabled(true);
    ui.lineEditAfficher->setEnabled(true);
    }
else
    {
    ui.lineEditAfficher->clear();
    ui.labelAfficher2->setEnabled(false);
    ui.lineEditAfficher->setEnabled(false);
    }
}