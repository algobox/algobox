/***************************************************************************
 *   copyright       : (C) 2009-2011 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONSOLEDIALOG_H
#define CONSOLEDIALOG_H

#include "ui_consoledialog.h"

class ConsoleDialog : public QDialog  {
   Q_OBJECT
public:
	ConsoleDialog(QWidget *parent=0);
	~ConsoleDialog();
	Ui::ConsoleDialog ui;
};


#endif
