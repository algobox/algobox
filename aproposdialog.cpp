/***************************************************************************
 *   copyright       : (C) 2006-2009 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "aproposdialog.h"

#include <QFile>
#include <QTextStream>
#include <QTextCodec>

AproposDialog::AproposDialog(QWidget *parent)
    :QDialog( parent)
{
ui.setupUi(this);
setModal(true);
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString contenu;
QFile apropos(":/documents/apropos.txt");
apropos.open(QIODevice::ReadOnly);
QTextStream in(&apropos);
in.setCodec(codec);
while (!in.atEnd()) 
	{
	contenu+= in.readLine()+"\n";
	}
apropos.close();
ui.textBrowser->setOpenExternalLinks(true);
ui.textBrowser->setHtml(contenu);
}

AproposDialog::~AproposDialog(){
}
