/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FINDWIDGET_H
#define FINDWIDGET_H

#include "ui_findwidget.h"
#include "algoeditor.h"

class FindWidget : public QWidget
{ 
    Q_OBJECT

public:
    FindWidget(QWidget* parent = 0);
    ~FindWidget();
    Ui::FindWidget ui;
public slots:
    virtual void doFind();
    void SetEditor(AlgoEditor *ed);
    void doHide();

protected:
    AlgoEditor *editor;
};

#endif
