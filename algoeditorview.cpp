/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "algoeditorview.h"
#include <QVBoxLayout>
#include <QFrame>
#include <QTextDocument>
#include <QTextCursor>
#include <QTextEdit>
#include <QTextBlock>
#include <QFontDatabase>

AlgoEditorView::AlgoEditorView(QWidget *parent) : QWidget(parent)
{
QFontDatabase fdb;
QStringList xf = fdb.families();
QString deft;
if (xf.contains("Liberation Mono",Qt::CaseInsensitive)) deft="Liberation Mono";
else if (xf.contains("DejaVu Sans Mono",Qt::CaseInsensitive)) deft="DejaVu Sans Mono";
#if defined( Q_WS_MACX )
else if (xf.contains("Courier",Qt::CaseInsensitive)) deft="Courier";
#endif
#if defined(Q_WS_WIN)
else if (xf.contains("Courier New",Qt::CaseInsensitive)) deft="Courier New";
#endif
else deft=qApp->font().family();  
efont.setFamily(deft);
//efont.setPointSize(qApp->font().pointSize());
//QFont efont(deft,qApp->font().pointSize());

QVBoxLayout* mainlay = new QVBoxLayout( this );
mainlay->setSpacing(0);
mainlay->setMargin(0);

QFrame *frame=new QFrame(this);
frame->setLineWidth(1);
frame->setFrameShape(QFrame::StyledPanel);
frame->setFrameShadow(QFrame::Sunken);
mainlay->addWidget(frame);

editor=new AlgoEditor(frame,efont);
connect(editor, SIGNAL(dofind()), this, SLOT(editFind()));
connect(editor, SIGNAL(doreplace()), this, SLOT(editReplace()));

m_lineNumberWidget = new LineNumberWidget( editor, frame);
// m_lineNumberWidget->setFont(efont);
// QFontMetrics fm( efont );
// m_lineNumberWidget->setFixedWidth( fm.width( "00000" ) + 14 );
QHBoxLayout* lay = new QHBoxLayout( frame );
lay->setSpacing(0);
lay->setMargin(0);
lay->addWidget( m_lineNumberWidget );
lay->addWidget( editor );
setFocusProxy( editor );
setLineNumberWidgetVisible(true);

findwidget=new FindWidget(this);
mainlay->addWidget(findwidget);
findwidget->SetEditor(editor);
findwidget->hide();
}

AlgoEditorView::~AlgoEditorView()
{
}

void AlgoEditorView::setLineNumberWidgetVisible( bool b )
{
    if( b ){
	m_lineNumberWidget->show();
    } else {
	m_lineNumberWidget->hide();
    }
}

void AlgoEditorView::editFind()
{
QTextCursor c =editor->textCursor();
if (c.hasSelection()) findwidget->ui.comboFind->lineEdit()->setText(c.selectedText());
findwidget->show();
findwidget->ui.comboFind->setFocus();
findwidget->ui.comboFind->lineEdit()->selectAll();
}

void AlgoEditorView::editReplace()
{
if ( !replaceDialog )  replaceDialog = new ReplaceDialog(this, 0);
replaceDialog->SetEditor(editor);
QTextCursor c =editor->textCursor();
if (c.hasSelection()) replaceDialog->ui.comboFind->lineEdit()->setText(c.selectedText());
replaceDialog->show();
replaceDialog->raise();
replaceDialog->ui.comboFind->setFocus();
replaceDialog->ui.comboFind->lineEdit()->selectAll();
}

void AlgoEditorView::setFontSize(int size)
{
efont.setPointSize(size);
m_lineNumberWidget->setFont(efont);
QFontMetrics fm( efont );
m_lineNumberWidget->setFixedWidth( fm.width( "00000" ) + 14 );
editor->setFont(efont);
}


