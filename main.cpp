/***************************************************************************
  *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QApplication>
#include <QTranslator>
#include <QFontDatabase>
#include <QDebug>

#include "algobox.h"

class AlgoBoxApp : public QApplication
{
private:
    MainWindow *mw;
protected:
    bool event(QEvent *event);
public:
    AlgoBoxApp( int & argc, char ** argv );
    ~AlgoBoxApp();
};

AlgoBoxApp::AlgoBoxApp( int & argc, char ** argv ) : QApplication ( argc, argv )
{
QTranslator* basicTranslator=new QTranslator(this);
#if defined( Q_WS_X11 )
#ifdef USB_VERSION
QString transdir=QCoreApplication::applicationDirPath()+"/ressources";
#else
QString transdir=PREFIX"/share/algobox";
#endif
#endif
#if defined( Q_WS_MACX )
QString transdir=QCoreApplication::applicationDirPath() + "/../Resources";
#endif
#if defined(Q_WS_WIN)
QString transdir=QCoreApplication::applicationDirPath()+"/ressources";
#endif
if (basicTranslator->load(QString("qt_fr"),transdir)) installTranslator(basicTranslator);
QFontDatabase::applicationFontFamilies(QFontDatabase::addApplicationFont(":/documents/LiberationMono-Regular.ttf"));
mw=new MainWindow();
mw->show();
//connect( this, SIGNAL( lastWindowClosed() ), this, SLOT( quit() ) );
for ( int i = 1; i < argc; ++i )
	{
	QString arg = argv[ i ];
	if ( arg[0] != '-' )    mw->OuvrirNouvelAlgo( arg );
	}
}

AlgoBoxApp::~AlgoBoxApp()
{
    delete mw;
}

bool AlgoBoxApp::event ( QEvent * event )
{
    if (event->type() == QEvent::FileOpen) {
        QFileOpenEvent *oe = static_cast<QFileOpenEvent *>(event);
        mw->OuvrirNouvelAlgo(oe->file());
    }
    return QApplication::event(event);
}

int main(int argc, char *argv[])
{
AlgoBoxApp a( argc, argv );
return a.exec();
}
