/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VERIFDIALOG_H
#define VERIFDIALOG_H

#include "ui_verifdialog.h"

class VerifDialog : public QDialog  {
   Q_OBJECT
public:
	VerifDialog(QWidget *parent=0);
	~VerifDialog();
	Ui::VerifDialog ui;
public slots:
void ExpandBranche(QTreeWidgetItem *item);
};


#endif
