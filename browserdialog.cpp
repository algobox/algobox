/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "browserdialog.h"
#include <QUrl>
#include <QWebFrame>
#include <QTextCursor>
#include <QTimer>
#include <QDebug>
#include <QApplication>
#include <QTextCharFormat>
#include <QFontDatabase>
#include <QMouseEvent>
#include <QTextCodec>
#include <QTextStream>



BrowserDialog::BrowserDialog(QWidget *parent,QString fichier,bool blackconsole)
    :QDialog( parent)
{
ui.setupUi(this);
ui.testerButton->setEnabled(false);
ui.suivantButton->setEnabled(false);
ui.stopButton->setEnabled(false);
ui.textEditPas->hide();
ui.labelProcess->hide();
setModal(true);
QFontDatabase fdb;
QStringList xf = fdb.families();
QString deft;
QTextCharFormat output_format;
if (xf.contains("Liberation Mono",Qt::CaseInsensitive)) deft="Liberation Mono";
else if (xf.contains("DejaVu Sans Mono",Qt::CaseInsensitive)) deft="DejaVu Sans Mono";
#if defined( Q_WS_MACX )
else if (xf.contains("Courier",Qt::CaseInsensitive)) deft="Courier";
#endif
#if defined(Q_WS_WIN)
else if (xf.contains("Courier New",Qt::CaseInsensitive)) deft="Courier New";
#endif
else deft=qApp->font().family();
output_format.setFont(QFont(deft,qApp->font().pointSize()));
// output_format.setFontFamily(deft);
// output_format.setFontFixedPitch(true);
// output_format.setFontItalic(false);
//output_format.setFontPointSize(qApp->font().pointSize()-1);
ui.textEdit->setCurrentCharFormat(output_format);
ui.textEditPas->setCurrentCharFormat(output_format);

QPalette palette=ui.textEdit->palette();
if (blackconsole)
{
palette.setColor(QPalette::Active, QPalette::Base, QColor(52,49,49));
palette.setColor(QPalette::Active, QPalette::Text, QColor(255,255,255));
palette.setColor(QPalette::Inactive, QPalette::Base, QColor(52,49,49));
palette.setColor(QPalette::Inactive, QPalette::Text, QColor(255,255,255));
}
else
{
palette.setColor(QPalette::Active, QPalette::Base, QColor(255,255,255));
palette.setColor(QPalette::Active, QPalette::Text, QColor(0,0,0));
palette.setColor(QPalette::Inactive, QPalette::Base, QColor(255,255,255));
palette.setColor(QPalette::Inactive, QPalette::Text, QColor(0,0,0));
} 

ui.textEdit->setPalette(palette);
ui.textEditPas->setPalette(palette);

highlighter = new LogHighlighter(ui.textEdit->document(),blackconsole);
highlighterPas = new LogHighlighter(ui.textEditPas->document(),blackconsole);

page=new AlgoWebPage(ui.webView);
ui.webView->setPage(page);
page->mainFrame()->load(QUrl(fichier));
connect(page->mainFrame(), SIGNAL(javaScriptWindowObjectCleared()),this, SLOT(populateJavaScriptWindowObject()));
connect(ui.testerButton, SIGNAL(clicked()),this, SLOT(LancerAlgo()));
connect(ui.suivantButton, SIGNAL(clicked()),this, SLOT(ContinuerPasAPas()));
connect(ui.stopButton, SIGNAL(clicked()),this, SLOT(StopperPasAPas()));
connect(ui.imprimerButton, SIGNAL(clicked()),ui.webView, SLOT(imprimer()));
connect(ui.pdfButton, SIGNAL(clicked()),ui.webView, SLOT(exporterPdf()));
ui.imprimerButton->setEnabled(false);
ui.pdfButton->setEnabled(false);
}

BrowserDialog::~BrowserDialog(){
page->arreter();
page->stopaffichage();
}

void BrowserDialog::accept()
{
page->arreter();
page->stopaffichage();
QDialog::accept();
}

void BrowserDialog::closeEvent(QCloseEvent *e)
{
page->arreter();
page->stopaffichage();
e->accept();
}

void BrowserDialog::populateJavaScriptWindowObject()
{
ui.textEdit->clear();
ui.textEditPas->clear();
ui.testerButton->setEnabled(true);
page->mainFrame()->addToJavaScriptWindowObject("browserDialog", this);
}

void BrowserDialog::scriptAfficher(QString msg,bool newline)
{
if (!msg.isEmpty())
  {
  QTextCursor cur=ui.textEdit->textCursor();
  if (newline) 
    {
    ui.textEdit->insertPlainText(msg+"\n");
    }
  else 
    {
    ui.textEdit->insertPlainText(msg);
    }
  cur.movePosition(QTextCursor::End,QTextCursor::MoveAnchor);
  ui.textEdit->setTextCursor(cur);
  }
page->minipause();
}

void BrowserDialog::scriptAfficherVariables(QString msg,bool newline)
{
if (msg.isEmpty()) return;
QTextCursor cur=ui.textEditPas->textCursor();
if (newline) 
  {
  ui.textEditPas->insertPlainText(msg+"\n");
  }
else 
  {
  ui.textEditPas->insertPlainText(msg);
  }
cur.movePosition(QTextCursor::End,QTextCursor::MoveAnchor);
ui.textEditPas->setTextCursor(cur);
}

void BrowserDialog::scriptLaunched()
{
// if (ui.checkBoxPasAPas->isChecked())
//   {
//   ui.suivantButton->setEnabled(true);
//   ui.stopButton->setEnabled(true);
//   }
ui.checkBoxPasAPas->setEnabled(false);
//ui.testerButton->setText(QString::fromUtf8("Exécution en cours"));
ui.testerButton->setEnabled(false);
ui.imprimerButton->setEnabled(false);
ui.pdfButton->setEnabled(false);
ui.labelProcess->show();
}

void BrowserDialog::scriptFinished()
{
QString output=ui.textEdit->toPlainText();
QString ligne;
QVariant data;
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QTextStream l(&output,QIODevice::ReadOnly);
l.setCodec(codec);
while (!l.atEnd()) 
	{
	ligne=l.readLine();
	data=page->mainFrame()->evaluateJavaScript("ALGOBOX_AJOUTE_RESULTAT(\""+ligne.trimmed()+"\");");;
	}
ui.labelProcess->hide();
ui.testerButton->setText(QString::fromUtf8("Lancer Algorithme"));
ui.testerButton->setEnabled(true);
ui.checkBoxPasAPas->setEnabled(true);
ui.suivantButton->setEnabled(false);
ui.stopButton->setEnabled(false);
ui.webView->setFocus();
QPoint pos(0,0);
QMouseEvent event(QEvent::MouseButtonPress,pos,Qt::LeftButton, Qt::LeftButton,Qt::NoModifier);
QApplication::sendEvent(ui.webView, &event);
ui.imprimerButton->setEnabled(true);
ui.pdfButton->setEnabled(true);
}

void BrowserDialog::scriptEffacer()
{
ui.textEdit->clear();
ui.textEditPas->clear();
}

void BrowserDialog::LancerAlgo()
{
ui.textEdit->clear();
ui.textEditPas->clear();
if (ui.checkBoxPasAPas->isChecked()) ui.textEditPas->show();
else ui.textEditPas->hide();
ui.stopButton->setEnabled(true);
QTimer::singleShot(100, this, SLOT(ExecuterAlgo()));
}

void BrowserDialog::ExecuterAlgo()
{
ui.textEdit->insertPlainText(QString::fromUtf8("***L'algorithme contient une erreur : impossible de le lancer***\n***Vérifiez la syntaxe des affectations et des conditions***\n"));  
QVariant data;
if (ui.checkBoxPasAPas->isChecked()) data=page->mainFrame()->evaluateJavaScript("ALGOBOX_ALGO(true);");
else data=page->mainFrame()->evaluateJavaScript("ALGOBOX_ALGO(false);");
}

void BrowserDialog::ContinuerPasAPas()
{
ui.labelProcess->show();
page->continuer();
}

void BrowserDialog::StopperPasAPas()
{
ui.labelProcess->hide();
QVariant data;
if (ui.suivantButton->isEnabled()) page->arreter();
else data=page->mainFrame()->evaluateJavaScript("ALGOBOX_EMERGENCY_STOP=true;");
}

void BrowserDialog::scriptDebutPause()
{
ui.suivantButton->setEnabled(true);
ui.labelProcess->hide();
//ui.stopButton->setEnabled(true);
}

void BrowserDialog::scriptFinPause()
{
ui.suivantButton->setEnabled(false);
ui.labelProcess->show();
//ui.stopButton->setEnabled(false);
}
