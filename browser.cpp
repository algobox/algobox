/***************************************************************************
 *   copyright       : (C) 2003-2011 by Pascal Brachet                     *
 *   http://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
#include "browser.h"

#include <QtGui>
#include <QPrinter>


Browser::Browser( const QString home, QWidget* parent, Qt::WFlags flags)
    : QMainWindow( parent, flags )
{
setWindowTitle("AlgoBox");
#ifdef Q_WS_MACX
setWindowIcon(QIcon(":/images/algobox128.png"));
#else
setWindowIcon(QIcon(":/images/algobox22.png"));
#endif
setIconSize(QSize(22,22 ));
progress = 0;
view = new QWebView(this);
if ( !home.isEmpty()) view->load(QUrl(home));
connect(view, SIGNAL(titleChanged(QString)), SLOT(adjustTitle()));
connect(view, SIGNAL(loadProgress(int)), SLOT(setProgress(int)));
connect(view, SIGNAL(loadFinished(bool)), SLOT(finishLoading(bool)));

QMenu *fileMenu = menuBar()->addMenu(QString::fromUtf8("&Fichier"));
fileMenu->addAction(QString::fromUtf8("&Imprimer"), this, SLOT(Print()));
fileMenu->addSeparator();
fileMenu->addAction(QString::fromUtf8("&Quitter"), this, SLOT(close()));

QToolBar *toolBar = addToolBar("Navigation");
QAction *Act;
Act = new QAction(QIcon(":/images/home.png"), QString::fromUtf8("Sommaire"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(Sommaire()));
toolBar->addAction(Act);
toolBar->addAction(view->pageAction(QWebPage::Back));
toolBar->addAction(view->pageAction(QWebPage::Forward));

QWidget* spacer = new QWidget();
spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
toolBar->addWidget(spacer);
searchLineEdit = new QLineEdit(toolBar);
connect(searchLineEdit, SIGNAL(returnPressed()), this, SLOT(Chercher()));
toolBar->addWidget(searchLineEdit);

findButton=new QPushButton(QString::fromUtf8("Chercher"),toolBar);
connect(findButton, SIGNAL(clicked()), this, SLOT(Chercher()));
toolBar->addWidget(findButton);

setCentralWidget(view);
setUnifiedTitleAndToolBarOnMac(true);
    
//resize(780,580 );
}

Browser::~Browser()
{

}

void Browser::adjustTitle()
{
if (progress <= 0 || progress >= 100)
    setWindowTitle(view->title());
else
    setWindowTitle(QString("%1 (%2%)").arg(view->title()).arg(progress));
}

void Browser::setProgress(int p)
{
progress = p;
adjustTitle();
}

void Browser::finishLoading(bool)
{
progress = 100;
adjustTitle();
}

void Browser::Sommaire()
{
view->page()->mainFrame()->evaluateJavaScript("window.location.href='#SOMMAIRE';");
}

void Browser::Print()
{
QPrinter printer;
QPrintDialog *dialog = new QPrintDialog(&printer, this);
dialog->setWindowTitle(QString::fromUtf8("Imprimer"));
if (dialog->exec() != QDialog::Accepted) return;
view->page()->mainFrame()->print(&printer);
}

void Browser::Chercher()
{
if (searchLineEdit->text().isEmpty()) return;
view->findText(searchLineEdit->text(),QWebPage::FindWrapsAroundDocument);
}
