/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ALGOWEBPAGE_H
#define ALGOWEBPAGE_H

#include <QWebPage>
#include <QObject>
#include <QEventLoop>
#include <QTimer>
#include <QDebug>

class AlgoWebPage : public QWebPage
{
Q_OBJECT
public:
          AlgoWebPage(QObject* parent =0);
	  ~AlgoWebPage();
protected:
   virtual void javaScriptAlert(QWebFrame *originatingFrame, const QString& msg);
   virtual bool javaScriptPrompt(QWebFrame *originatingFrame, const QString& msg, const QString& defaultValue, QString* result);
   virtual bool javaScriptConfirm(QWebFrame *originatingFrame, const QString& msg);
private:
bool result, stop;
QEventLoop loop_aff, loop_pause;
QTimer timer;
public slots:
void continuer();
void arreter();
void minipause();
void stopaffichage();
bool shouldInterruptJavaScript();
};

#endif
