/***************************************************************************
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301, USA.                                                  *
 ***************************************************************************/

#ifndef CODEMODELTEST_H
#define CODEMODELTEST_H

#include "../model/codeModel.h"

#include <QTest>
#include <QObject>

class CodeModelTest : public QObject
{
    Q_OBJECT

public:

    CodeModelTest();

private slots:

    void testAddVariableDeclarationFirstInsert();

    void testAddVariableDeclarationWrongIndexAlone();

    void testAddVariableDeclarationWrongIndexNotAlone();

    void testAddVariableDeclarationAfterAnotherVariable();

    void testAddVariableDeclarationTwoVariablesWithoutIndex();

    void testAddVariableDeclarationAfterFirstVariableAfterFirstComment();

    void testAddVariableDeclarationAfterFirstVariableAfterFirstCommentAfterFirstVariable();

    void testAddInputVariableValue();

    void testAddPrintVariable();

    void testAddPrintMessage();

    void testAddIf();

    void testAddFor();

    void testAddWhile();

    void testAddSleep();

    void testAddComment();

    void testParentRelationInModel();

};

#endif // CODEMODELTEST_H
