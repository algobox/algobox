/***************************************************************************
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301, USA.                                                  *
 ***************************************************************************/

#include "codeModelTest.h"

#include <QSignalSpy>
#include <QModelIndex>
#include <QDebug>

Q_DECLARE_METATYPE(QModelIndex)

CodeModelTest::CodeModelTest()
{
    qRegisterMetaType<QModelIndex>("QModelIndex");
}

void CodeModelTest::testAddVariableDeclarationFirstInsert()
{
    CodeModel testModel;

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addVariableDeclaration(QModelIndex(), "testVariable", CodeModelDefinitions::IntType);

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }
}

void CodeModelTest::testAddVariableDeclarationWrongIndexAlone()
{
    CodeModel testModel;

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addVariableDeclaration(testModel.index(2, 0, testModel.index(0, 0)), "testVariable", CodeModelDefinitions::IntType);

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }
}

void CodeModelTest::testAddVariableDeclarationWrongIndexNotAlone()
{
    CodeModel testModel;

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addVariableDeclaration(QModelIndex(), "testVariable1", CodeModelDefinitions::IntType);
    testModel.addVariableDeclaration(testModel.index(3, 0, testModel.index(0, 0, testModel.index(0, 0))), "testVariable2", CodeModelDefinitions::IntType);

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 2);
        QList<QVariant> argumentsFirst = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(argumentsFirst.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsFirst.at(1).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(1).toInt() == 0);
        QVERIFY(argumentsFirst.at(2).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(2).toInt() == 0);
        QList<QVariant> argumentsSecond = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(argumentsSecond.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsSecond.at(1).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(1).toInt() == 1);
        QVERIFY(argumentsSecond.at(2).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(2).toInt() == 1);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 2);
        QList<QVariant> argumentsFirst = rowsInsertedSpy.takeFirst();
        QVERIFY(argumentsFirst.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsFirst.at(1).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(1).toInt() == 0);
        QVERIFY(argumentsFirst.at(2).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(2).toInt() == 0);
        QList<QVariant> argumentsSecond = rowsInsertedSpy.takeFirst();
        QVERIFY(argumentsSecond.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsSecond.at(1).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(1).toInt() == 1);
        QVERIFY(argumentsSecond.at(2).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(2).toInt() == 1);
    }

    QVERIFY(testModel.index(0, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable1"));
    QVERIFY(testModel.index(1, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable2"));
}

void CodeModelTest::testAddVariableDeclarationAfterAnotherVariable()
{
    CodeModel testModel;

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addVariableDeclaration(QModelIndex(), "testVariable1", CodeModelDefinitions::IntType);
    testModel.addVariableDeclaration(QModelIndex(), "testVariable2", CodeModelDefinitions::IntType);
    testModel.addVariableDeclaration(testModel.index(0, 0, testModel.index(0, 0, testModel.index(0, 0))), "testVariable3", CodeModelDefinitions::IntType);

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 3);
        QList<QVariant> argumentsFirst = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(argumentsFirst.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsFirst.at(1).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(1).toInt() == 0);
        QVERIFY(argumentsFirst.at(2).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(2).toInt() == 0);
        QList<QVariant> argumentsSecond = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(argumentsSecond.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsSecond.at(1).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(1).toInt() == 1);
        QVERIFY(argumentsSecond.at(2).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(2).toInt() == 1);
        QList<QVariant> argumentsThird = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(argumentsThird.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsThird.at(1).type() == QVariant::Int);
        QVERIFY(argumentsThird.at(1).toInt() == 1);
        QVERIFY(argumentsThird.at(2).type() == QVariant::Int);
        QVERIFY(argumentsThird.at(2).toInt() == 1);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 3);
        QList<QVariant> argumentsFirst = rowsInsertedSpy.takeFirst();
        QVERIFY(argumentsFirst.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsFirst.at(1).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(1).toInt() == 0);
        QVERIFY(argumentsFirst.at(2).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(2).toInt() == 0);
        QList<QVariant> argumentsSecond = rowsInsertedSpy.takeFirst();
        QVERIFY(argumentsSecond.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsSecond.at(1).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(1).toInt() == 1);
        QVERIFY(argumentsSecond.at(2).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(2).toInt() == 1);
        QList<QVariant> argumentsThird = rowsInsertedSpy.takeFirst();
        QVERIFY(argumentsThird.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsThird.at(1).type() == QVariant::Int);
        QVERIFY(argumentsThird.at(1).toInt() == 1);
        QVERIFY(argumentsThird.at(2).type() == QVariant::Int);
        QVERIFY(argumentsThird.at(2).toInt() == 1);
    }

    QVERIFY(testModel.index(0, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable1"));
    QVERIFY(testModel.index(1, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable3"));
    QVERIFY(testModel.index(2, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable2"));
}

void CodeModelTest::testAddVariableDeclarationTwoVariablesWithoutIndex()
{
    CodeModel testModel;

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addVariableDeclaration(QModelIndex(), "testVariable1", CodeModelDefinitions::IntType);
    testModel.addVariableDeclaration(QModelIndex(), "testVariable2", CodeModelDefinitions::IntType);

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 2);
        QList<QVariant> argumentsFirst = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(argumentsFirst.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsFirst.at(1).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(1).toInt() == 0);
        QVERIFY(argumentsFirst.at(2).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(2).toInt() == 0);
        QList<QVariant> argumentsSecond = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(argumentsSecond.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsSecond.at(1).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(1).toInt() == 1);
        QVERIFY(argumentsSecond.at(2).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(2).toInt() == 1);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 2);
        QList<QVariant> argumentsFirst = rowsInsertedSpy.takeFirst();
        QVERIFY(argumentsFirst.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsFirst.at(1).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(1).toInt() == 0);
        QVERIFY(argumentsFirst.at(2).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(2).toInt() == 0);
        QList<QVariant> argumentsSecond = rowsInsertedSpy.takeFirst();
        QVERIFY(argumentsSecond.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsSecond.at(1).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(1).toInt() == 1);
        QVERIFY(argumentsSecond.at(2).type() == QVariant::Int);
        QVERIFY(argumentsSecond.at(2).toInt() == 1);
    }

    QVERIFY(testModel.index(0, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable1"));
    QVERIFY(testModel.index(1, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable2"));
}

void CodeModelTest::testAddVariableDeclarationAfterFirstVariableAfterFirstComment()
{
    CodeModel testModel;

    testModel.addVariableDeclaration(QModelIndex(), "testVariable1", CodeModelDefinitions::IntType);

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addVariableDeclaration(testModel.index(0, 0, testModel.index(0, 0, testModel.index(0, 0))), "testVariable2", CodeModelDefinitions::IntType);

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 1);
        QList<QVariant> argumentsFirst = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(argumentsFirst.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsFirst.at(1).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(1).toInt() == 1);
        QVERIFY(argumentsFirst.at(2).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(2).toInt() == 1);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 1);
        QList<QVariant> argumentsFirst = rowsInsertedSpy.takeFirst();
        QVERIFY(argumentsFirst.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsFirst.at(1).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(1).toInt() == 1);
        QVERIFY(argumentsFirst.at(2).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(2).toInt() == 1);
    }

    QVERIFY(testModel.index(0, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable1"));
    QVERIFY(testModel.index(1, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable2"));
}

void CodeModelTest::testAddVariableDeclarationAfterFirstVariableAfterFirstCommentAfterFirstVariable()
{
    CodeModel testModel;

    testModel.addVariableDeclaration(QModelIndex(), "testVariable1", CodeModelDefinitions::IntType);
    testModel.addVariableDeclaration(testModel.index(0, 0, testModel.index(0, 0, testModel.index(0, 0))), "testVariable2", CodeModelDefinitions::IntType);

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addVariableDeclaration(testModel.index(1, 0, testModel.index(0, 0, testModel.index(0, 0))), "testVariable3", CodeModelDefinitions::IntType);

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 1);
        QList<QVariant> argumentsFirst = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(argumentsFirst.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsFirst.at(1).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(1).toInt() == 2);
        QVERIFY(argumentsFirst.at(2).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(2).toInt() == 2);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 1);
        QList<QVariant> argumentsFirst = rowsInsertedSpy.takeFirst();
        QVERIFY(argumentsFirst.at(0).value<QModelIndex>() == testModel.index(0, 0, testModel.index(0, 0)));
        QVERIFY(argumentsFirst.at(1).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(1).toInt() == 2);
        QVERIFY(argumentsFirst.at(2).type() == QVariant::Int);
        QVERIFY(argumentsFirst.at(2).toInt() == 2);
    }
    qDebug() << testModel.index(2, 0, testModel.index(0, 0, testModel.index(0, 0))).data();
    QVERIFY(testModel.index(0, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable1"));
    QVERIFY(testModel.index(1, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable2"));
    QVERIFY(testModel.index(2, 0, testModel.index(0, 0, testModel.index(0, 0))).data().toString() == QString("testVariable3"));
}

void CodeModelTest::testAddInputVariableValue()
{
    CodeModel testModel;

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addInputVariableValue(testModel.index(1, 0, testModel.index(0, 0)), "testVariable");

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(1, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(1, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }
}

void CodeModelTest::testAddPrintVariable()
{
    CodeModel testModel;

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addPrintVariable(testModel.index(1, 0, testModel.index(0, 0)), "testVariable");

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(1, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(1, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }
}

void CodeModelTest::testAddPrintMessage()
{
    CodeModel testModel;

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addPrintMessage(testModel.index(1, 0, testModel.index(0, 0)), "testVariable");

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(1, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(1, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }
}

void CodeModelTest::testAddIf()
{
}

void CodeModelTest::testAddFor()
{
}

void CodeModelTest::testAddWhile()
{
}

void CodeModelTest::testAddSleep()
{
    CodeModel testModel;

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addSleep(testModel.index(1, 0, testModel.index(0, 0)), 10);

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(1, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(1, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }
}

void CodeModelTest::testAddComment()
{
    CodeModel testModel;

    QSignalSpy rowsAboutToBeInsertedSpy(&testModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)));
    QSignalSpy rowsInsertedSpy(&testModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)));

    testModel.addComment(testModel.index(1, 0, testModel.index(0, 0)), "testVariable");

    {
        QVERIFY(rowsAboutToBeInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsAboutToBeInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(1, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }

    {
        QVERIFY(rowsInsertedSpy.count() == 1);
        QList<QVariant> arguments = rowsInsertedSpy.takeFirst();
        QVERIFY(arguments.at(0).value<QModelIndex>() == testModel.index(1, 0, testModel.index(0, 0)));
        QVERIFY(arguments.at(1).type() == QVariant::Int);
        QVERIFY(arguments.at(1).toInt() == 0);
        QVERIFY(arguments.at(2).type() == QVariant::Int);
        QVERIFY(arguments.at(2).toInt() == 0);
    }
}

void CodeModelTest::testParentRelationInModel()
{
    CodeModel testModel;

    QVERIFY(testModel.index(0, 0).parent() == QModelIndex());
    QVERIFY(testModel.index(0, 0, testModel.index(0, 0)).parent() == testModel.index(0, 0));
    QVERIFY(testModel.index(1, 0, testModel.index(0, 0)).parent() == testModel.index(0, 0));
    QVERIFY(testModel.index(2, 0, testModel.index(0, 0)).parent() == testModel.index(0, 0));
}

QTEST_MAIN(CodeModelTest)
