/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef APROPOSDIALOG_H
#define APROPOSDIALOG_H

#include "ui_aproposdialog.h"

class AproposDialog : public QDialog  {
   Q_OBJECT
public:
	AproposDialog(QWidget *parent=0);
	~AproposDialog();
	Ui::AproposDialog ui;
};


#endif
