/***************************************************************************
 *   copyright       : (C) 2009-2011 by Pascal Brachet                     *
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "algobox.h"
#include "browserdialog.h"
#include "variabledialog.h"
#include "liredialog.h"
#include "afficherdialog.h"
#include "messagedialog.h"
#include "affectationdialog.h"
#include "conditiondialog.h"
#include "pourdialog.h"
#include "tantquedialog.h"
#include "pointdialog.h"
#include "segmentdialog.h"
#include "aproposdialog.h"
#include "commentairedialog.h"
#include "verifdialog.h"
#include "x11fontdialog.h"
#include "modifierlignedialog.h"
#include "consoledialog.h"
#include "model/codeModel.h"

#include <QSettings>
#include <QAction>
#include <QMessageBox>
#include <QApplication>
#include <QDir>
#include <QDesktopWidget>
#include <QStyleFactory>
#include <QStyle>
#include <QDomDocument>
#include <QDomElement>
#include <QDomProcessingInstruction>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QFontDatabase>
#include <QDebug>
#include <QTextStream>
#include <QTextCodec>
#include <QFont>
#include <QColor>
#include <QDesktopServices>
#include <QUrl>
#include <QStyleFactory>
#include <QStyle>
#include <QTextDocument>
#include <QPrintDialog>
#include <QPrinter>
#include <QDate>
#include <QTextDocumentWriter>
#include <QTextCharFormat>
#include <QAbstractItemModel>

MainWindow::MainWindow(QWidget *parent)
    :QMainWindow( parent)
{
ui.setupUi(this);
theModel = new CodeModel;
ui.treeView->setModel(theModel);
ui.treeView->setExpanded(theModel->index(0, 0), true);
theModel->addVariableDeclaration(QModelIndex(), "testVariable", CodeModelDefinitions::IntType);
ui.treeView->setRootIndex(QModelIndex());
#ifndef VERSION_MINIPC
ui.textEditTip->setPlainText(QString::fromUtf8("- Pour utiliser une variable, il faut d'abord la déclarer (bouton \"Déclarer nouvelle variable\").\n- Pour ajouter un nouvel élément à l'algorithme, il faut d'abord insérer une nouvelle ligne (bouton \"Nouvelle Ligne\"), puis cliquer sur un des boutons disponibles dans le panneau \"Ajouter code\"."));
ui.textEditTipEditeur->setPlainText(QString::fromUtf8("- La \"syntaxe AlgoBox\" doit être pleinement respectée sous peine de non-reconnaissance du code.\n- Une ligne ne doit comporter qu'une seule instruction.\n- Lors de l'utilisation de l'auto-complétion et des commandes du panneau inférieur, le symbole • représente un champ à compléter : pour passer au champ suivant, il suffit d'utiliser la touche Tab.\n- Un clic-droit permet d'accéder aux commandes du menu spécial \"éditeur\".\n- Le symbole * après l'instruction AFFICHER indique que l'affichage sera suivi d'un retour à la ligne."));
#else
ui.textEditTip->hide();
ui.textEditTipEditeur->hide();
#endif
LireConfig();
clipboardItem=new QTreeWidgetItem(QStringList(QString("")));
QFile file(":/documents/styleapplication.qss");
file.open(QFile::ReadOnly);
QString styleSheet = QLatin1String(file.readAll());
qApp->setStyleSheet(styleSheet);

QFont fontCommande=qApp->font();
fontCommande.setBold(true);
QFont fontCommentaire=qApp->font();
//fontCommentaire.setItalic(true);
#ifdef Q_WS_MACX
fontCommentaire.setPointSize(10);
#else
fontCommentaire.setPointSize(8);
#endif
QListWidgetItem *commande, *commentaire;
QStringList commandeList,commentaireList,roleList;

commandeList << "sqrt(x)";
commentaireList << QString::fromUtf8("-> racine carrée de x");
roleList << "sqrt()#5";

commandeList << "pow(x,n)";
commentaireList << QLatin1String("-> x puissance n");
roleList << "pow(,)#4";

commandeList << "x%y";
commentaireList << QLatin1String("-> reste de la division de x par y");
roleList << "%#0";

commandeList << "cos(x)";
commentaireList << QLatin1String("-> cosinus de x");
roleList << "cos()#4";

commandeList << "sin(x)";
commentaireList << QLatin1String("-> sinus de x");
roleList << "sin()#4";

commandeList << "tan(x)";
commentaireList << QLatin1String("-> tangente de x");
roleList << "tan()#4";

commandeList << "exp(x)";
commentaireList << QLatin1String("-> exponentielle de x");
roleList << "exp()#4";

commandeList << "log(x)";
commentaireList << QString::fromUtf8("-> logarithme népérien de x");
roleList << "log()#4";

commandeList << "abs(x)";
commentaireList << QLatin1String("-> valeur absolue de x");
roleList << "abs()#4";

commandeList << "floor(x)";
commentaireList << QString::fromUtf8("-> partie entière de x");
roleList << "floor()#6";

commandeList << "round(x)";
commentaireList << QLatin1String("-> entier le plus proche de x");
roleList << "round()#6";

commandeList << "acos(x)";
commentaireList << QLatin1String("-> arccosinus de x");
roleList << "acos()#5";

commandeList << "asin(x)";
commentaireList << QLatin1String("-> arcsinus de x");
roleList << "asin()#5";

commandeList << "atan(x)";
commentaireList << QLatin1String("-> arctangente de x");
roleList << "atan()#5";

commandeList << "Math.PI";
commentaireList << QLatin1String("-> constante PI");
roleList << "Math.PI#7";

commandeList << "random()";
commentaireList << QString::fromUtf8("-> nombre pseudo-aléatoire entre 0 et 1");
roleList << "random()#7";

commandeList << "ALGOBOX_ALEA_ENT(p,n)";
commentaireList << QString::fromUtf8("-> entier pseudo-aléatoire entre p et n");
roleList << "ALGOBOX_ALEA_ENT(,)#17";

commandeList << "ALGOBOX_COEFF_BINOMIAL(n,p)";
commentaireList << QLatin1String("-> coefficient binomial 'p parmi n'");
roleList << "ALGOBOX_COEFF_BINOMIAL(,)#23";

commandeList << "ALGOBOX_LOI_BINOMIALE(n,p,k)";
commentaireList << QString::fromUtf8("-> p(X=k) pour la loi binomiale de paramètres (n,p)");
roleList << "ALGOBOX_LOI_BINOMIALE(,,)#22";

commandeList << "ALGOBOX_LOI_NORMALE_CR(x)";
commentaireList << QString::fromUtf8("-> p(X<x) pour la loi normale centrée réduite");
roleList << "ALGOBOX_LOI_NORMALE_CR()#23";

commandeList << "ALGOBOX_LOI_NORMALE(esp,ecart,x)";
commentaireList << QString::fromUtf8("-> p(X<x) pour la loi normale d'espérance 'esp' et d'écart-type 'ecart'");
roleList << "ALGOBOX_LOI_NORMALE(,,)#20";

commandeList << "ALGOBOX_INVERSE_LOI_NORMALE_CR(p)";
commentaireList << QString::fromUtf8("-> opération inverse de ALGOBOX_LOI_NORMALE_CR(x)");
roleList << "ALGOBOX_INVERSE_LOI_NORMALE_CR()#31";

commandeList << "ALGOBOX_INVERSE_LOI_NORMALE(esp,ecart,p)";
commentaireList << QString::fromUtf8("-> opération inverse de ALGOBOX_LOI_NORMALE(esp,ecart,x)");
roleList << "ALGOBOX_INVERSE_LOI_NORMALE(,,)#28";

commandeList << "ALGOBOX_FACTORIELLE(n)";
commentaireList << QString::fromUtf8("-> factorielle de n");
roleList << "ALGOBOX_FACTORIELLE()#20";

commandeList << "ALGOBOX_SOMME(liste,p,n)";
commentaireList << QString::fromUtf8("-> somme des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_SOMME(,,)#14";

commandeList << "ALGOBOX_MOYENNE(liste,p,n)";
commentaireList << QString::fromUtf8("-> moyenne des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MOYENNE(,,)#16";

commandeList << "ALGOBOX_VARIANCE(liste,p,n)";
commentaireList << QString::fromUtf8("-> variance des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_VARIANCE(,,)#17";

commandeList << "ALGOBOX_ECART_TYPE(liste,p,n)";
commentaireList << QString::fromUtf8("-> écart-type des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_ECART_TYPE(,,)#19";

commandeList << "ALGOBOX_MEDIANE(liste,p,n)";
commentaireList << QString::fromUtf8("-> médiane des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MEDIANE(,,)#16";

commandeList << "ALGOBOX_QUARTILE1(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q1 (calculatrice) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE1(,,)#18";

commandeList << "ALGOBOX_QUARTILE3(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q3 (calculatrice) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE3(,,)#18";

commandeList << "ALGOBOX_QUARTILE1_BIS(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q1 (25%) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE1_BIS(,,)#22";

commandeList << "ALGOBOX_QUARTILE3_BIS(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q3 (75%) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE3_BIS(,,)#22";

commandeList << "ALGOBOX_MINIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> minimum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MINIMUM(,,)#16";

commandeList << "ALGOBOX_MAXIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> maximum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MAXIMUM(,,)#16";

commandeList << "ALGOBOX_POS_MINIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> rang du minimum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_POS_MINIMUM(,,)#20";

commandeList << "ALGOBOX_POS_MAXIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> rang du maximum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_POS_MAXIMUM(,,)#20";

for (int i = 0; i < commandeList.count(); i++)
{
commande=new QListWidgetItem(ui.listWidgetOp);
commande->setFont(fontCommande);
commande->setText(commandeList.at(i));
commande->setData(Qt::UserRole,roleList.at(i));
commentaire=new QListWidgetItem(ui.listWidgetOp);
commentaire->setFont(fontCommentaire);
commentaire->setText(commentaireList.at(i));
commentaire->setFlags(Qt::ItemIsEnabled);
}

connect(ui.listWidgetOp, SIGNAL(itemClicked ( QListWidgetItem*)), this, SLOT(InsertOperation(QListWidgetItem*)));

QTextCodec *codec = QTextCodec::codecForName("UTF-8");

setWindowIcon(QIcon(":/images/algobox128.png"));

connect(ui.pushButtonAjouterLigne, SIGNAL(clicked()), this, SLOT(AjouterLigne()));

connect(ui.pushButtonSupprimerLigne, SIGNAL(clicked()), this, SLOT(SupprimerLigne()));

connect(ui.pushButtonModifierLigne, SIGNAL(clicked()), this, SLOT(ModifierLigne()));

connect(ui.treeWidget, SIGNAL(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)),this,SLOT(ActualiserArbre()));

connect(ui.pushButtonVariable, SIGNAL(clicked()), this, SLOT(AjouterVariable()));

connect(ui.pushButtonLire, SIGNAL(clicked()), this, SLOT(AjouterLire()));

connect(ui.pushButtonAfficher, SIGNAL(clicked()), this, SLOT(AjouterAfficher()));

connect(ui.pushButtonMessage, SIGNAL(clicked()), this, SLOT(AjouterMessage()));

connect(ui.pushButtonPause, SIGNAL(clicked()), this, SLOT(AjouterPause()));

connect(ui.pushButtonAffectation, SIGNAL(clicked()), this, SLOT(AjouterAffectation()));

connect(ui.pushButtonCondition, SIGNAL(clicked()), this, SLOT(AjouterCondition()));

connect(ui.pushButtonBoucle, SIGNAL(clicked()), this, SLOT(AjouterBoucle()));

connect(ui.pushButtonTantque, SIGNAL(clicked()), this, SLOT(AjouterTantque()));

connect(ui.pushButtonPoint, SIGNAL(clicked()), this, SLOT(AjouterPoint()));

connect(ui.pushButtonSegment, SIGNAL(clicked()), this, SLOT(AjouterSegment()));

connect(ui.pushButtonExecuter, SIGNAL(clicked()), this, SLOT(JavascriptExport()));

connect(ui.pushButtonCommentaire, SIGNAL(clicked()), this, SLOT(AjouterCommentaire()));

connect(ui.checkBoxFonction, SIGNAL(toggled(bool)),this, SLOT(ActiverFonction(bool)));

connect(ui.checkBoxRepere, SIGNAL(toggled(bool)),this, SLOT(ActiverRepere(bool)));

connect(ui.pushButtonExecuterBis, SIGNAL(clicked()), this, SLOT(JavascriptExport()));

connect(ui.pushButtonVerifier, SIGNAL(clicked()), this, SLOT(VerifierCodeTexte()));

connect(ui.checkBoxF2, SIGNAL(toggled(bool)),this, SLOT(ActiverF2(bool)));
connect(ui.pushButtonAjouterF2, SIGNAL(clicked()), this, SLOT(AjouterF2()));
connect(ui.pushButtonHautF2, SIGNAL(clicked()), this, SLOT(HautF2()));
connect(ui.pushButtonBasF2, SIGNAL(clicked()), this, SLOT(BasF2()));
connect(ui.pushButtonSupprimerF2, SIGNAL(clicked()), this, SLOT(SupprimerF2()));
connect(ui.listWidgetF2, SIGNAL(itemDoubleClicked ( QListWidgetItem*)), this, SLOT(ModifierLigneF2(QListWidgetItem*)));

#ifndef VERSION_MINIPC
fileToolBar = addToolBar("Barre d'outils");
fileToolBar->setObjectName("Barre d'outils");
fileToolBar->setIconSize(QSize(22,22 ));
fileToolBar->setFloatable(false);
fileToolBar->setOrientation(Qt::Horizontal);
fileToolBar->setMovable(false);
fileToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
#endif

fichierMenu = menuBar()->addMenu(QLatin1String("&Fichier"));
QAction *Act;

Act = new QAction(QIcon::fromTheme(QLatin1String("document-new"), QIcon(QLatin1String(":/images/nouveau.png"))), QLatin1String("&Nouveau"), this);
Act->setShortcut(Qt::CTRL+Qt::Key_N);
connect(Act, SIGNAL(triggered()), this, SLOT(NouvelAlgo()));
fichierMenu->addAction(Act);
fileToolBar->addAction(Act);

Act = new QAction(QIcon::fromTheme(QLatin1String("document-open"), QIcon(QLatin1String(":/images/ouvrir.png"))), QLatin1String("&Ouvrir"), this);
Act->setShortcut(Qt::CTRL+Qt::Key_O);
connect(Act, SIGNAL(triggered()), this, SLOT(ChargerAlgo()));
fichierMenu->addAction(Act);
fileToolBar->addAction(Act);

recentMenu=fichierMenu->addMenu(QString::fromUtf8("Récemment ouverts"));
recentMenu->setIcon(QIcon::fromTheme(QLatin1String("document-open-recent")));
for (int i = 0; i < 5; ++i) 
	{
            recentFileActs[i] = new QAction(QIcon::fromTheme(QLatin1String("document-save-as")), QString(),  this);
	recentFileActs[i]->setVisible(false);
	connect(recentFileActs[i], SIGNAL(triggered()),this, SLOT(fileOpenRecent()));
	recentMenu->addAction(recentFileActs[i]);
	}
//********************************************************************************
// Act = new QAction(QString::fromUtf8("&Ouvrir code texte"), this);
// connect(Act, SIGNAL(triggered()), this, SLOT(ImporterCodeTexte()));
// fichierMenu->addAction(Act);
//********************************************************************************

Act = new QAction(QIcon::fromTheme(QLatin1String("document-save"), QIcon(QLatin1String(":/images/sauver.png"))), QLatin1String("&Sauver"), this);
Act->setShortcut(Qt::CTRL+Qt::Key_S);
connect(Act, SIGNAL(triggered()), this, SLOT(SauverAlgo()));
fichierMenu->addAction(Act);
fileToolBar->addAction(Act);
fileToolBar->addSeparator();

Act = new QAction(QIcon(":/images/executer22.png"), QLatin1String("Tester"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(JavascriptExport()));
fileToolBar->addAction(Act);

Act = new QAction(QIcon::fromTheme(QLatin1String("document-save-as"), QIcon(QLatin1String(":/images/sauversous.png"))),QLatin1String("Sauver So&us"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(SauverSousAlgo()));
fichierMenu->addAction(Act);

fichierMenu->addSeparator();

Act = new QAction(QLatin1String("Exporter algorithme complet vers page &web"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ExporterVersHtml()));
fichierMenu->addAction(Act);

Act = new QAction(QLatin1String("Exporter code vers fichier &texte"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ExporterVersTexte()));
fichierMenu->addAction(Act);

Act = new QAction(QLatin1String("Exporter code au format O&DF (OpenOffice.org, Koffice)"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ExporterVersODF()));
fichierMenu->addAction(Act);

Act = new QAction(QLatin1String("Exporter code vers document &LaTeX"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ExporterVersLatex()));
fichierMenu->addAction(Act);

Act = new QAction(QIcon::fromTheme(QLatin1String("document-print"), QIcon(QLatin1String(":/images/imprimer.png"))),QLatin1String("&Imprimer algorithme"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(Imprimer()));
fichierMenu->addAction(Act);

fichierMenu->addSeparator();

Act = new QAction(QLatin1String("Ouvrir un &exemple"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ChargerExemple()));
fichierMenu->addAction(Act);

fichierMenu->addSeparator();

Act = new QAction( QLatin1String("&Quitter"), this);
Act->setShortcut(Qt::CTRL+Qt::Key_Q);
connect(Act, SIGNAL(triggered()), this, SLOT(Quitter()));
fichierMenu->addAction(Act);

editMenu= menuBar()->addMenu(QLatin1String("&Edition"));

actionCopier=new QAction(QIcon::fromTheme(QLatin1String("edit-copy"), QIcon(QLatin1String(":/images/copier.png"))),QLatin1String("Cop&ier Ligne"), this);
actionCopier->setShortcut(Qt::CTRL+Qt::Key_C);
connect(actionCopier, SIGNAL(triggered()), this, SLOT(EditCopier()));
editMenu->addAction(actionCopier);
actionCopier->setEnabled(false);

actionColler=new QAction(QIcon::fromTheme(QLatin1String("edit-paste"), QIcon(QLatin1String(":/images/coller.png"))),QLatin1String("C&oller Ligne"), this);
actionColler->setShortcut(Qt::CTRL+Qt::Key_V);
connect(actionColler, SIGNAL(triggered()), this, SLOT(EditColler()));
editMenu->addAction(actionColler);
actionColler->setEnabled(false);

actionCouper=new QAction(QIcon::fromTheme(QLatin1String("edit-cut"), QIcon(QLatin1String(":/images/couper.png"))),QLatin1String("Co&uper Ligne"), this);
actionCouper->setShortcut(Qt::CTRL+Qt::Key_X);
connect(actionCouper, SIGNAL(triggered()), this, SLOT(EditCouper()));
editMenu->addAction(actionCouper);
actionCouper->setEnabled(false);

modeMenu=editMenu->addMenu(QLatin1String("Mode &Edition"));
modeGroup=new QActionGroup(this);
Act = new QAction(QLatin1String("Mode &normal"), this);
Act->setCheckable(true);
connect(Act, SIGNAL(triggered()), this, SLOT(ActualiserMode()));
modeGroup->addAction(Act);
modeMenu->addAction(Act);
if (modeNormal) Act->setChecked(true);
Act = new QAction(QString::fromUtf8("Mode éditeur &texte"), this);
Act->setCheckable(true);
connect(Act, SIGNAL(triggered()), this, SLOT(ActualiserMode()));
modeGroup->addAction(Act);
modeMenu->addAction(Act);
if (!modeNormal) Act->setChecked(true);

tutoMenu = menuBar()->addMenu(QLatin1String("&Tutoriel"));
Act = new QAction(QString::fromUtf8("&Initiation en ligne à l'algorithmique"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(Tutoriel()));
tutoMenu->addAction(Act);

affichageMenu=menuBar()->addMenu(QLatin1String("Afficha&ge"));

affichageMenu->addAction(fileToolBar->toggleViewAction());

ToggleAct = new QAction(this);
ToggleAct->setText(QString::fromUtf8("Cadre '&Présentation'"));
ToggleAct->setCheckable(true);
ToggleAct->setChecked(afficheCadrePresentation);
connect(ToggleAct, SIGNAL(triggered()), this, SLOT(ToggleCadrePresentation()));
affichageMenu->addAction(ToggleAct);

affichageMenu->addSeparator();
Act = new QAction(QLatin1String("Changer Police &Interface"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(SetInterfaceFont()));
affichageMenu->addAction(Act);

affichageMenu->addSeparator();
Act = new QAction(QString::fromUtf8("Changer Couleur &Console 'Résultats'"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(CouleurConsole()));
affichageMenu->addAction(Act);

aideMenu = menuBar()->addMenu(QLatin1String("&Aide"));
Act = new QAction(QIcon::fromTheme(QLatin1String("help-contents"), QIcon(QLatin1String(":/images/aide.png"))),QLatin1String("A&ide"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(Aide()));
aideMenu->addAction(Act);
aideMenu->addSeparator();
Act = new QAction(QIcon::fromTheme(QLatin1String("help-about")), QLatin1String("A &propos d'AlgoBox"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(APropos()));
aideMenu->addAction(Act);

completer = new QCompleter(this);
QAbstractItemModel *model;
QFile completefile(":/documents/completion.txt");
if (!completefile.open(QFile::ReadOnly)) model=new QStringListModel(completer);
QStringList words;
QString line;
QTextStream tscompleter(&completefile);
tscompleter.setCodec(codec);
while (!tscompleter.atEnd()) 
	{
	line = tscompleter.readLine();
	if (!line.isEmpty()) words.append(line.remove("\n"));
	}
words.sort();
model=new QStringListModel(words, completer);
completer->setModel(model);
completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
completer->setCaseSensitivity(Qt::CaseInsensitive);
completer->setWrapAround(false);
ui.EditorView->editor->setCompleter(completer);

UpdateRecentFile();

QFile interditsfile(":/documents/interdits.txt");
if (interditsfile.open(QFile::ReadOnly))
    {
    QString line;
    while (!interditsfile.atEnd()) 
	    {
	    line = interditsfile.readLine();
	    if (!line.isEmpty()) ListeNomsInterdits.append(line.trimmed());
	    }
    }
estVierge=true;
estModifie=false;
nomFichier="sanstitre";
setWindowTitle(QString("AlgoBox "ALGOBOXVERSION" : ")+nomFichier);
Init();
//DesactiverBoutons();
ActualiserArbre();

connect(ui.textEditDescription, SIGNAL(textChanged()),this,SLOT(ActualiserStatut()));
connect(ui.lineEditFonction, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditXmin, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditXmax, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditYmin, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditYmax, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditGradX, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditGradY, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditParametresF2, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditConditionF2, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditRetourF2, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditDefautF2, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
//connect(ui.EditorView->editor, SIGNAL(textChanged()), this, SLOT(ActualiserStatut()));
connect(ui.EditorView->editor->document(), SIGNAL(modificationChanged(bool)), this, SLOT(NouveauStatut(bool)));
ui.EditorView->editor->document()->setModified(false);
restoreState(windowstate, 0);

if (modeNormal) ui.stackedWidget->setCurrentWidget(ui.page_arbre);
else 
  {
  ui.stackedWidget->setCurrentWidget(ui.page_editeur);
  ui.EditorView->editor->setFocus();
  }
}

MainWindow::~MainWindow(){
}

void MainWindow::closeEvent(QCloseEvent *e)
{
if (estModifie) 
	{
        switch(  QMessageBox::warning(this,QLatin1String("AlgoBox : "),
					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de quitter?"),
                                        QLatin1String("Enregistrer"), QLatin1String("Ne pas enregistrer"), QLatin1String("Abandon"),
					0,
					2 ) )
		{
		case 0:
		SauverAlgo();
		break;
		case 1:
		break;
		case 2:
		default:
		e->ignore();
		return;
		break;
		}
	}
SauverConfig();
if (browserWindow) browserWindow->close();
e->accept();
}

void MainWindow::Quitter()
{
if (estModifie) 
	{
        switch(  QMessageBox::warning(this,QLatin1String("AlgoBox : "),
					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de quitter?"),
                                        QLatin1String("Enregistrer"), QLatin1String("Ne pas enregistrer"), QLatin1String("Abandon"),
					0,
					2 ) )
		{
		case 0:
		SauverAlgo();
		break;
		case 1:
		break;
		case 2:
		default:
		return;
		break;
		}
	}
SauverConfig();
if (browserWindow) browserWindow->close();
qApp->quit();
}
//*************************
void MainWindow::AjouterLigne()
{
if (!modeNormal) return;
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
NouvelleLigne(curItem);
ActualiserArbre();
ActualiserStatut();
}

void MainWindow::NouvelleLigne(QTreeWidgetItem *item)
{
QTreeWidgetItem *newItem = 0;
QTreeWidgetItem *nextItem = 0;
if (item->text(0)=="DEBUT_ALGORITHME")
    {
    newItem=new QTreeWidgetItem(QStringList(QString("")));
    newItem->setData(0,Qt::UserRole,QString("103#commentaire"));
    debutItem->insertChild(0,newItem);
    ui.treeWidget->setCurrentItem(newItem);
    }
else if ((item->text(0).left(3)=="FIN") ||(item->text(0)=="SINON") )
    {
    if (item->parent())
	{
	int idx = item->parent()->childCount() - 1;
	nextItem = item->parent()->child(idx);
	NouvelleLigne(item->parent());
	}
    }
else if (item->text(0).left(5)=="DEBUT")
    {
    newItem=new QTreeWidgetItem(QStringList(QString("")));
    newItem->setData(0,Qt::UserRole,QString("103#commentaire"));
    if (item->parent())
	{
	item->parent()->insertChild(1,newItem);
	ui.treeWidget->setCurrentItem(newItem);	
	}
    }
else
    {
    if (item->parent()) newItem = new QTreeWidgetItem(item->parent(), item);
    newItem->setText(0,"");
    newItem->setData(0,Qt::UserRole,QString("103#commentaire"));
    ui.treeWidget->setCurrentItem(newItem);
    }
}
void MainWindow::SupprimerLigne()
{
if (!modeNormal) return;
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem) return;
QString code=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=code.split("#");
int type_para=parametres.at(0).toInt();
int query=1;
if (type_para==1)
    {
    query =QMessageBox::warning(this, "AlgoBox", QString::fromUtf8("Etes-vous sûr de vouloir supprimer la déclaration de cette variable?\n(si elle est utilisée dans le reste du code, l'algorithme ne pourra plus fonctionner)"),QLatin1String("Confirmer la suppression"), QLatin1String("Abandonner") );
    }
else if ((type_para==6) || (type_para==12) || (type_para==15)) 
    {
    query =QMessageBox::warning(this, "AlgoBox", QString::fromUtf8("La suppression de cette ligne entrainera la suppression de tout le bloc qui en dépend.\nEtes-vous sûr de vouloir supprimer tout ce bloc d'instructions?"),QLatin1String("Confirmer la suppression"), QLatin1String("Abandonner") );
    }
else
    {
    query =QMessageBox::warning(this, "AlgoBox", QString::fromUtf8("Etes-vous sûr de vouloir supprimer cette ligne?"),QLatin1String("Confirmer la suppression"), QLatin1String("Abandonner") );
    }
if (query==1) return;
QTreeWidgetItem *nextCurrent = 0;
if (curItem->parent()) 
	{
	int idx = curItem->parent()->indexOfChild(curItem);
	if (idx == curItem->parent()->childCount() - 1) idx--;
	else idx++;
	if (idx < 0) nextCurrent = curItem->parent();
	else nextCurrent = curItem->parent()->child(idx);
	} 
else 
	{
	int idx = ui.treeWidget->indexOfTopLevelItem(curItem);
	if (idx == ui.treeWidget->topLevelItemCount() - 1) idx--;
	else idx++;
	if (idx >= 0) nextCurrent = ui.treeWidget->topLevelItem(idx);
	}
delete curItem;
if (nextCurrent) ui.treeWidget->setCurrentItem(nextCurrent);
ActualiserArbre();
ActualiserStatut();
}

void MainWindow::ModifierLigne()
{
if (!modeNormal) return;
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem) return;
QString code=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=code.split("#");
int type_para=parametres.at(0).toInt();
if (type_para==2) ModifierLire();
else if (type_para==3) ModifierAfficher();
else if (type_para==4) ModifierMessage();
else if (type_para==5) ModifierAffectation();
else if (type_para==6) ModifierCondition();
else if (type_para==12) ModifierBoucle();
else if (type_para==15) ModifierTantque();
else if (type_para==19) ModifierCommentaire();
else if (type_para==50) ModifierPoint();
else if (type_para==51) ModifierSegment();
ActualiserArbre();
ActualiserStatut();
}

void MainWindow::ActualiserArbre()
{
if (modeNormal)
  {
  bool canAddline = true;
  bool canRemoveline = true;
  bool canModifyline=true;
  bool canCopyline=false;
  bool canPasteline=false;
  bool canCutline=false;
  QTreeWidgetItem *current = ui.treeWidget->currentItem();
  QTreeWidgetItem *next = 0;
  if (current) 
	  {
	  QString code=current->data(0,Qt::UserRole).toString();
	  QStringList parametres=code.split("#");
	  int type_para=parametres.at(0).toInt();
	  switch (type_para)
	      {
	      case 1: //VARIABLES
		      if (parametres.count()==3)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 2: //LIRE
		      if (parametres.count()==3)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
			  }
		      break;
	      case 3: //AFFICHER
		      if (parametres.count()==4)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
			  }
		      break;
	      case 4: //MESSAGE
		      if (parametres.count()==3)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
			  }
		      break;
	      case 5: //AFFECTATION
		      if (parametres.count()==4)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
			  }
		      break;
	      case 6: //SI
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=true;
			  canCopyline=true;//false
			  canPasteline=false;
			  canCutline=true;//false
			  }
		      break;
	      case 7: //DEBUT_SI
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 8: //FIN_SI
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = false;
			  canModifyline=false;
			  if (current->parent())
			      {
			      int idx = current->parent()->indexOfChild(current);
			      if (idx<current->parent()->childCount() - 1)
				  {
				  idx++;
				  if (idx>=0) 
				      {
				      next=current->parent()->child(idx);
				      if (next->text(0)!="SINON") canAddline = true;			  
				      }
				  }
			      else canAddline = true;
			      }
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 9: //SINON
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 10: //DEBUT_SINON
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 11: //FIN_SINON
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 12: //POUR
		      if (parametres.count()==4)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=true;
			  canCopyline=true;//false
			  canPasteline=false;
			  canCutline=true;//false
			  }
		      break;
	      case 13: //DEBUT_POUR
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 14: //FIN_POUR
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 15: //TANT_QUE
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=true;
			  canCopyline=true;//false
			  canPasteline=false;
			  canCutline=true;//false
			  }
		      break;
	      case 16: //DEBUT_TANT_QUE
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 17: //FIN_TANT_QUE
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 18: //PAUSE
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
			  }
		      break;
	      case 19: //COMMENTAIRE
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
			  }
		      break;
	      case 50: //POINT
		      if (parametres.count()==4)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
			  }
		      break;
	      case 51: //SEGMENT
		      if (parametres.count()==6)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
			  }
		      break;
	      case 100: //DECLARATIONS VARIABLES
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = false;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 101: //DEBUT_ALGO
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 102: //FIN_ALGO
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = false;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      case 103: //autres
	      default:
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
			  }
		      break;
	      }
	  if (current->text(0).isEmpty()) 
	      {
	      ActiverBoutons();
	      if (clipboardItem) 
		{
		if (!clipboardItem->text(0).isEmpty()) canPasteline=true;
		}
	      }
          else {
              //DesactiverBoutons();
          }
	  }
  else 
	  {
	  canAddline = false;
	  canModifyline=false;
          //DesactiverBoutons();
	  }
  ui.pushButtonAjouterLigne->setEnabled(canAddline);
  ui.pushButtonSupprimerLigne->setEnabled(canRemoveline);
  ui.pushButtonModifierLigne->setEnabled(canModifyline);

  actionCopier->setEnabled(canCopyline);
  actionColler->setEnabled(canPasteline);
  actionCouper->setEnabled(canCutline);
  ui.treeWidget->setFocus();
  ActualiserVariables();
  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);
  }
else
  {
  actionCopier->setEnabled(true);
  actionColler->setEnabled(true);
  actionCouper->setEnabled(true);
  ActiverBoutons();
  }
}

void MainWindow::ActualiserVariables()
{
ListeTypesVariables.clear();
QString code;
QRegExp rxvar("(.*) EST_DU_TYPE (.*)");
for (int i = 0; i < variablesItem->childCount(); i++) 
    {
    code=variablesItem->child(i)->text(0);
    if (rxvar.indexIn(code)>-1)
	{
	ListeTypesVariables.append(rxvar.cap(2));
	}
    }
}
//*************************
void MainWindow::AjouterVariable()
{
    if (modeNormal)
    {
        const QModelIndex &currentIndex = ui.treeView->currentIndex();
        VariableDialog *variableDialog = new VariableDialog(this);
        if (variableDialog->exec())
        {
            QString nomvariable = variableDialog->ui.lineEditVariable->text();
            QString type = variableDialog->ui.comboBoxVariable->currentText();
            QString code = "";
            theModel->addVariableDeclaration(currentIndex, nomvariable, CodeModelDefinitions::IntType);
            //newItem->setText(0, nomvariable + " EST_DU_TYPE " + type);
            code = "1#" + type + "#" + nomvariable;
            ActualiserStatut();
        }
        ActualiserArbre();
    }
    else
    {
        ui.EditorView->editor->insertTag(QLatin1String("• EST_DU_TYPE •"), 0, 0);
        ui.EditorView->editor->search(QString(0x2022), true, true, true, true);
    }
}

void MainWindow::AjouterLire()
{
    if (modeNormal) {
        const QModelIndex &currentIndex = ui.treeView->currentIndex();
        if (!theModel->variablesExists()) {
            QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
            return;
        }
        LireDialog *lireDialog = new LireDialog(this, theModel->getAllVariablesName().join("#"), ListeTypesVariables.join("#"));
        if (lireDialog->exec()) {
            QString nomvariable = lireDialog->ui.comboBoxLire->currentText();
            QString rang = lireDialog->ui.lineEditLire->text();
            QString code, type;
            code = "";
            if (!nomvariable.isEmpty()) {
                if (theModel->checkVariableExists(nomvariable)) {
                    type = theModel->getVariableType(nomvariable);
                    if ((type=="LISTE") && (!rang.isEmpty())) {
                        theModel->addInputVariableValue(currentIndex, nomvariable);
                        //curItem->setText(0, QLatin1String("LIRE ") + nomvariable + "[" + rang + "]");
                        code = "2#" + nomvariable + "#" + rang;
                    } else {
                        theModel->addInputVariableValue(currentIndex, nomvariable);
                        //curItem->setText(0, QLatin1String("LIRE ") + nomvariable);
                        code = "2#" + nomvariable + "#pasliste";
                    }
                }
                ActualiserStatut();
            } else {
                QMessageBox::warning(this, QLatin1String("Erreur"), QString::fromUtf8("Pas de variable définie"));
            }
        }
        ActualiserArbre();
    } else {
        ui.EditorView->editor->insertTag(QLatin1String("LIRE •"), 0, 0);
        ui.EditorView->editor->search(QString(0x2022), true, true, true, true);
    }
}

void MainWindow::AjouterAfficher()
{
    if (modeNormal) {
        const QModelIndex &currentIndex = ui.treeView->currentIndex();
        if (!theModel->variablesExists()) {
            QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
            return;
        }
        AfficherDialog *afficherDialog = new AfficherDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
        if (afficherDialog->exec()) {
            QString nomvariable=afficherDialog->ui.comboBoxAfficher->currentText();
            QString rang=afficherDialog->ui.lineEditAfficher->text();
            QString code="";
            if (!nomvariable.isEmpty()) {
                if (theModel->checkVariableExists(nomvariable)) {
                    QString type = ""/*theModel->getVariableType(nomvariable)*/;
                    if (afficherDialog->ui.checkBoxAfficher->isChecked()) {
                        code="3#"+nomvariable+"#1";
                    } else {
                        code="3#"+nomvariable+"#0";
                    }
                    if ((type=="LISTE") && (!rang.isEmpty())) {
                        theModel->addPrintVariable(currentIndex, nomvariable);
                        //curItem->setText(0,QLatin1String("AFFICHER ")+nomvariable+"["+rang+"]");
                        code+="#"+rang;
                    } else {
                        theModel->addPrintVariable(currentIndex, nomvariable);
                        //curItem->setText(0,QLatin1String("AFFICHER ")+nomvariable);
                        code+="#pasliste";
                    }
                }
                ActualiserStatut();
            } else {
                QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de variable définie"));
            }
        }
        ActualiserArbre();
    } else {
        ui.EditorView->editor->insertTag(QLatin1String("AFFICHER •"),0,0);
        ui.EditorView->editor->search(QString(0x2022) ,true,true,true,true);
    }
}

void MainWindow::AjouterMessage()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  MessageDialog *messageDialog = new MessageDialog(this);
  if (messageDialog->exec())
      {
      QString message=messageDialog->ui.lineEditMessage->text();
      message.remove("#");
      message.remove("\"");
  //    message.remove("'");
      QString code="";
      if (!message.isEmpty())
	  {
                  curItem->setText(0,QLatin1String("AFFICHER \"")+message+"\"");
		  if (messageDialog->ui.checkBoxMessage->isChecked()) code="4#"+message+"#1";
		  else code="4#"+message+"#0";
		  curItem->setData(0,Qt::UserRole,QString(code));
		  ui.treeWidget->setCurrentItem(curItem);
		  ActualiserStatut();
	  }
      else
	  {
          QMessageBox::warning( this,QLatin1String("Erreur"),QLatin1String("Message vide"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QLatin1String("AFFICHER \"•\""),0,0);
  ui.EditorView->editor->search(QString(0x2022) ,true,true,true,true);
  }
}

void MainWindow::AjouterPause()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  QString code="18#pause";
  curItem->setText(0,QLatin1String("PAUSE"));
  curItem->setData(0,Qt::UserRole,QString(code));
  ui.treeWidget->setCurrentItem(curItem);
  ActualiserStatut();
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QLatin1String("PAUSE"),5,0);
  }
}

void MainWindow::AjouterAffectation()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  if (!theModel->variablesExists())
      {
      QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
      return;
      }
  AffectationDialog *affectationDialog = new AffectationDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
  if (affectationDialog->exec())
      {
      QString nomvariable=affectationDialog->ui.comboBoxAffectation->currentText();
      QString contenu=affectationDialog->ui.lineEditAffectation->text();
      QString rang=affectationDialog->ui.lineEditAffectation2->text();
      contenu.remove("#");
      QString code="";
      if (!nomvariable.isEmpty())
	  {
	  if (!contenu.isEmpty())
	      {
                  if (theModel->checkVariableExists(nomvariable))
                  {
                  QString type = ""/*theModel->getVariableType(nomvariable)*/;
		  code="5#"+nomvariable+"#"+contenu;
		  if ((type=="LISTE") && (!rang.isEmpty())) 
		      {
                      curItem->setText(0,nomvariable+"["+rang+"]"+QLatin1String(" PREND_LA_VALEUR ")+contenu);
		      code+="#"+rang;
		      }
		  else 
		      {
                      curItem->setText(0,nomvariable+QLatin1String(" PREND_LA_VALEUR ")+contenu);
		      code+="#pasliste";
		      }
		  curItem->setData(0,Qt::UserRole,QString(code));
		  }
	      ui.treeWidget->setCurrentItem(curItem);
	      ActualiserStatut();
	      }
	  else
	      {
              QMessageBox::warning( this,QLatin1String("Erreur"),QLatin1String("Affectation vide"));
	      }
	  }
      else
	  {
          QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de variable définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QLatin1String("• PREND_LA_VALEUR •"),0,0);
  ui.EditorView->editor->search(QString(0x2022) ,true,true,true,true);
  }
}

void MainWindow::AjouterCondition()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  ConditionDialog *conditionDialog = new ConditionDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
  if (conditionDialog->exec())
      {
      QString condition=conditionDialog->ui.lineEditCondition->text();
      condition.remove("#");
      QString code="";
      QTreeWidgetItem *newItem, *sinonItem;
      if (!condition.isEmpty())
	  {
          curItem->setText(0,QLatin1String("SI (")+condition+") "+QLatin1String("ALORS"));
	  ui.treeWidget->setItemExpanded (curItem,true);
	  code="6#"+condition;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  newItem = new QTreeWidgetItem(curItem);
          newItem->setText(0,QLatin1String("DEBUT_SI"));
	  newItem->setData(0,Qt::UserRole,QString("7#debutsi"));
	  newItem = new QTreeWidgetItem(curItem);
	  ui.treeWidget->setCurrentItem(newItem);
	  newItem = new QTreeWidgetItem(curItem);
          newItem->setText(0,QLatin1String("FIN_SI"));
	  newItem->setData(0,Qt::UserRole,QString("8#finsi"));
	  if (conditionDialog->ui.checkBoxCondition->isChecked())
	      {
	      sinonItem = new QTreeWidgetItem(curItem,true);
              sinonItem->setText(0,QLatin1String("SINON"));
	      ui.treeWidget->setItemExpanded (sinonItem,true);
	      code="9#sinon";
	      sinonItem->setData(0,Qt::UserRole,QString(code));
	      newItem = new QTreeWidgetItem(sinonItem);
              newItem->setText(0,QLatin1String("DEBUT_SINON"));
	      newItem->setData(0,Qt::UserRole,QString("10#debutsinon"));
	      newItem = new QTreeWidgetItem(sinonItem);
	      newItem = new QTreeWidgetItem(sinonItem);
              newItem->setText(0,QLatin1String("FIN_SINON"));
	      newItem->setData(0,Qt::UserRole,QString("11#finsinon"));
	      }
	  ActualiserStatut();
	  }
      else
	  {
          QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de condition définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  QTextCursor cur=ui.EditorView->editor->textCursor();
  int pos=cur.position();
  ui.EditorView->editor->insertPlainText(QLatin1String("SI (•) ALORS"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QLatin1String("\tDEBUT_SI"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QLatin1String("FIN_SI"));
  cur.setPosition(pos,QTextCursor::MoveAnchor);
  ui.EditorView->editor->setTextCursor(cur);
  ui.EditorView->editor->setFocus();
  ui.EditorView->editor->search(QString(0x2022) ,true,true,true,true); 
  }
}

void MainWindow::AjouterBoucle()
{
#if 0
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  QStringList ListePourVariables;
  for (int i = 0; i < ListeNomsVariables.count(); i++) 
      {
      if (ListeTypesVariables.at(i)=="NOMBRE") ListePourVariables.append(ListeNomsVariables.at(i));
      }
  PourDialog *pourDialog = new PourDialog(this,ListePourVariables.join("#"));
  if (pourDialog->exec())
      {
      QString nomvariable=pourDialog->ui.comboBoxBoucle->currentText();
      QString debut=pourDialog->ui.lineEditBoucle1->text();
      debut.remove("#");
      QString fin=pourDialog->ui.lineEditBoucle2->text();
      fin.remove("#");
      QString code="";
      QTreeWidgetItem *newItem;
      if (!nomvariable.isEmpty())
	  {
	  if (!debut.isEmpty() && !fin.isEmpty())
	      {
	      int i=ListeNomsVariables.indexOf(nomvariable);
	      if (i>-1)
		  {
                  curItem->setText(0,QLatin1String("POUR ")+nomvariable+ QLatin1String(" ALLANT_DE ")+debut+ " A "+fin);
		  code="12#"+nomvariable+"#"+debut+"#"+fin;
		  curItem->setData(0,Qt::UserRole,QString(code));
		  newItem = new QTreeWidgetItem(curItem);
                  newItem->setText(0,QLatin1String("DEBUT_POUR"));
		  newItem->setData(0,Qt::UserRole,QString("13#debutpour"));
		  newItem = new QTreeWidgetItem(curItem);
		  ui.treeWidget->setCurrentItem(newItem);
		  newItem = new QTreeWidgetItem(curItem);
                  newItem->setText(0,QLatin1String("FIN_POUR"));
		  newItem->setData(0,Qt::UserRole,QString("14#finpour"));
		  }
	      ActualiserStatut();
	      }
	  else
	      {
              QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Les valeurs minimales et maximales du compteur ne sont pas définies"));
	      }
	  }
      else
	  {
          QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de variable définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  QTextCursor cur=ui.EditorView->editor->textCursor();
  int pos=cur.position();
  ui.EditorView->editor->insertPlainText(QLatin1String("POUR • ALLANT_DE • A •"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QLatin1String("\tDEBUT_POUR"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QLatin1String("FIN_POUR"));
  cur.setPosition(pos,QTextCursor::MoveAnchor);
  ui.EditorView->editor->setTextCursor(cur);
  ui.EditorView->editor->setFocus();
  ui.EditorView->editor->search(QString(0x2022) ,true,true,true,true);    
  }
#endif
}

void MainWindow::AjouterTantque()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  TantqueDialog *tantqueDialog = new TantqueDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
  if (tantqueDialog->exec())
      {
      QString condition=tantqueDialog->ui.lineEditTantque->text();
      condition.remove("#");
      QString code="";
      QTreeWidgetItem *newItem;
      if (!condition.isEmpty())
	  {
          curItem->setText(0,QLatin1String("TANT_QUE (")+condition+") "+QLatin1String("FAIRE"));
	  ui.treeWidget->setItemExpanded (curItem,true);
	  code="15#"+condition;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  newItem = new QTreeWidgetItem(curItem);
          newItem->setText(0,QLatin1String("DEBUT_TANT_QUE"));
	  newItem->setData(0,Qt::UserRole,QString("16#debuttantque"));
	  newItem = new QTreeWidgetItem(curItem);
	  ui.treeWidget->setCurrentItem(newItem);
	  newItem = new QTreeWidgetItem(curItem);
          newItem->setText(0,QLatin1String("FIN_TANT_QUE"));
	  newItem->setData(0,Qt::UserRole,QString("17#fintantque"));
	  ActualiserStatut();
	  }
      else
	  {
          QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de condition définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  QTextCursor cur=ui.EditorView->editor->textCursor();
  int pos=cur.position();
  ui.EditorView->editor->insertPlainText(QLatin1String("TANT_QUE (•) FAIRE"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QLatin1String("\tDEBUT_TANT_QUE"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QLatin1String("FIN_TANT_QUE"));
  cur.setPosition(pos,QTextCursor::MoveAnchor);
  ui.EditorView->editor->setTextCursor(cur);
  ui.EditorView->editor->setFocus();
  ui.EditorView->editor->search(QString(0x2022) ,true,true,true,true);
  }
}

void MainWindow::AjouterPoint()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  PointDialog *pointDialog = new PointDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
  if (pointDialog->exec())
      {
      QString x=pointDialog->ui.lineEditX->text();
      QString y=pointDialog->ui.lineEditY->text();
      QString couleur=pointDialog->ui.comboBoxCouleur->currentText();
      x.remove("#");
      y.remove("#");
      if ((!x.isEmpty()) && (!y.isEmpty()))
	  {
          curItem->setText(0,QLatin1String("TRACER_POINT (")+x+","+y+")");
	  QString code="50#"+x+"#"+y+"#"+couleur;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserStatut();
	  }
      else
	  {
          QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Au moins une des coordonnées n'est pas définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QLatin1String("TRACER_POINT (•,•)"),0,0);
  ui.EditorView->editor->search(QString(0x2022) ,true,true,true,true);
  }
}

void MainWindow::AjouterSegment()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  SegmentDialog *segmentDialog = new SegmentDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
  if (segmentDialog->exec())
      {
      QString xdep=segmentDialog->ui.lineEditXdep->text();
      QString ydep=segmentDialog->ui.lineEditYdep->text();
      QString xfin=segmentDialog->ui.lineEditXfin->text();
      QString yfin=segmentDialog->ui.lineEditYfin->text();
      QString couleur=segmentDialog->ui.comboBoxCouleur->currentText();
      xdep.remove("#");
      ydep.remove("#");
      xfin.remove("#");
      yfin.remove("#");
      if ((!xdep.isEmpty()) && (!ydep.isEmpty()) && (!xfin.isEmpty()) && (!yfin.isEmpty()))
	  {
          curItem->setText(0,QLatin1String("TRACER_SEGMENT (")+xdep+","+ydep+")->("+xfin+","+yfin+")");
	  QString code="51#"+xdep+"#"+ydep+"#"+xfin+"#"+yfin+"#"+couleur;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserStatut();
	  }
      else
	  {
          QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Au moins une des coordonnées n'est pas définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QLatin1String("TRACER_SEGMENT (•,•)->(•,•)"),0,0);
  ui.EditorView->editor->search(QString(0x2022) ,true,true,true,true);
  }
}

void MainWindow::AjouterCommentaire()
{
if (modeNormal)
  {
  QFont commentFont = qApp->font();
  commentFont.setItalic(true);
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  CommentaireDialog *commentaireDialog = new CommentaireDialog(this);
  if (commentaireDialog->exec())
      {
      QString commentaire=commentaireDialog->ui.lineEditCommentaire->text();
      commentaire.remove("#");
      //commentaire.remove("\"");
      QString code="";
      if (!commentaire.isEmpty())
	  {
		  curItem->setText(0,"//"+commentaire);
		  code="19#"+commentaire;
		  curItem->setData(0,Qt::UserRole,QString(code));
		  curItem->setFont(0,commentFont);
		  ui.treeWidget->setCurrentItem(curItem);
		  ActualiserStatut();
	  }
      else
	  {
          QMessageBox::warning( this,QLatin1String("Erreur"),QLatin1String("Commentaire vide"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QLatin1String("//"),2,0);
  }
}
//***********************************************
void MainWindow::ModifierLire()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
if (!theModel->variablesExists())
    {
    QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
    return;
    }

QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==3)
    {
    QString exnomvariable=parametres.at(1);
    QString exrang=parametres.at(2);
    if (theModel->checkVariableExists(exnomvariable))
    {
        QString extype=""/*ListeTypesVariables.at(i)*/;
        LireDialog *lireDialog = new LireDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
	int f=lireDialog->ui.comboBoxLire->findText(exnomvariable,Qt::MatchExactly | Qt::MatchCaseSensitive);
	lireDialog->ui.comboBoxLire->setCurrentIndex(f);
	if (extype=="LISTE")
	    {
	    lireDialog->ui.labelLire2->setEnabled(true);
	    lireDialog->ui.lineEditLire->setEnabled(true);
	    lireDialog->ui.lineEditLire->setText(exrang);
	    }
	else
	    {
	    lireDialog->ui.lineEditLire->clear();
	    lireDialog->ui.labelLire2->setEnabled(false);
	    lireDialog->ui.lineEditLire->setEnabled(false);
	    }
	if (lireDialog->exec())
	    {
	    QString nomvariable=lireDialog->ui.comboBoxLire->currentText();
	    QString rang=lireDialog->ui.lineEditLire->text();
	    QString code,type;
	    code="";
	    if (!nomvariable.isEmpty())
		{
		if (curItem && curItem->parent()) 
			{
                        if (theModel->checkVariableExists(exnomvariable))
                        {
                            type=""/*ListeTypesVariables.at(i)*/;
			    if ((type=="LISTE") && (!rang.isEmpty())) 
				{
                                curItem->setText(0,QLatin1String("LIRE ")+nomvariable+"["+rang+"]");
				code="2#"+nomvariable+"#"+rang;
				}
			    else 
				{
                                curItem->setText(0,QLatin1String("LIRE ")+nomvariable);
				code="2#"+nomvariable+"#pasliste";
				}
			    curItem->setData(0,Qt::UserRole,QString(code));
			    }
			ui.treeWidget->setCurrentItem(curItem);
			ActualiserStatut();
			}
		}
	    else
		{
                QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de variable définie"));
		}
	    }
	ActualiserArbre();
	}
    }
}

void MainWindow::ModifierAfficher()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
if (!theModel->variablesExists())
    {
    QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
    return;
    }
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==4)
    {
    QString exnomvariable=parametres.at(1);
    QString exretourligne=parametres.at(2);
    QString exrang=parametres.at(3);
    if (theModel->checkVariableExists(exnomvariable))
    {
        QString extype=""/*ListeTypesVariables.at(i)*/;
        AfficherDialog *afficherDialog = new AfficherDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
	int f=afficherDialog->ui.comboBoxAfficher->findText(exnomvariable,Qt::MatchExactly | Qt::MatchCaseSensitive);
	afficherDialog->ui.comboBoxAfficher->setCurrentIndex(f);
	if (extype=="LISTE")
	    {
	    afficherDialog->ui.labelAfficher2->setEnabled(true);
	    afficherDialog->ui.lineEditAfficher->setEnabled(true);
	    afficherDialog->ui.lineEditAfficher->setText(exrang);
	    }
	else
	    {
	    afficherDialog->ui.lineEditAfficher->clear();
	    afficherDialog->ui.labelAfficher2->setEnabled(false);
	    afficherDialog->ui.lineEditAfficher->setEnabled(false);
	    }
	if (exretourligne=="1") afficherDialog->ui.checkBoxAfficher->setChecked(true);
	else afficherDialog->ui.checkBoxAfficher->setChecked(false);
	if (afficherDialog->exec())
	    {
	    QString nomvariable=afficherDialog->ui.comboBoxAfficher->currentText();
	    QString rang=afficherDialog->ui.lineEditAfficher->text();
	    QString code="";
	    if (!nomvariable.isEmpty())
		{
                if (theModel->checkVariableExists(exnomvariable))
                {
                    QString type=""/*ListeTypesVariables.at(i)*/;
		    if (afficherDialog->ui.checkBoxAfficher->isChecked()) code="3#"+nomvariable+"#1";
		    else code="3#"+nomvariable+"#0";
		    if ((type=="LISTE") && (!rang.isEmpty())) 
			{
                        curItem->setText(0,QLatin1String("AFFICHER ")+nomvariable+"["+rang+"]");
			code+="#"+rang;
			}
		    else 
			{
                        curItem->setText(0,QLatin1String("AFFICHER ")+nomvariable);
			code+="#pasliste";
			}
		    curItem->setData(0,Qt::UserRole,QString(code));
		    }
		ui.treeWidget->setCurrentItem(curItem);
		ActualiserStatut();
		}
	    else
		{
                QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de variable définie"));
		}
	    }
	ActualiserArbre();
	}
    }
}

void MainWindow::ModifierMessage()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==3)
    {
    QString exmessage=parametres.at(1);
    QString exretourligne=parametres.at(2);
    MessageDialog *messageDialog = new MessageDialog(this);
    messageDialog->ui.lineEditMessage->setText(exmessage);
    if (exretourligne=="1") messageDialog->ui.checkBoxMessage->setChecked(true);
    else messageDialog->ui.checkBoxMessage->setChecked(false);
    if (messageDialog->exec())
	{
	QString message=messageDialog->ui.lineEditMessage->text();
	message.remove("#");
	message.remove("\"");
//	message.remove("'");
	QString code="";
	if (!message.isEmpty())
	    {
                    curItem->setText(0,QLatin1String("AFFICHER \"")+message+"\"");
		    if (messageDialog->ui.checkBoxMessage->isChecked()) code="4#"+message+"#1";
		    else code="4#"+message+"#0";
		    curItem->setData(0,Qt::UserRole,QString(code));
		    ui.treeWidget->setCurrentItem(curItem);
		    ActualiserStatut();
	    }
	else
	    {
            QMessageBox::warning( this,QLatin1String("Erreur"),QLatin1String("Message vide"));
	    }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierAffectation()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
if (!theModel->variablesExists())
    {
    QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
    return;
    }
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==4)
    {
    QString exnomvariable=parametres.at(1);
    QString excontenu=parametres.at(2);
    QString exrang=parametres.at(3);
    if (theModel->checkVariableExists(exnomvariable))
    {
        QString extype=""/*ListeTypesVariables.at(i)*/;
        AffectationDialog *affectationDialog = new AffectationDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
	int f=affectationDialog->ui.comboBoxAffectation->findText(exnomvariable,Qt::MatchExactly | Qt::MatchCaseSensitive);
	affectationDialog->ui.comboBoxAffectation->setCurrentIndex(f);
	affectationDialog->ui.lineEditAffectation->setText(excontenu);
	if (extype=="LISTE")
	    {
	    affectationDialog->ui.labelAffectation3->setEnabled(true);
	    affectationDialog->ui.lineEditAffectation2->setEnabled(true);
	    affectationDialog->ui.lineEditAffectation2->setText(exrang);
	    }
	else
	    {
	    affectationDialog->ui.lineEditAffectation2->clear();
	    affectationDialog->ui.lineEditAffectation2->setEnabled(false);
	    affectationDialog->ui.labelAffectation3->setEnabled(false);
	    }
	if (affectationDialog->exec())
	    {
	    QString nomvariable=affectationDialog->ui.comboBoxAffectation->currentText();
	    QString contenu=affectationDialog->ui.lineEditAffectation->text();
	    QString rang=affectationDialog->ui.lineEditAffectation2->text();
	    contenu.remove("#");
	    QString code="";
	    if (!nomvariable.isEmpty())
		{
                if (theModel->checkVariableExists(exnomvariable))
                {
                    QString type=""/*ListeTypesVariables.at(i)*/;
		    code="5#"+nomvariable+"#"+contenu;
		    if ((type=="LISTE") && (!rang.isEmpty())) 
			{
                        curItem->setText(0,nomvariable+"["+rang+"]"+QLatin1String(" PREND_LA_VALEUR ")+contenu);
			code+="#"+rang;
			}
		    else 
			{
                        curItem->setText(0,nomvariable+QLatin1String(" PREND_LA_VALEUR ")+contenu);
			code+="#pasliste";
			}
		    curItem->setData(0,Qt::UserRole,QString(code));
		    }
		ui.treeWidget->setCurrentItem(curItem);
		ActualiserStatut();
		}
	    else
		{
                QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de variable définie"));
		}
	    }
	ActualiserArbre();

	}
    }
}


void MainWindow::ModifierCondition()
{
QTreeWidgetItem *lastChildItem, *sinonItem, *newItem;
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==2)
    {
    QString excondition=parametres.at(1);
    ConditionDialog *conditionDialog = new ConditionDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
    conditionDialog->ui.lineEditCondition->setText(excondition);
    int idx=curItem->childCount()-1;
    if (idx>=0)
	{
	lastChildItem=curItem->child(idx);
	if (lastChildItem->text(0)=="SINON") 
	    {
	    conditionDialog->ui.checkBoxCondition->hide();
	    conditionDialog->ui.checkBoxCondition->setChecked(false);
	    }
	if (conditionDialog->exec())
	    {
	    QString condition=conditionDialog->ui.lineEditCondition->text();
	    condition.remove("#");
	    QString code="";
	    if (!condition.isEmpty())
		{
                curItem->setText(0,QLatin1String("SI (")+condition+") "+QLatin1String("ALORS"));
		ui.treeWidget->setItemExpanded (curItem,true);
		code="6#"+condition;
		curItem->setData(0,Qt::UserRole,QString(code));
		ui.treeWidget->setCurrentItem(curItem);
		if (conditionDialog->ui.checkBoxCondition->isChecked())
		    {
		    sinonItem = new QTreeWidgetItem(curItem,true);
                    sinonItem->setText(0,QLatin1String("SINON"));
		    ui.treeWidget->setItemExpanded (sinonItem,true);
		    code="9#sinon";
		    sinonItem->setData(0,Qt::UserRole,QString(code));
		    newItem = new QTreeWidgetItem(sinonItem);
                    newItem->setText(0,QLatin1String("DEBUT_SINON"));
		    newItem->setData(0,Qt::UserRole,QString("10#debutsinon"));
		    newItem = new QTreeWidgetItem(sinonItem);
		    ui.treeWidget->setCurrentItem(newItem);
		    newItem = new QTreeWidgetItem(sinonItem);
                    newItem->setText(0,QLatin1String("FIN_SINON"));
		    newItem->setData(0,Qt::UserRole,QString("11#finsinon"));
		    }
		ActualiserStatut();
		}
	    else
		{
                QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de condition définie"));
		}
	    }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierBoucle()
{
#if 0
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==4)
    {
    QString exnomvariable=parametres.at(1);
    QString exdebut=parametres.at(2);
    QString exfin=parametres.at(3);
    QStringList ListePourVariables;
    for (int i = 0; i < ListeNomsVariables.count(); i++) 
	{
	if (ListeTypesVariables.at(i)=="NOMBRE") ListePourVariables.append(ListeNomsVariables.at(i));
	}
    PourDialog *pourDialog = new PourDialog(this,ListePourVariables.join("#"));
    int f=pourDialog->ui.comboBoxBoucle->findText(exnomvariable,Qt::MatchExactly | Qt::MatchCaseSensitive);
    pourDialog->ui.comboBoxBoucle->setCurrentIndex(f);
    pourDialog->ui.lineEditBoucle1->setText(exdebut);
    pourDialog->ui.lineEditBoucle2->setText(exfin);
    if (pourDialog->exec())
	{
	QString nomvariable=pourDialog->ui.comboBoxBoucle->currentText();
	QString debut=pourDialog->ui.lineEditBoucle1->text();
	debut.remove("#");
	QString fin=pourDialog->ui.lineEditBoucle2->text();
	fin.remove("#");
	QString code="";
	if (!nomvariable.isEmpty())
	    {
	    int i=ListeNomsVariables.indexOf(nomvariable);
	    if (i>-1)
		{
                curItem->setText(0,QLatin1String("POUR ")+nomvariable+" ALLANT_DE "+debut+ " A "+fin);
		code="12#"+nomvariable+"#"+debut+"#"+fin;
		curItem->setData(0,Qt::UserRole,QString(code));
		ui.treeWidget->setCurrentItem(curItem);
		}
	    ActualiserStatut();
	    }
	else
	    {
            QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de variable définie"));
	    }
	}
    ActualiserArbre();
    }
#endif
}

void MainWindow::ModifierTantque()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==2)
    {
    QString excondition=parametres.at(1);
    TantqueDialog *tantqueDialog = new TantqueDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
    tantqueDialog->ui.lineEditTantque->setText(excondition);
    if (tantqueDialog->exec())
	{
	QString condition=tantqueDialog->ui.lineEditTantque->text();
	condition.remove("#");
	QString code="";
	if (!condition.isEmpty())
	    {
            curItem->setText(0,QLatin1String("TANT_QUE (")+condition+") "+QLatin1String("FAIRE"));
	    ui.treeWidget->setItemExpanded (curItem,true);
	    code="15#"+condition;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserStatut();
	    }
	else
	    {
            QMessageBox::warning( this,QLatin1String("Erreur"),QString::fromUtf8("Pas de condition définie"));
	    }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierPoint()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==4)
    {
    QString ex_x=parametres.at(1);
    QString ex_y=parametres.at(2);
    QString ex_couleur=parametres.at(3);
    PointDialog *pointDialog = new PointDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
    pointDialog->ui.lineEditX->setText(ex_x);
    pointDialog->ui.lineEditY->setText(ex_y);
    int index=pointDialog->ui.comboBoxCouleur->findText (ex_couleur , Qt::MatchExactly);
    pointDialog->ui.comboBoxCouleur->setCurrentIndex(index);
    if (pointDialog->exec())
	{
	QString x=pointDialog->ui.lineEditX->text();
	QString y=pointDialog->ui.lineEditY->text();
	QString couleur=pointDialog->ui.comboBoxCouleur->currentText();
	x.remove("#");
	y.remove("#");
	if ((!x.isEmpty()) && (!y.isEmpty()))
	    {
            curItem->setText(0,QLatin1String("TRACER_POINT (")+x+","+y+")");
	    QString code="50#"+x+"#"+y+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Au moins une des coordonnées n'est pas définie"));
	    }
	ActualiserArbre();
	}
    }
}

void MainWindow::ModifierSegment()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==6)
    {
    QString ex_xdep=parametres.at(1);
    QString ex_ydep=parametres.at(2);
    QString ex_xfin=parametres.at(3);
    QString ex_yfin=parametres.at(4);
    QString ex_couleur=parametres.at(5);
    SegmentDialog *segmentDialog = new SegmentDialog(this,theModel->getAllVariablesName().join("#"),ListeTypesVariables.join("#"));
    segmentDialog->ui.lineEditXdep->setText(ex_xdep);
    segmentDialog->ui.lineEditYdep->setText(ex_ydep);
    segmentDialog->ui.lineEditXfin->setText(ex_xfin);
    segmentDialog->ui.lineEditYfin->setText(ex_yfin);
    int index=segmentDialog->ui.comboBoxCouleur->findText (ex_couleur , Qt::MatchExactly);
    segmentDialog->ui.comboBoxCouleur->setCurrentIndex(index);
    if (segmentDialog->exec())
	{
	QString xdep=segmentDialog->ui.lineEditXdep->text();
	QString ydep=segmentDialog->ui.lineEditYdep->text();
	QString xfin=segmentDialog->ui.lineEditXfin->text();
	QString yfin=segmentDialog->ui.lineEditYfin->text();
	QString couleur=segmentDialog->ui.comboBoxCouleur->currentText();
	xdep.remove("#");
	ydep.remove("#");
	xfin.remove("#");
	yfin.remove("#");
	if ((!xdep.isEmpty()) && (!ydep.isEmpty()) && (!xfin.isEmpty()) && (!yfin.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_SEGMENT (")+xdep+","+ydep+")->("+xfin+","+yfin+")");
	    QString code="51#"+xdep+"#"+ydep+"#"+xfin+"#"+yfin+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Au moins une des coordonnées n'est pas définie"));
	    }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierCommentaire()
{
QFont commentFont = qApp->font();
commentFont.setItalic(true);
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==2)
    {
    QString excommentaire=parametres.at(1);
    CommentaireDialog *commentaireDialog = new CommentaireDialog(this);
    commentaireDialog->ui.lineEditCommentaire->setText(excommentaire);
    if (commentaireDialog->exec())
	{
	QString commentaire=commentaireDialog->ui.lineEditCommentaire->text();
	commentaire.remove("#");
	//commentaire.remove("\"");
	QString code="";
	if (!commentaire.isEmpty())
	    {
		    curItem->setText(0,"//"+commentaire);
		    code="19#"+commentaire;
		    curItem->setData(0,Qt::UserRole,QString(code));
		    curItem->setFont(0,commentFont);
		    ui.treeWidget->setCurrentItem(curItem);
		    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Commentaire vide"));
	    }
	}
    ActualiserArbre();
    }
}
//***********************************************
void MainWindow::LireConfig()
{
#ifdef USB_VERSION
QSettings *config=new QSettings(QCoreApplication::applicationDirPath()+"/algobox.ini",QSettings::IniFormat);
#else
QSettings *config=new QSettings(QSettings::IniFormat,QSettings::UserScope,"xm1","algobox");
#endif
config->beginGroup( "algobox" );
dernierRepertoire=config->value("dernier repertoire",QDir::homePath()).toString();
QRect screen = QApplication::desktop()->screenGeometry();
int w= config->value( "Geometries/MainwindowWidth",900).toInt();
int h= config->value( "Geometries/MainwindowHeight",screen.height()-200).toInt() ;
int x= config->value( "Geometries/MainwindowX",10).toInt();
int y= config->value( "Geometries/MainwindowY",10).toInt() ;
resize(w,h);
move(x,y);
windowstate=config->value("MainWindowState").toByteArray();
afficheCadrePresentation=config->value("Presentation",true).toBool();
modeNormal=config->value("Edition normale",true).toBool();
blackconsole=config->value("Black Console",true).toBool();
if (!afficheCadrePresentation)	ui.groupBoxDescription->hide();
browserwidth=config->value( "Geometries/BrowserWidth",850).toInt();
browserheight=config->value( "Geometries/BrowserHeight",540).toInt();

QFontDatabase fdb;
QStringList xf = fdb.families();
QString deft;
if (xf.contains("DejaVu Sans",Qt::CaseInsensitive)) deft="DejaVu Sans";
else if (xf.contains("DejaVu Sans LGC",Qt::CaseInsensitive)) deft="DejaVu Sans LGC";
else if (xf.contains("Bitstream Vera Sans",Qt::CaseInsensitive)) deft="Bitstream Vera Sans";
else if (xf.contains("Luxi Sans",Qt::CaseInsensitive)) deft="Luxi Sans";
else deft=qApp->font().family();
x11fontfamily=config->value("X11/Font Family",deft).toString();
x11fontsize=config->value( "X11/Font Size",qApp->font().pointSize()).toInt();
QFont x11Font (x11fontfamily,x11fontsize);
QApplication::setFont(x11Font);
ui.EditorView->setFontSize(x11fontsize);
#ifdef Q_WS_X11
int desktop_env=1; // 1 : no kde ; 2: kde ; 3 : kde4 ; 
QStringList styles = QStyleFactory::keys();
QString kdesession= ::getenv("KDE_FULL_SESSION");
QString kdeversion= ::getenv("KDE_SESSION_VERSION");
if (!kdesession.isEmpty()) desktop_env=2;
if (!kdeversion.isEmpty()) desktop_env=3;
if (desktop_env ==1) //no-kde
    {
    if (styles.contains("GTK+")) QApplication::setStyle(QStyleFactory::create("GTK+"));//gtkstyle
    else QApplication::setStyle(QStyleFactory::create("Cleanlooks"));
    }
else if ((desktop_env ==3) && (styles.contains("Oxygen"))) QApplication::setStyle(QStyleFactory::create("Oxygen")); //kde4+oxygen
else QApplication::setStyle(QStyleFactory::create("Plastique")); //others
QString baseStyleName = qApp->style()->objectName(); //fallback
if (baseStyleName == QLatin1String("Windows")) QApplication::setStyle(QStyleFactory::create("Plastique")); 



QPalette pal = QApplication::palette();
pal.setColor( QPalette::Active, QPalette::Highlight, QColor("#4490d8") );
pal.setColor( QPalette::Inactive, QPalette::Highlight, QColor("#4490d8") );
pal.setColor( QPalette::Disabled, QPalette::Highlight, QColor("#4490d8") );

pal.setColor( QPalette::Active, QPalette::HighlightedText, QColor("#ffffff") );
pal.setColor( QPalette::Inactive, QPalette::HighlightedText, QColor("#ffffff") );
pal.setColor( QPalette::Disabled, QPalette::HighlightedText, QColor("#ffffff") );

pal.setColor( QPalette::Active, QPalette::Base, QColor("#ffffff") );
pal.setColor( QPalette::Inactive, QPalette::Base, QColor("#ffffff") );
pal.setColor( QPalette::Disabled, QPalette::Base, QColor("#ffffff") );

pal.setColor( QPalette::Active, QPalette::WindowText, QColor("#000000") );
pal.setColor( QPalette::Inactive, QPalette::WindowText, QColor("#000000") );
pal.setColor( QPalette::Disabled, QPalette::WindowText, QColor("#000000") );

pal.setColor( QPalette::Active, QPalette::Text, QColor("#000000") );
pal.setColor( QPalette::Inactive, QPalette::Text, QColor("#000000") );
pal.setColor( QPalette::Disabled, QPalette::Text, QColor("#000000") );

pal.setColor( QPalette::Active, QPalette::ButtonText, QColor("#000000") );
pal.setColor( QPalette::Inactive, QPalette::ButtonText, QColor("#000000") );
pal.setColor( QPalette::Disabled, QPalette::ButtonText, QColor("#000000") );

if (desktop_env ==3)
	{
	pal.setColor( QPalette::Active, QPalette::Window, QColor("#eae9e9") );
	pal.setColor( QPalette::Inactive, QPalette::Window, QColor("#eae9e9") );
	pal.setColor( QPalette::Disabled, QPalette::Window, QColor("#eae9e9") );

	pal.setColor( QPalette::Active, QPalette::Button, QColor("#eae9e9") );
	pal.setColor( QPalette::Inactive, QPalette::Button, QColor("#eae9e9") );
	pal.setColor( QPalette::Disabled, QPalette::Button, QColor("#eae9e9") );
	}
else
	{
	pal.setColor( QPalette::Active, QPalette::Window, QColor("#f6f3eb") );
	pal.setColor( QPalette::Inactive, QPalette::Window, QColor("#f6f3eb") );
	pal.setColor( QPalette::Disabled, QPalette::Window, QColor("#f6f3eb") );

	pal.setColor( QPalette::Active, QPalette::Button, QColor("#f6f3eb") );
	pal.setColor( QPalette::Inactive, QPalette::Button, QColor("#f6f3eb") );
	pal.setColor( QPalette::Disabled, QPalette::Button, QColor("#f6f3eb") );
	}

QApplication::setPalette(pal);
#endif
recentFilesList=config->value("Files/Recent Files").toStringList();
config->endGroup();
}

void MainWindow::SauverConfig()
{
#ifdef USB_VERSION
QSettings config(QCoreApplication::applicationDirPath()+"/algobox.ini",QSettings::IniFormat);
#else
QSettings config(QSettings::IniFormat,QSettings::UserScope,"xm1","algobox");
#endif
config.beginGroup( "algobox" );
config.setValue("dernier repertoire",dernierRepertoire);
config.setValue("MainWindowState",saveState(0));
config.setValue("Presentation",afficheCadrePresentation);
config.setValue("Edition normale",modeNormal);
config.setValue("Black Console",blackconsole);
config.setValue("Geometries/MainwindowWidth", width() );
config.setValue("Geometries/MainwindowHeight", height() );
config.setValue("Geometries/MainwindowX", x() );
config.setValue("Geometries/MainwindowY", y() );
config.setValue("Geometries/BrowserWidth",browserwidth);
config.setValue("Geometries/BrowserHeight",browserheight);
config.setValue("X11/Font Family",x11fontfamily);
config.setValue( "X11/Font Size",x11fontsize);
if (recentFilesList.count()>0) config.setValue("Files/Recent Files",recentFilesList); 
config.endGroup();
}
//**************************
void MainWindow::InitOuvrir()
{
Init();
// ui.treeWidget->clear();
// clipboardItem=new QTreeWidgetItem(QStringList(QString("")));
// ui.treeWidget->setColumnCount(1);
// ui.treeWidget->header()->hide();
// ui.treeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
// ui.treeWidget->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
// //ui.treeWidget->header()->setResizeMode(0, QHeaderView::Stretch);
// ui.treeWidget->header()->setResizeMode(0, QHeaderView::ResizeToContents);
// ui.treeWidget->header()->setStretchLastSection(false);
// ui.textEditDescription->clear();
// 
// ui.checkBoxFonction->setChecked(false);
// ui.lineEditFonction->setText("");
// ui.lineEditFonction->setEnabled(false);
// 
// 
// ui.checkBoxRepere->setChecked(false);
// ui.lineEditXmin->setText("-10");
// ui.lineEditXmax->setText("10");
// ui.lineEditYmin->setText("-10");
// ui.lineEditYmax->setText("10");
// ui.lineEditGradX->setText("2");
// ui.lineEditGradY->setText("2");
// ui.lineEditXmin->setEnabled(false);
// ui.lineEditXmax->setEnabled(false);
// ui.lineEditYmin->setEnabled(false);
// ui.lineEditYmax->setEnabled(false);
// ui.lineEditGradX->setEnabled(false);
// ui.lineEditGradY->setEnabled(false);
// 
// editor->clear();
// ui.EditorView->editor->insertTag(QString::fromUtf8("VARIABLES\n\nDEBUT_ALGORITHME\n\nFIN_ALGORITHME"),0,1);
// if (!modeNormal) ui.EditorView->editor->setFocus();
// 
// ui.tabWidget->setCurrentIndex(0);
}

void MainWindow::Init()
{
ui.treeWidget->clear();
clipboardItem=new QTreeWidgetItem(QStringList(QString("")));
ui.treeWidget->setColumnCount(1);
ui.treeWidget->header()->hide();
ui.treeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
ui.treeWidget->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
//ui.treeWidget->header()->setResizeMode(0, QHeaderView::Stretch);
ui.treeWidget->header()->setResizeMode(0, QHeaderView::ResizeToContents);
ui.treeWidget->header()->setStretchLastSection(false);
ui.textEditDescription->clear();

QFont titleFont = qApp->font();
titleFont.setBold(true);

variablesItem =new QTreeWidgetItem;
variablesItem->setText(0,QString("VARIABLES"));
variablesItem->setData(0,Qt::UserRole,QString("100#declarationsvariables"));
variablesItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(variablesItem);
ui.treeWidget->setItemExpanded (variablesItem,true);

debutItem =new QTreeWidgetItem;
debutItem->setText(0,QString("DEBUT_ALGORITHME"));
debutItem->setData(0,Qt::UserRole,QString("101#debutalgo"));
debutItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(debutItem);
ui.treeWidget->setItemExpanded (debutItem,true);

finItem =new QTreeWidgetItem;
finItem->setText(0,QString("FIN_ALGORITHME"));
finItem->setData(0,Qt::UserRole,QString("102#finalgo"));
finItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(finItem);

ui.treeWidget->setCurrentItem(debutItem);

ui.checkBoxFonction->setChecked(false);
ui.lineEditFonction->setText("");
ui.lineEditFonction->setEnabled(false);
ui.listWidgetOp->setEnabled(false);

ui.checkBoxF2->setChecked(false);
ui.lineEditParametresF2->setText("");
ui.lineEditConditionF2->setText("");
ui.lineEditRetourF2->setText("");
ui.lineEditDefautF2->setText("");
ui.listWidgetF2->clear();
ui.lineEditParametresF2->setEnabled(false);
ui.lineEditConditionF2->setEnabled(false);
ui.lineEditRetourF2->setEnabled(false);
ui.lineEditDefautF2->setEnabled(false);
ui.listWidgetF2->setEnabled(false);
ui.pushButtonAjouterF2->setEnabled(false);
ui.pushButtonHautF2->setEnabled(false);
ui.pushButtonBasF2->setEnabled(false);
ui.pushButtonSupprimerF2->setEnabled(false);

ui.checkBoxRepere->setChecked(false);
ui.lineEditXmin->setText("-10");
ui.lineEditXmax->setText("10");
ui.lineEditYmin->setText("-10");
ui.lineEditYmax->setText("10");
ui.lineEditGradX->setText("2");
ui.lineEditGradY->setText("2");
ui.lineEditXmin->setEnabled(false);
ui.lineEditXmax->setEnabled(false);
ui.lineEditYmin->setEnabled(false);
ui.lineEditYmax->setEnabled(false);
ui.lineEditGradX->setEnabled(false);
ui.lineEditGradY->setEnabled(false);

ui.EditorView->editor->clear();
ui.EditorView->editor->insertTag(QString::fromUtf8("VARIABLES\n\nDEBUT_ALGORITHME\n\t\nFIN_ALGORITHME"),0,1);
if (!modeNormal) ui.EditorView->editor->setFocus();

ActualiserVariables();  
ui.tabWidget->setCurrentIndex(0);
}

void MainWindow::EffaceArbre()
{
ui.treeWidget->clear();
clipboardItem=new QTreeWidgetItem(QStringList(QString("")));
ui.treeWidget->setColumnCount(1);
ui.treeWidget->header()->hide();
ui.treeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
ui.treeWidget->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
//ui.treeWidget->header()->setResizeMode(0, QHeaderView::Stretch);
ui.treeWidget->header()->setResizeMode(0, QHeaderView::ResizeToContents);
ui.treeWidget->header()->setStretchLastSection(false);

QFont titleFont = qApp->font();
titleFont.setBold(true);

variablesItem =new QTreeWidgetItem;
variablesItem->setText(0,QString("VARIABLES"));
variablesItem->setData(0,Qt::UserRole,QString("100#declarationsvariables"));
variablesItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(variablesItem);
ui.treeWidget->setItemExpanded (variablesItem,true);

debutItem =new QTreeWidgetItem;
debutItem->setText(0,QString("DEBUT_ALGORITHME"));
debutItem->setData(0,Qt::UserRole,QString("101#debutalgo"));
debutItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(debutItem);
ui.treeWidget->setItemExpanded (debutItem,true);

finItem =new QTreeWidgetItem;
finItem->setText(0,QString("FIN_ALGORITHME"));
finItem->setData(0,Qt::UserRole,QString("102#finalgo"));
finItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(finItem);
ui.treeWidget->setCurrentItem(debutItem);  

ActualiserVariables();
}

void MainWindow::ActiverBoutons()
{
ui.pushButtonLire->setEnabled(true);
ui.pushButtonAfficher->setEnabled(true);
ui.pushButtonMessage->setEnabled(true);
ui.pushButtonPause->setEnabled(true);
ui.pushButtonAffectation->setEnabled(true);
ui.pushButtonCondition->setEnabled(true);
ui.pushButtonBoucle->setEnabled(true);
ui.pushButtonTantque->setEnabled(true);
ui.pushButtonCommentaire->setEnabled(true);
if (ui.checkBoxRepere->isChecked())
    {
    ui.pushButtonPoint->setEnabled(true);
    ui.pushButtonSegment->setEnabled(true);
    }
}

void MainWindow::DesactiverBoutons()
{
ui.pushButtonLire->setEnabled(false);
ui.pushButtonAfficher->setEnabled(false);
ui.pushButtonMessage->setEnabled(false);
ui.pushButtonPause->setEnabled(false);
ui.pushButtonAffectation->setEnabled(false);
ui.pushButtonCondition->setEnabled(false);
ui.pushButtonBoucle->setEnabled(false);
ui.pushButtonTantque->setEnabled(false);
ui.pushButtonPoint->setEnabled(false);
ui.pushButtonSegment->setEnabled(false);
ui.pushButtonCommentaire->setEnabled(false);
}

void MainWindow::ActiverFonction(bool etat)
{
ui.lineEditFonction->setEnabled(etat);
ui.listWidgetOp->setEnabled(etat);
ActualiserStatut();
}

void MainWindow::ActiverF2(bool etat)
{
ui.lineEditParametresF2->setEnabled(etat);
ui.lineEditConditionF2->setEnabled(etat);
ui.lineEditRetourF2->setEnabled(etat);
ui.lineEditDefautF2->setEnabled(etat);
ui.listWidgetF2->setEnabled(etat);
ui.pushButtonAjouterF2->setEnabled(etat);
ui.pushButtonHautF2->setEnabled(etat);
ui.pushButtonBasF2->setEnabled(etat);
ui.pushButtonSupprimerF2->setEnabled(etat);
ActualiserStatut();
}

void MainWindow::ActiverRepere(bool etat)
{
ui.lineEditXmin->setEnabled(etat);
ui.lineEditXmax->setEnabled(etat);
ui.lineEditYmin->setEnabled(etat);
ui.lineEditYmax->setEnabled(etat);
ui.lineEditGradX->setEnabled(etat);
ui.lineEditGradY->setEnabled(etat);

if (ui.pushButtonLire->isEnabled()) //ligne vide
    {
    ui.pushButtonPoint->setEnabled(etat);
    ui.pushButtonSegment->setEnabled(etat);
    }
else
    {
    ui.pushButtonPoint->setEnabled(false);
    ui.pushButtonSegment->setEnabled(false);
    }
ActualiserStatut();
}

//********************************
bool MainWindow::NomInterdit(QString nom)
{
bool retour=false;
#if 0
if ((ListeNomsInterdits.contains(nom,Qt::CaseInsensitive)) || (nom.isEmpty()) || (ListeNomsVariables.contains(nom,Qt::CaseInsensitive))) retour=true;
else
    {
    QStringList motcle=QString("algobox,F1(,acos(,asin(,atan(,sqrt(,pow(,random(,floor(,cos(,sin(,tan(,exp(,log(,round(,max(,min(,abs(").split(",");
    for (int i = 0; i < motcle.count(); i++)
	{
	if (nom.contains(motcle.at(i),Qt::CaseInsensitive) )
	    {
	    retour=true;
	    break;
	    }
	}
    }
#endif
return retour;
}
//*******************************
QString MainWindow::GenererCode(bool exporthtml)
{
QString code=QString::fromUtf8("<!-- Généré par AlgoBox -->\n");
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QFile modelefile;
if (exporthtml) 
  {
    modelefile.setFileName(":/documents/modeleexportweb.txt");
  }
else 
  {
    modelefile.setFileName(":/documents/modelepageweb.txt");
  }
modelefile.open(QIODevice::ReadOnly);
QTextStream t(&modelefile);
t.setCodec(codec);
while (!t.atEnd()) 
	{
	code+= t.readLine()+"\n";
	}
modelefile.close();

QString variablescode="";
#if 0
for (int i = 0; i < ListeNomsVariables.count(); i++) 
    {
    if (ListeTypesVariables.at(i)=="NOMBRE")
      {
      variablescode+="ALGOBOX_AFFICHE_PASAPAS(\""+ListeNomsVariables.at(i)+":\",false);\n";
      variablescode+="ALGOBOX_AFFICHE_PASAPAS(ALGOBOX_FORMAT_TEXTE("+ListeNomsVariables.at(i)+"),false);\n";
      if (i<ListeNomsVariables.count()-1) variablescode+="ALGOBOX_AFFICHE_PASAPAS(\" | \",false);\n";    
      }
    else if (ListeTypesVariables.at(i)=="CHAINE")
      {
      variablescode+="ALGOBOX_AFFICHE_PASAPAS(\""+ListeNomsVariables.at(i)+":\",false);\n";
      variablescode+="ALGOBOX_AFFICHE_PASAPAS("+ListeNomsVariables.at(i)+",false);\n";
      if (i<ListeNomsVariables.count()-1) variablescode+="ALGOBOX_AFFICHE_PASAPAS(\" | \",false);\n";    
      }
    }
for (int i = 0; i < ListeNomsVariables.count(); i++) 
    {
    if (ListeTypesVariables.at(i)=="LISTE")
      {
      variablescode+="ALGOBOX_AFFICHE_PASAPAS(\" \",true);\n";
      variablescode+="ALGOBOX_AFFICHE_PASAPAS(\"#\"+ALGOBOX_COMPTEUR_ETAPE+\" Liste "+ListeNomsVariables.at(i)+" ("+QString::fromUtf8("ligne")+" \"+ALGOBOX_ID_LIGNE+\") -> \",false);\n";
      variablescode+="for (ALGOBOX_CPT=0;ALGOBOX_CPT<"+ListeNomsVariables.at(i)+".length;ALGOBOX_CPT++)\n";
      variablescode+="{\n";
//      variablescode+="if ("+ListeNomsVariables.at(i)+"[0]==\"Erreur\") \n";
      variablescode+="if (ALGOBOX_FORMAT_TEXTE("+ListeNomsVariables.at(i)+"[ALGOBOX_CPT])!=\"Erreur\") ALGOBOX_AFFICHE_PASAPAS(ALGOBOX_FORMAT_TEXTE("+ListeNomsVariables.at(i)+"[ALGOBOX_CPT]),false);\n";
      variablescode+="else ALGOBOX_AFFICHE_PASAPAS(\"?\",false);\n";
      variablescode+="if (ALGOBOX_CPT<"+ListeNomsVariables.at(i)+".length-1) ALGOBOX_AFFICHE_PASAPAS(\" | \",false);\n";
      variablescode+="}\n";
      }
    }
#endif
code.replace("#AFFICHE_VARIABLES#",variablescode);

QString initcode="";
#if 0
for (int i = 0; i < ListeNomsVariables.count(); i++) 
    {
    if (ListeTypesVariables.at(i)=="NOMBRE")
      {
      initcode+=ListeNomsVariables.at(i)+"=0;\n";
      }
    else if (ListeTypesVariables.at(i)=="CHAINE")
      {
      initcode+=ListeNomsVariables.at(i)+"=\"\";\n";
      }
    else if (ListeTypesVariables.at(i)=="LISTE")
      {
      initcode+="for (ALGOBOX_CPT=0;ALGOBOX_CPT<"+ListeNomsVariables.at(i)+".length;ALGOBOX_CPT++)\n";
      initcode+="{\n";
      initcode+=ListeNomsVariables.at(i)+"[ALGOBOX_CPT]=0;\n";
      initcode+="}\n";
      }
    }
#endif
code.replace("#INIT_VARIABLES#",initcode);

int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches==0) return code;
QString jscriptcode="";
indent=0;
idligne=0;
//repereDefini=false;
for (int i = 0; i < nb_branches; i++) jscriptcode+=CodeNoeud(ui.treeWidget->topLevelItem(i),exporthtml);
code.replace("#JAVASCRIPT#",jscriptcode);

QFileInfo fic(nomFichier);
if (nomFichier!="sanstitre") code.replace("#TITRE#","AlgoBox : "+fic.baseName());
else code.replace("#TITRE#","AlgoBox : "+nomFichier);


QString desc=ui.textEditDescription->toPlainText();
if (!desc.isEmpty())
    {
    desc.replace(QString("<"),QString("&lt;"));
    desc.replace(QString(">"),QString("&gt;"));
    desc.replace(QString("\n"),QString("<br>"));
    desc="<div id=\"contentlabel\">"+QString::fromUtf8("Présentation de l'algorithme :")+"</div>\n<p>\n"+desc+"\n</p>\n";
    code.replace("#DESCRIPTION#",desc);
    }
else code.replace("#DESCRIPTION#","");

QString pseudocode="";
indent=0;
idligne=0;
for (int i = 0; i < nb_branches; i++) 
    {
    pseudocode+=AlgoNoeud(ui.treeWidget->topLevelItem(i));
    }
if (ui.checkBoxFonction->isChecked())
	{
	QString expression=ui.lineEditFonction->text();
	if (!expression.isEmpty() && !expression.contains("F1(",Qt::CaseInsensitive)) pseudocode+="<br>\n"+QString::fromUtf8("Fonction numérique utilisée :")+"<br>\nF1(x)="+expression+"\n";
	}
if (ui.checkBoxF2->isChecked())
	{
	if (ui.checkBoxFonction->isChecked()) pseudocode+="<br>\n";
	QString role,condition,commande;
	QStringList tagList;
	QListWidgetItem *item;
	pseudocode+="<br>\nfonction F2("+ui.lineEditParametresF2->text()+"):<br>\n";
	for (int i = 0; i < ui.listWidgetF2->count(); ++i)
	  {
	  tagList.clear();
	  item=ui.listWidgetF2->item(i);
	  role=item->data(Qt::UserRole).toString();
	  tagList= role.split("@");
	  if (tagList.count()==2) 
	    {
	    condition=tagList.at(0);
	    commande=tagList.at(1);
	    if ((!condition.isEmpty()) && (!commande.isEmpty()))
	      {
	      pseudocode+="SI ("+condition+") RENVOYER "+commande+"<br>\n";
	      }
	    }
	  }
	  if (!ui.lineEditDefautF2->text().isEmpty()) pseudocode+=QString::fromUtf8("Dans les autres cas, RENVOYER ")+ui.lineEditDefautF2->text()+"<br>\n";
	  }
QString texte="";	
QTextStream l(&pseudocode,QIODevice::ReadOnly);
l.setCodec(codec);
int ligne=1;
QString num="";
QString blanc="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
while (!l.atEnd()) 
	{
	num=QString::number(ligne);
	num+=blanc.left(blanc.length()-6*num.length());
	texte+=num+l.readLine()+"\n";
	ligne++;
	}

code.replace("#CODE#",texte);
return code;
}

QString MainWindow::CodeVersJavascript(QString algocode, bool exporthtml,int id)
{
QStringList parametres=algocode.split("#");
QString code="//***************";
QString varboucle;
if (parametres.count()>0)
  {
  int type_para=parametres.at(0).toInt();
  switch (type_para)
    {
    case 1: //VARIABLES
	    if (parametres.count()==3)
		{
		QString type=parametres.at(1);
		QString nomvariable=parametres.at(2);
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\n";
		if (type=="NOMBRE") code+=nomvariable+"=new Number();";
		else if (type=="CHAINE") code+=nomvariable+"=new String();";
		else if (type=="LISTE") code+=nomvariable+"=new Array();";
		}
	    break;
    case 2: //LIRE
	    if (parametres.count()==3)
		{
		QString nomvariable=parametres.at(1);
                QString rang=parametres.at(2);
                if (theModel->checkVariableExists(nomvariable))
		    {
                    QString type=""/*ListeTypesVariables.at(i)*/;
		    code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		    if (type=="CHAINE") 
		      {
		      code+=nomvariable+"=prompt(\"Entrer "+nomvariable+" :\");\n"+nomvariable+"="+nomvariable+".replace(new RegExp(\"\\\"\",\"g\"),\"\");\n";
		      if (!exporthtml) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}";
		      }
		    else if (type=="NOMBRE") 
		      {
		      code+=nomvariable+"_temp=prompt(\"Entrer "+nomvariable+" :\\n"+QString::fromUtf8("(utiliser le . comme séparateur décimal)")+"\\n(exemples de syntaxe possible : -3 ; 2.6 ; 4/3 ; 1+sqrt(3) ; ...)\");\n";
		      code+="if (!"+nomvariable+"_temp) {throw(\"erreur_input\");}\n";
		      code+="if (ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp)==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      code+=nomvariable+"=ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp);\n";
		      if (!exporthtml) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}";
		      }
		    else if (type=="LISTE") 
		      {
		      code+=nomvariable+"_temp=prompt(\"Entrer le terme de rang "+rang+" de la liste "+nomvariable+" :\\n"+QString::fromUtf8("(utiliser le . comme séparateur décimal)")+"\\n(exemples de syntaxe possible : -3 ; 2.6 ; 4/3 ; 1+sqrt(3) ; ...)\");\n";
		      code+=nomvariable+"_temp_tab="+nomvariable+"_temp.split(new RegExp(\"[:]+\", \"g\"));\n";
		      code+="for ("+nomvariable+"_temp_tab_compteur=0;"+nomvariable+"_temp_tab_compteur<"+nomvariable+"_temp_tab.length;"+nomvariable+"_temp_tab_compteur++)\n{\n";
		      code+="if (!"+nomvariable+"_temp_tab["+nomvariable+"_temp_tab_compteur]) {throw(\"erreur_input\");}\n";
		      code+="if (ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp_tab["+nomvariable+"_temp_tab_compteur])==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      code+=nomvariable+"["+rang+"+"+nomvariable+"_temp_tab_compteur]=ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp_tab["+nomvariable+"_temp_tab_compteur]);\n}\n";
		      code+="while ("+nomvariable+"_temp_tab.length>0) "+nomvariable+"_temp_tab.pop();\n";
//		      code+="if (!"+nomvariable+"_temp) {throw(\"erreur_input\");}\n";
//		      code+="if (ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp)==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      //code+=nomvariable+"["+rang+"]=ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp);\n";
		      if (!exporthtml) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}";
		      }
		    }
		}
	    break;
    case 3: //AFFICHER
	    if (parametres.count()==4)
		{
		  QString nomvariable=parametres.at(1);
		  QString retourligne=parametres.at(2);
		  QString rang=parametres.at(3);
                  if (theModel->checkVariableExists(nomvariable))
		      {
                      QString type=""/*ListeTypesVariables.at(i)*/;
		      code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		      if (retourligne=="1") 
			  {
			  if (type=="LISTE") code+="ALGOBOX_AJOUTE_OUTPUT(ALGOBOX_FORMAT_TEXTE("+nomvariable+"["+rang+"]"+"),true);";
			  else if (type=="NOMBRE") code+="ALGOBOX_AJOUTE_OUTPUT(ALGOBOX_FORMAT_TEXTE("+nomvariable+"),true);";
			  else code+="ALGOBOX_AJOUTE_OUTPUT("+nomvariable+",true);";
			  }
		      else 
			  {
			  if (type=="LISTE") code+="ALGOBOX_AJOUTE_OUTPUT(ALGOBOX_FORMAT_TEXTE("+nomvariable+"["+rang+"]"+"),false);";
			  else if (type=="NOMBRE") code+="ALGOBOX_AJOUTE_OUTPUT(ALGOBOX_FORMAT_TEXTE("+nomvariable+"),false);";
			  else code+="ALGOBOX_AJOUTE_OUTPUT("+nomvariable+",false);";
			  }
		      }	  
		}
	    break;
    case 4: //MESSAGE
	    if (parametres.count()==3)
		{
		  QString message=parametres.at(1);
		  QString retourligne=parametres.at(2);
		  code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		  if (retourligne=="1") code+="ALGOBOX_AJOUTE_OUTPUT(\""+message+"\",true);";
		  else code+="ALGOBOX_AJOUTE_OUTPUT(\""+message+"\",false);";	  
		}
	    break;
    case 5: //AFFECTATION
	    if (parametres.count()==4)
		{
		QString nomvariable=parametres.at(1);
		QString contenu=FiltreCalcul(parametres.at(2));
		QString rang=parametres.at(3);
                if (theModel->checkVariableExists(nomvariable))
		    {
                    QString type=""/*ListeTypesVariables.at(i)*/;
		    code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		    if (type=="LISTE") 
		      {
		      //code+=nomvariable+"["+rang+"]=ALGOBOX_ARRONDI("+contenu+");\n";
		     // if (!exporthtml) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}\n";
		      code+=nomvariable+"_temp_tab=\""+contenu+"\".split(new RegExp(\"[:]+\", \"g\"));\n";
		      code+="for ("+nomvariable+"_temp_tab_compteur=0;"+nomvariable+"_temp_tab_compteur<"+nomvariable+"_temp_tab.length;"+nomvariable+"_temp_tab_compteur++)\n{\n";
		      code+="if (ALGOBOX_ARRONDI(eval("+nomvariable+"_temp_tab["+nomvariable+"_temp_tab_compteur]))==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      code+=nomvariable+"["+rang+"+"+nomvariable+"_temp_tab_compteur]=ALGOBOX_ARRONDI(eval("+nomvariable+"_temp_tab["+nomvariable+"_temp_tab_compteur]));\n}\n";
		      code+="while ("+nomvariable+"_temp_tab.length>0) "+nomvariable+"_temp_tab.pop();\n";
		      //code+="if (ALGOBOX_ARRONDI("+contenu+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      //code+=nomvariable+"["+rang+"]=ALGOBOX_ARRONDI("+contenu+");\n";
		      if (!exporthtml) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}\n";
		      }
		    else if (type=="NOMBRE") 
		      {
		      //code+=nomvariable+"=ALGOBOX_ARRONDI("+contenu+");\n";
		      //if (!exporthtml) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}\n";
		      code+="if (ALGOBOX_ARRONDI("+contenu+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      code+=nomvariable+"=ALGOBOX_ARRONDI("+contenu+");\n";
		      if (!exporthtml) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}\n";
		      }
		    else 
		      {
		      code+=nomvariable+"="+contenu+";\n";
		      if (!exporthtml) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}";
		      }
		    }
		}
	    break;
    case 6: //SI
	    if (parametres.count()==2)
		{
		QString condition=FiltreCondition(parametres.at(1));
		condition=FiltreCalcul(condition);
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
/*		if ((!repereDefini) && (ui.checkBoxRepere->isChecked()))
		  {
		  code+="\ndocument.getElementById(\"contentgraphic\").style.visibility=\"visible\";\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradX->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradY->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="ALGOBOX_REPERE("+FiltreCalcul(ui.lineEditXmin->text())+","+FiltreCalcul(ui.lineEditXmax->text())+","+FiltreCalcul(ui.lineEditYmin->text())+","+FiltreCalcul(ui.lineEditYmax->text())+","+FiltreCalcul(ui.lineEditGradX->text())+","+FiltreCalcul(ui.lineEditGradY->text())+");\n";
		  repereDefini=true;
		  }*/
		if (!exporthtml)
		  {
		  code+="\nif (ALGOBOX_PAS_A_PAS)\n";
		  code+="{\n";
		  code+="if ("+condition+") {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("La condition est vérifiée")+"\")) {throw(\"erreur_pause\");}}";
		  code+="else {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("La condition n'est pas vérifiée")+"\")) {throw(\"erreur_pause\");}}";
		  code+="}\n";
		  }
		code+="if ("+condition+")";
		}
	    break;
    case 7: //DEBUT_SI
	    if (parametres.count()==2)
		{
		code="{\n";
		code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		if (!exporthtml) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Entrée dans le bloc DEBUT_SI/FIN_SI")+"\")) {throw(\"erreur_pause\");}}";
		}
	    break;
    case 8: //FIN_SI
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		if (!exporthtml) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Sortie du bloc DEBUT_SI/FIN_SI")+"\")) {throw(\"erreur_pause\");}}";
		code+="}";
		}
	    break;
    case 9: //SINON
	    if (parametres.count()==2)
		{
		code="else";
		}
	    break;
    case 10: //DEBUT_SINON
	    if (parametres.count()==2)
		{
		code="{\n";
		code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
	    	if (!exporthtml) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Entrée dans le bloc DEBUT_SINON/FIN_SINON")+"\")) {throw(\"erreur_pause\");}}";
		}
	    break;
    case 11: //FIN_SINON
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
	    	if (!exporthtml) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Sortie du bloc DEBUT_SINON/FIN_SINON")+"\")) {throw(\"erreur_pause\");}}";
		code+="}";
		}
	    break;
    case 12: //POUR
	    if (parametres.count()==4)
		{
		QString nomvariable=parametres.at(1);
		QString debut=FiltreCalcul(parametres.at(2));
		QString fin=FiltreCalcul(parametres.at(3));
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent);
		code=varboucle+"=0;\n";
		code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
/*		if ((!repereDefini) && (ui.checkBoxRepere->isChecked()))
		  {
		  code+="\ndocument.getElementById(\"contentgraphic\").style.visibility=\"visible\";\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradX->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradY->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="ALGOBOX_REPERE("+FiltreCalcul(ui.lineEditXmin->text())+","+FiltreCalcul(ui.lineEditXmax->text())+","+FiltreCalcul(ui.lineEditYmin->text())+","+FiltreCalcul(ui.lineEditYmax->text())+","+FiltreCalcul(ui.lineEditGradX->text())+","+FiltreCalcul(ui.lineEditGradY->text())+");\n";
		  repereDefini=true;
		  }*/
		code+="for ("+nomvariable+"="+debut+";"+nomvariable+"<="+fin+";"+nomvariable+"++)";
		}
	    break;
    case 13: //DEBUT_POUR
	    if (parametres.count()==2)
		{
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent-1);
		code="{\n"+varboucle+"++;\n";
		code+="ALGOBOX_COMPTEUR_BOUCLE_GLOBAL++;\n";
	        code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
	    	if (!exporthtml) code+="if (ALGOBOX_PAS_A_PAS) {ALGOBOX_AFFICHE_PASAPAS(\""+QString::fromUtf8("Entrée dans le bloc DEBUT_POUR/FIN_POUR")+" (ligne "+QString::number(id)+")\",true); if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}";
		}
	    break;
    case 14: //FIN_POUR
	    if (parametres.count()==2)
		{
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent-1);
	        code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		if (!exporthtml) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Sortie du bloc DEBUT_POUR/FIN_POUR")+"\")) {throw(\"erreur_pause\");}}";
		code+="if (ALGOBOX_DEPASSEMENT_AFFICHAGE) {throw(\"erreur_affiche\"); break;}\n";
		code+="if ("+varboucle+">500000) {throw(\"erreur_boucle\"); break;}\n";
	    	if (!exporthtml) code+="if (ALGOBOX_COMPTEUR_BOUCLE_GLOBAL%10000==0) ALGOBOX_PING_OUTPUT();\n";
	    	code+="if (ALGOBOX_COMPTEUR_BOUCLE_GLOBAL>5000000) {throw(\"erreur_boucle\"); break;}\n}";
		}
	    break;
    case 15: //TANT_QUE
	    if (parametres.count()==2)
		{
		QString condition=FiltreCondition(parametres.at(1));
		condition=FiltreCalcul(condition);
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent);
		code=varboucle+"=0;\n";
		code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
// 		if (!exporthtml)
// 		  {
// 		  code+="\nif (ALGOBOX_PAS_A_PAS)\n";
// 		  code+="{\n";
// 		  code+="if ("+condition+") {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("La condition est vérifiée")+"\")) {throw(\"erreur_pause\");}}";
// 		  code+="else {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("La condition n'est pas vérifiée")+"\")) {throw(\"erreur_pause\");}}";
// 		  code+="}\n";
// 		  }
/*		if ((!repereDefini) && (ui.checkBoxRepere->isChecked()))
		  {
		  code+="\ndocument.getElementById(\"contentgraphic\").style.visibility=\"visible\";\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradX->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradY->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="ALGOBOX_REPERE("+FiltreCalcul(ui.lineEditXmin->text())+","+FiltreCalcul(ui.lineEditXmax->text())+","+FiltreCalcul(ui.lineEditYmin->text())+","+FiltreCalcul(ui.lineEditYmax->text())+","+FiltreCalcul(ui.lineEditGradX->text())+","+FiltreCalcul(ui.lineEditGradY->text())+");\n";
		  repereDefini=true;
		  }*/
		code+="while ("+condition+")";
		}
	    break;
    case 16: //DEBUT_TANT_QUE
	    if (parametres.count()==2)
		{
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent-1);
		code="{\n"+varboucle+"++;\n";
	    	code+="ALGOBOX_COMPTEUR_BOUCLE_GLOBAL++;";
	        code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		if (!exporthtml) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Entrée dans le bloc DEBUT_TANT_QUE/FIN_TANT_QUE : condition vérifiée")+"\")) {throw(\"erreur_pause\");}}";
		}
	    break;
    case 17: //FIN_TANT_QUE
	    if (parametres.count()==2)
		{
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent-1);
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		if (!exporthtml) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Sortie du bloc DEBUT_TANT_QUE/FIN_TANT_QUE")+"\")) {throw(\"erreur_pause\");}}";
	    	code+="if (ALGOBOX_DEPASSEMENT_AFFICHAGE) {throw(\"erreur_affiche\"); break;}\n";
		code+="if ("+varboucle+">500000) {throw(\"erreur_boucle\"); break;}\n";
	    	if (!exporthtml) code+="if (ALGOBOX_COMPTEUR_BOUCLE_GLOBAL%10000==0) ALGOBOX_PING_OUTPUT();\n";
	    	code+="if (ALGOBOX_COMPTEUR_BOUCLE_GLOBAL>5000000) {throw(\"erreur_boucle\"); break;}\n}";
		}
	    break;
    case 18: //PAUSE
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
	    	code+="if (!ALGOBOX_PAUSE()) {throw(\"erreur_pause\");}\n";
		}
	    break;
    case 19: //COMMENTAIRE
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		code+="//***************";
		}
	    break;
    case 50: //POINT
	    if ((parametres.count()==4) && (ui.checkBoxRepere->isChecked()))
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		//if (!repereDefini)
		  //{
		  code+="\nif (!ALGOBOX_REPERE_DEFINI) {"; 
		  code+="\ndocument.getElementById(\"contentgraphic\").style.visibility=\"visible\";\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradX->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradY->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="ALGOBOX_REPERE("+FiltreCalcul(ui.lineEditXmin->text())+","+FiltreCalcul(ui.lineEditXmax->text())+","+FiltreCalcul(ui.lineEditYmin->text())+","+FiltreCalcul(ui.lineEditYmax->text())+","+FiltreCalcul(ui.lineEditGradX->text())+","+FiltreCalcul(ui.lineEditGradY->text())+");\n";
		  code+="}\n";
		  //repereDefini=true;
		  //}
		QString x=FiltreCalcul(parametres.at(1));
		QString y=FiltreCalcul(parametres.at(2));
		QString col="2";
		if (parametres.at(3)=="Rouge") col="3";
		else if (parametres.at(3)=="Vert") col="4";
		code+="if (ALGOBOX_ARRONDI("+x+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		code+="if (ALGOBOX_ARRONDI("+y+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		if (!exporthtml) 
		  {
		  code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Tracé du point (")+"\"";
		  code+="+ALGOBOX_ARRONDI("+x+")+\",\"";
		  code+="+ALGOBOX_ARRONDI("+y+")+\")\"))"; 
		  code+="{throw(\"erreur_pause\");}}\n";
		  }
		code+="ALGOBOX_POINT("+x+","+y+","+col+");";
		}
	    break;
    case 51: //SEGMENT
	    if ((parametres.count()==6) && (ui.checkBoxRepere->isChecked()))
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		//if (!repereDefini)
		  //{
		  code+="\nif (!ALGOBOX_REPERE_DEFINI) {"; 
		  code+="\ndocument.getElementById(\"contentgraphic\").style.visibility=\"visible\";\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradX->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradY->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="ALGOBOX_REPERE("+FiltreCalcul(ui.lineEditXmin->text())+","+FiltreCalcul(ui.lineEditXmax->text())+","+FiltreCalcul(ui.lineEditYmin->text())+","+FiltreCalcul(ui.lineEditYmax->text())+","+FiltreCalcul(ui.lineEditGradX->text())+","+FiltreCalcul(ui.lineEditGradY->text())+");\n";
		  code+="}\n";
		  //repereDefini=true;
		  //}
		QString xdep=FiltreCalcul(parametres.at(1));
		QString ydep=FiltreCalcul(parametres.at(2));
		QString xfin=FiltreCalcul(parametres.at(3));
		QString yfin=FiltreCalcul(parametres.at(4));
		QString coul="2";
		if (parametres.at(5)=="Rouge") coul="3";
		else if (parametres.at(5)=="Vert") coul="4";
		code+="if (ALGOBOX_ARRONDI("+xdep+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		code+="if (ALGOBOX_ARRONDI("+ydep+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		code+="if (ALGOBOX_ARRONDI("+xfin+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		code+="if (ALGOBOX_ARRONDI("+yfin+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		if (!exporthtml) 
		  {
		  code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Tracé du segment (")+"\"";
		  code+="+ALGOBOX_ARRONDI("+xdep+")+\",\"";
		  code+="+ALGOBOX_ARRONDI("+ydep+")+\",\"";
		  code+="+ALGOBOX_ARRONDI("+xfin+")+\",\"";
		  code+="+ALGOBOX_ARRONDI("+yfin+")+\")\"))"; 
		  code+="{throw(\"erreur_pause\");}}\n";
		  }
		code+="ALGOBOX_TRAIT("+xdep+","+ydep+","+xfin+","+yfin+","+coul+");";
		}
	    break;
    case 100: //DECLARATIONS VARIABLES
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\n";
		code+="//VARIABLES";
		}
	    break;
    case 101: //DEBUT_ALGO
	    if (parametres.count()==2)
		{
		code="function F1(x)\n{\n";
		if (ui.checkBoxFonction->isChecked())
			{
			QString expression=ui.lineEditFonction->text();
			if (!expression.isEmpty() && !expression.contains("F1(",Qt::CaseInsensitive)) code+="return "+FiltreCalcul(expression)+";\n";
			else code+="return x;\n";
			}
		else code+="return x;\n";
		code+="}\n";
		if (ui.checkBoxF2->isChecked())
			{
			QString role,condition,commande;
			QStringList tagList;
			QListWidgetItem *item;
			code+="function F2("+ui.lineEditParametresF2->text()+")\n{\n";
			code+="ALGOBOX_COMPTEUR_RECURSION++;\n";
			code+="if (ALGOBOX_COMPTEUR_RECURSION>100000) return \"Erreur\";\n";
			for (int i = 0; i < ui.listWidgetF2->count(); ++i)
			  {
			  tagList.clear();
			  item=ui.listWidgetF2->item(i);
			  role=item->data(Qt::UserRole).toString();
			  tagList= role.split("@");
			  if (tagList.count()==2) 
			    {
			    condition=tagList.at(0);
			    commande=tagList.at(1);
			    if ((!condition.isEmpty()) && (!commande.isEmpty()))
			      {
			      condition=FiltreCondition(condition);
			      condition=FiltreCalcul(condition);
			      commande=FiltreCalcul(commande);
			      code+="if ("+condition+") return "+commande+";\n";
			      }
			    }
			  }
			  if (ui.lineEditDefautF2->text().isEmpty()) code+="return 1;\n";
			  else code+="return "+ui.lineEditDefautF2->text()+";\n";
			  code+="}\n";
			  }
		code+="function ALGOBOX_ALGO(ALGOBOX_PAS_A_PAS)\n{\n";
		code+="ALGOBOX_DESACTIVER_LIGNE(ALGOBOX_LIGNE_COURANTE);\n";
		code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\n";
		code+="ALGOBOX_COMPTEUR_BOUCLE_GLOBAL=0\n";
		code+="ALGOBOX_COMPTEUR_RECURSION=0\n";
		code+="ALGOBOX_EFFACE_OUTPUT();\n";
		code+="ALGOBOX_INIT_VARIABLES();\n";
		code+="ALGOBOX_REPERE_DEFINI=false;\n";
		if (ui.checkBoxRepere->isChecked())
		    {
		    code+="output_graph=document.getElementById(\"graphique\");\n";
		    code+="if (output_graph.firstChild) output_graph.removeChild(output_graph.firstChild);\n";
		    code+="output_graph.appendChild(ALGOBOX_canevas);\n";
		    code+="if (navigator.appName!=\"Microsoft Internet Explorer\") ALGOBOX_contexte.clearRect(0, 0, ALGOBOX_canevas.width,ALGOBOX_canevas.height);\n";
		    }
		if (!exporthtml) code+="browserDialog.scriptLaunched();\n";
		code+="if (ALGOBOX_PAS_A_PAS) ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme lancé en mode pas à pas***")+"\",true);\nelse ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme lancé***")+"\",true);\n";    
		code+="try\n{";
		}
	    break;
    case 102: //FIN_ALGO
	    if (parametres.count()==2)
		{
		  code="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\nALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme terminé***")+"\",false);\n";
		  if (!exporthtml) code+="browserDialog.scriptFinished();\n";
		  code+="}\ncatch(ALGOBOX_MESSAGE)\n{\n";
		  //erreur_input
		  code+="if (ALGOBOX_MESSAGE==\"erreur_input\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8(" : erreur dans la lecture d'une variable ***")+"\",false);\n";
		  code+="}\n";
		  //erreur_calcul
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_calcul\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8(" : erreur de calcul***")+"\",false);\n";
		  code+="}\n";
		  //erreur_boucle
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_boucle\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8(" : dépassement de la capacité autorisée pour les boucles***")+"\",false);\n";
		  code+="}\n";
		  //erreur_affiche
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_affiche\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8(" : affichage trop important de données***")+"\",false);\n";
		  code+="}\n";
		  //erreur_repere
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_repere\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu : erreur dans la définition du repère graphique***")+"\",false);\n";
		  code+="}\n";
		  //erreur_pause
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_pause\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8("***")+"\",false);\n";
		  code+="}\n";
		  //erreur_emergency_stop
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_emergency\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu sur demande***")+"\",false);\n";
		  code+="}\n";
		  //erreur inconnue
		  code+="else\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8(" suite à une erreur dans son exécution***")+"\",false);\n";
		  code+="}\n";  
		  //
		  code+="ALGOBOX_ACTIVER_LIGNE(ALGOBOX_LIGNE_COURANTE);\n";
		  code+="window.location.href = \"#\"+ALGOBOX_LIGNE_COURANTE;\n";
		  if (!exporthtml) code+="\nbrowserDialog.scriptFinished();\n";
		  code+="\n}\n}";		  
		}
	    break;
    case 103: //autres
    default:
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		code+="//***************";
		}
	    break;
    }
  }
return code;
}

QString MainWindow::FiltreNomVariable(QString orig)
{
QStringList motcle=QString("*,+,-,/,%,(,),{,},[,],;,:,!,',\",&,|,!,:,^,#").split(",");
QString result=orig.trimmed();
for (int i = 0; i < motcle.count(); i++)
    {
    result=result.remove(motcle.at(i));
    }
result.remove(",");
result.replace(" ","_");
return result;
}

QString MainWindow::FiltreCalcul(QString orig)
{
QStringList motcle=QString("acos(,asin(,atan(,sqrt(,pow(,random(,floor(,exp(,log(,round(,max(,min(,abs(").split(",");
QString result=orig;
for (int i = 0; i < motcle.count(); i++)
    {
    if (result.contains(motcle.at(i),Qt::CaseInsensitive)) 
      {
      result=result.replace(motcle.at(i),"Math."+motcle.at(i),Qt::CaseInsensitive);
      }
    }
//qDebug() << orig << result;
QStringList motclebis=QString("cos(,sin(,tan(").split(",");
int leftPos;
for (int i = 0; i < motclebis.count(); i++)
    {
    leftPos = result.indexOf(motclebis.at(i),Qt::CaseInsensitive);
    while ( leftPos != -1 ) 
      {
      if (leftPos==0) 
	{
	result=result.replace(leftPos,motclebis.at(i).length(),"Math."+motclebis.at(i));
	leftPos = result.indexOf(motclebis.at(i), leftPos+5+motclebis.at(i).length(),Qt::CaseInsensitive );
	}
      else if (result.at(leftPos-1)!='a')
	{
	result=result.replace(leftPos,motclebis.at(i).length(),"Math."+motclebis.at(i));
	leftPos = result.indexOf(motclebis.at(i), leftPos+5+motclebis.at(i).length(),Qt::CaseInsensitive );
	}
      else leftPos = result.indexOf(motclebis.at(i), leftPos+1,Qt::CaseInsensitive );
      }
    }
//qDebug() << orig << result;
return result;
}

QString MainWindow::FiltreCondition(QString orig)
{
QString result=orig;
result.replace(" OU "," || ",Qt::CaseInsensitive);
result.replace(" ET "," && ",Qt::CaseInsensitive);
return result;
}

QString MainWindow::CodeNoeud(QTreeWidgetItem *item,bool exporthtml)
{
QString code="";
if (!item) return code;
idligne++;
code=CodeVersJavascript(item->data(0,Qt::UserRole).toString(),exporthtml,idligne)+"\n";
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	indent+=1;
	for (int i = 0; i < nb_branches; i++) code+=CodeNoeud(item->child(i),exporthtml);
	indent-=1;
	}
return code;
}

QString MainWindow::AlgoNoeud(QTreeWidgetItem *item)
{
QString code="";
for (int i = 0; i < indent; i++) code+="&nbsp;&nbsp;";
if (!item) return code;
idligne++;
QString ligne=item->text(0);
ligne.replace(QString("<"),QString("&lt;"));
ligne.replace(QString(">"),QString("&gt;"));
if (ligne.startsWith("//",Qt::CaseInsensitive)) ligne="<i>"+ligne+"</i>";
code+="<a name=\""+QString::number(idligne)+"\">&nbsp;</a><span id=\"ligne"+QString::number(idligne)+"\" >"+ligne+"</span><br>\n";
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	indent+=1;
	for (int i = 0; i < nb_branches; i++) code+=AlgoNoeud(item->child(i));
	indent-=1;
	}
return code;
}

QString MainWindow::AlgoNoeudTexte(QTreeWidgetItem *item)
{
QString code="";
for (int i = 0; i < indent; i++) code+="  ";
if (!item) return code;
QString texte=item->text(0);
code+=texte+"\n";
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	indent+=1;
	for (int i = 0; i < nb_branches; i++) code+=AlgoNoeudTexte(item->child(i));
	indent-=1;
	}
return code;
}

QString MainWindow::AlgoNoeudCode(QTreeWidgetItem *item)
{
QString code="";
for (int i = 0; i < indent; i++) code+="  ";
if (!item) return code;
QString texte=item->text(0);
QString data=item->data(0,Qt::UserRole).toString();
QStringList parametres=data.split("#");
QRegExp rxaffich("AFFICHER\\s+(.*)",Qt::CaseInsensitive);
QRegExp rxpoint("TRACER_POINT\\s+(.*)",Qt::CaseInsensitive);
QRegExp rxsegment("TRACER_SEGMENT\\s+(.*)",Qt::CaseInsensitive);
if (rxaffich.indexIn(texte)>-1)
  {
  if ((parametres.count()==4) || (parametres.count()==3))
      {
      if (parametres.at(2)=="1") code+="AFFICHER* "+rxaffich.cap(1)+"\n";
      else code+=texte+"\n";
      }
  }
else if (rxpoint.indexIn(texte)>-1)
  {
  if (parametres.count()==4)
    {
    code+="TRACER_POINT_"+parametres.at(3)+" "+rxpoint.cap(1)+"\n";
    }
  }
else if (rxsegment.indexIn(texte)>-1)
  {
  if (parametres.count()==6)
    {
    code+="TRACER_SEGMENT_"+parametres.at(5)+" "+rxsegment.cap(1)+"\n";    
    }
  }
else code+=texte+"\n";
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	indent+=1;
	for (int i = 0; i < nb_branches; i++) code+=AlgoNoeudCode(item->child(i));
	indent-=1;
	}
return code;
}

void MainWindow::JavascriptExport()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible de tester l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString htmlfichier=QDir::homePath();
htmlfichier="algobox_temp_"+htmlfichier.section('/',-1);
htmlfichier=QString(QUrl::toPercentEncoding(htmlfichier));
htmlfichier.remove("%");
htmlfichier=htmlfichier+".html";
QString tempDir=QDir::tempPath();
htmlfichier=tempDir+"/"+htmlfichier;
QFile fichier;
fichier.setFileName(htmlfichier);
fichier.open(QIODevice::WriteOnly);
QTextStream out (&fichier);
out.setCodec(codec);
out << GenererCode(false);
fichier.close();
QFileInfo fic(htmlfichier);
BrowserDialog *browserDialog = new BrowserDialog(this,htmlfichier,blackconsole);
if (fic.exists() && fic.isReadable() )
	{
	
	browserDialog->resize(browserwidth,browserheight);
	if (browserDialog->exec())
	  {
	  browserwidth=browserDialog->width();
	  browserheight=browserDialog->height();
	  }
	}
if (browserDialog) delete browserDialog;
}

void MainWindow::ExporterVersTexte()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'exporter l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString texte="";
QFileInfo fic(nomFichier);
if (nomFichier!="sanstitre") texte=fic.baseName();
else texte=nomFichier;
texte+="  -  "; 
QDate CurrDate = QDate::currentDate();
texte+=CurrDate.toString("dd.MM.yyyy");
texte+="\n\n";
texte+="******************************************\n";
texte+=ui.textEditDescription->toPlainText()+"\n";
texte+="******************************************\n\n";
QString code="";
int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches>0)
    {
    indent=0;
    for (int i = 0; i < nb_branches; i++) 
	{
	code+=AlgoNoeudTexte(ui.treeWidget->topLevelItem(i));
	}
    }
QTextStream t(&code,QIODevice::ReadOnly);
t.setCodec(codec);
int ligne=1;
QString num="";
QString blanc="    ";
while (!t.atEnd()) 
	{
	num=QString::number(ligne);
	num+=blanc.left(blanc.length()-num.length());
	texte+=num+t.readLine()+"\n";
	ligne++;
	}
if (ui.checkBoxFonction->isChecked())
	{
	QString expression=ui.lineEditFonction->text();
	if (!expression.isEmpty() && !expression.contains("F1(",Qt::CaseInsensitive)) texte+=QString::fromUtf8("\nFonction numérique utilisée :\nF1(x)=")+expression+"\n";
	}
if (ui.checkBoxF2->isChecked())
	{
	if (ui.checkBoxFonction->isChecked()) texte+="\n";
	QString role,condition,commande;
	QStringList tagList;
	QListWidgetItem *item;
	texte+="\nfonction F2("+ui.lineEditParametresF2->text()+"):\n";
	for (int i = 0; i < ui.listWidgetF2->count(); ++i)
	  {
	  tagList.clear();
	  item=ui.listWidgetF2->item(i);
	  role=item->data(Qt::UserRole).toString();
	  tagList= role.split("@");
	  if (tagList.count()==2) 
	    {
	    condition=tagList.at(0);
	    commande=tagList.at(1);
	    if ((!condition.isEmpty()) && (!commande.isEmpty()))
	      {
	      texte+="SI ("+condition+") RENVOYER "+commande+"\n";
	      }
	    }
	  }
	  if (!ui.lineEditDefautF2->text().isEmpty()) texte+=QString::fromUtf8("Dans les autres cas, RENVOYER ")+ui.lineEditDefautF2->text()+"\n";
	  }
QString exportFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),dernierRepertoire,QString::fromUtf8("Fichier texte (*.txt)"));
if (!exportFichier.isEmpty()) 
    {
    QFileInfo fi(exportFichier);
    dernierRepertoire=fi.absolutePath();
    QFile fichier;
    fichier.setFileName(exportFichier);
    fichier.open(QIODevice::WriteOnly);
    QTextStream out (&fichier);
    out.setCodec(codec);
    out << texte;
    fichier.close();
    }
}

void MainWindow::ExporterVersODF()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'exporter l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QFontDatabase fdb;
QStringList xf = fdb.families();
QString deft;
if (xf.contains("Liberation Mono",Qt::CaseInsensitive)) deft="Liberation Mono";
else if (xf.contains("DejaVu Sans Mono",Qt::CaseInsensitive)) deft="DejaVu Sans Mono";
#if defined( Q_WS_MACX )
else if (xf.contains("Courier",Qt::CaseInsensitive)) deft="Courier";
#endif
#if defined(Q_WS_WIN)
else if (xf.contains("Courier New",Qt::CaseInsensitive)) deft="Courier New";
#endif
else deft=qApp->font().family();
QTextCharFormat output_format;
//output_format.setFont(QFont(deft,10));  //bug Qt 4.5
output_format.setFontFamily(deft);
QTextDocument *document =new QTextDocument(this);
document->setDefaultFont(QFont(deft,9));
QTextCursor cursor(document);
QString texte="";
QFileInfo fic(nomFichier);
if (nomFichier!="sanstitre") texte=fic.baseName();
else texte=nomFichier;
texte+="  -  "; 
QDate CurrDate = QDate::currentDate();
texte+=CurrDate.toString("dd.MM.yyyy");
texte+="\n\n";
texte+=ui.textEditDescription->toPlainText()+"\n\n";
QString code="";
int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches>0)
    {
    indent=0;
    for (int i = 0; i < nb_branches; i++) 
	{
	code+=AlgoNoeudTexte(ui.treeWidget->topLevelItem(i));
	}
    }
QTextStream t(&code,QIODevice::ReadOnly);
t.setCodec(codec);
int ligne=1;
QString num="";
QString blanc="    ";
while (!t.atEnd()) 
	{
	num=QString::number(ligne);
	num+=blanc.left(blanc.length()-num.length());
	texte+=num+t.readLine()+"\n";
	ligne++;
	}
if (ui.checkBoxFonction->isChecked())
	{
	QString expression=ui.lineEditFonction->text();
	if (!expression.isEmpty() && !expression.contains("F1(",Qt::CaseInsensitive)) texte+=QString::fromUtf8("\nFonction numérique utilisée :\nF1(x)=")+expression+"\n";
	}
if (ui.checkBoxF2->isChecked())
	{
	if (ui.checkBoxFonction->isChecked()) texte+="\n";
	QString role,condition,commande;
	QStringList tagList;
	QListWidgetItem *item;
	texte+="\nfonction F2("+ui.lineEditParametresF2->text()+"):\n";
	for (int i = 0; i < ui.listWidgetF2->count(); ++i)
	  {
	  tagList.clear();
	  item=ui.listWidgetF2->item(i);
	  role=item->data(Qt::UserRole).toString();
	  tagList= role.split("@");
	  if (tagList.count()==2) 
	    {
	    condition=tagList.at(0);
	    commande=tagList.at(1);
	    if ((!condition.isEmpty()) && (!commande.isEmpty()))
	      {
	      texte+="SI ("+condition+") RENVOYER "+commande+"\n";
	      }
	    }
	  }
	  if (!ui.lineEditDefautF2->text().isEmpty()) texte+=QString::fromUtf8("Dans les autres cas, RENVOYER ")+ui.lineEditDefautF2->text()+"\n";
	  }
cursor.mergeCharFormat(output_format);
cursor.insertText(texte);

QString exportFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),dernierRepertoire,QString::fromUtf8("Fichier ODF (*.odt)"));
if (!exportFichier.isEmpty()) 
    {
    QFileInfo fi(exportFichier);
    dernierRepertoire=fi.absolutePath();
    QTextDocumentWriter fich(exportFichier);
    fich.setFormat("odf");
    fich.setCodec(codec);
    fich.write(document);
    }
}


void MainWindow::ExporterVersLatex()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'exporter l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
int query= QMessageBox::question(this,"AlgoBox : ", QString::fromUtf8("Quel encodage voulez-vous utiliser?"),QString::fromUtf8("UTF-8"), QString::fromUtf8("ISO-8859-1"),QString::fromUtf8("Abandon"),0,2 );
if (query==2) return;
QString texte="";
QTextCodec *readcodec = QTextCodec::codecForName("UTF-8");
QFile texfile(":/documents/modelelatex.txt");
texfile.open(QIODevice::ReadOnly);
QTextStream t(&texfile);
t.setCodec(readcodec);
while (!t.atEnd()) 
	{
	texte+= t.readLine()+"\n";
	}
texfile.close();
QDate CurrDate = QDate::currentDate();
QString date=CurrDate.toString("dd.MM.yyyy");
QString code="";
int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches>0)
    {
    indent=0;
    for (int i = 0; i < nb_branches; i++) 
	{
	code+=AlgoNoeudTexte(ui.treeWidget->topLevelItem(i));
	}
    }

QTextStream tnum(&code,QIODevice::ReadOnly);
tnum.setCodec(readcodec);
int ligne=1;
QString num="";
QString blanc="    ";
QString codenum="";
while (!tnum.atEnd()) 
	{
	num=QString::number(ligne);
	num+=blanc.left(blanc.length()-num.length());
	codenum+=num+tnum.readLine()+"\n";
	ligne++;
	}
QString expression=ui.lineEditFonction->text();
if ((ui.checkBoxFonction->isChecked()) && (!expression.isEmpty()) && (!expression.contains("F1(",Qt::CaseInsensitive)))
	{
	codenum+="\n"+QString::fromUtf8("Fonction numérique utilisée : F1(x)=")+expression;
	}
if (ui.checkBoxF2->isChecked())
	{
	if (ui.checkBoxFonction->isChecked()) texte+="\n";
	QString role,condition,commande;
	QStringList tagList;
	QListWidgetItem *item;
	codenum+="\nfonction F2("+ui.lineEditParametresF2->text()+"):\n";
	for (int i = 0; i < ui.listWidgetF2->count(); ++i)
	  {
	  tagList.clear();
	  item=ui.listWidgetF2->item(i);
	  role=item->data(Qt::UserRole).toString();
	  tagList= role.split("@");
	  if (tagList.count()==2) 
	    {
	    condition=tagList.at(0);
	    commande=tagList.at(1);
	    if ((!condition.isEmpty()) && (!commande.isEmpty()))
	      {
	      codenum+="SI ("+condition+") RENVOYER "+commande+"\n";
	      }
	    }
	  }
	  if (!ui.lineEditDefautF2->text().isEmpty()) codenum+=QString::fromUtf8("Dans les autres cas, RENVOYER ")+ui.lineEditDefautF2->text()+"\n";
	  }
QFileInfo fic(nomFichier);
if (nomFichier!="sanstitre") texte.replace("#TITRE#",fic.baseName());
else texte.replace("#TITRE#",nomFichier);

texte.replace("#DATE#",date);
QString descr=ui.textEditDescription->toPlainText();
descr.replace("%","\\%");
descr.replace("$","\\$");
descr.replace("_","\\_");
texte.replace("#DESCRIPTION#",descr);
texte.replace("#CODE#",codenum);

QTextCodec *codec;
if (query==1)
    {
    codec = QTextCodec::codecForName("ISO-8859-1");
    texte.replace("#ENCODAGE#","\\usepackage[latin1]{inputenc}");
    }
else
    {
    codec = QTextCodec::codecForName("UTF-8");
    texte.replace("#ENCODAGE#","\\usepackage[utf8x]{inputenc}\n\\usepackage{ucs}");
    }

QString exportFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),dernierRepertoire,QString::fromUtf8("Document LaTeX (*.tex)"));
if (!exportFichier.isEmpty()) 
    {
    QFileInfo fi(exportFichier);
    dernierRepertoire=fi.absolutePath();
    QFile fichier;
    fichier.setFileName(exportFichier);
    fichier.open(QIODevice::WriteOnly);
    QTextStream out (&fichier);
    out.setCodec(codec);
    out << texte;
    fichier.close();
    }
}

void MainWindow::ExporterVersHtml()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'exporter l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString htmlfichier=QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),dernierRepertoire,QString::fromUtf8("Page web (*.html)"));
if (!htmlfichier.isEmpty()) 
    {
    QFile fichier;
    fichier.setFileName(htmlfichier);
    fichier.open(QIODevice::WriteOnly);
    QTextStream out (&fichier);
    out.setCodec(codec);
    out << GenererCode(true);
    fichier.close();
    }
}

void MainWindow::Imprimer()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'imprimer l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString texte="";
QFileInfo fic(nomFichier);
if (nomFichier!="sanstitre") texte=fic.baseName();
else texte=nomFichier;
texte+="  -  "; 
QDate CurrDate = QDate::currentDate();
texte+=CurrDate.toString("dd.MM.yyyy");
texte+="\n\n";
texte+="******************************************\n";
texte+=ui.textEditDescription->toPlainText()+"\n";
texte+="******************************************\n\n";
QString code="";
int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches>0)
    {
    indent=0;
    for (int i = 0; i < nb_branches; i++) 
	{
	code+=AlgoNoeudTexte(ui.treeWidget->topLevelItem(i));
	}
    }
QTextStream t(&code,QIODevice::ReadOnly);
t.setCodec(codec);
int ligne=1;
QString num="";
QString blanc="    ";
while (!t.atEnd()) 
	{
	num=QString::number(ligne);
	num+=blanc.left(blanc.length()-num.length());
	texte+=num+t.readLine()+"\n";
	ligne++;
	}	
if (ui.checkBoxFonction->isChecked())
	{
	QString expression=ui.lineEditFonction->text();
	if (!expression.isEmpty() && !expression.contains("F1(",Qt::CaseInsensitive)) texte+=QString::fromUtf8("\nFonction numérique utilisée :\nF1(x)=")+expression+"\n";
	}
if (ui.checkBoxF2->isChecked())
	{
	if (ui.checkBoxFonction->isChecked()) texte+="\n";
	QString role,condition,commande;
	QStringList tagList;
	QListWidgetItem *item;
	texte+="\nfonction F2("+ui.lineEditParametresF2->text()+"):\n";
	for (int i = 0; i < ui.listWidgetF2->count(); ++i)
	  {
	  tagList.clear();
	  item=ui.listWidgetF2->item(i);
	  role=item->data(Qt::UserRole).toString();
	  tagList= role.split("@");
	  if (tagList.count()==2) 
	    {
	    condition=tagList.at(0);
	    commande=tagList.at(1);
	    if ((!condition.isEmpty()) && (!commande.isEmpty()))
	      {
	      texte+="SI ("+condition+") RENVOYER "+commande+"\n";
	      }
	    }
	  }
	  if (!ui.lineEditDefautF2->text().isEmpty()) texte+=QString::fromUtf8("Dans les autres cas, RENVOYER ")+ui.lineEditDefautF2->text()+"\n";
	  }
QPrinter printer;
QPrintDialog *dlg = new QPrintDialog(&printer, this);
if (dlg->exec() != QDialog::Accepted) return;
QTextDocument *document =new QTextDocument(texte);
QFontDatabase fdb;
QStringList xf = fdb.families();
QString deft;
if (xf.contains("DejaVu Sans Mono",Qt::CaseInsensitive)) deft="DejaVu Sans Mono";
else if (xf.contains("Liberation Mono",Qt::CaseInsensitive)) deft="Liberation Mono";
#if defined( Q_WS_MACX )
else if (xf.contains("Courier",Qt::CaseInsensitive)) deft="Courier";
#endif
#if defined(Q_WS_WIN)
else if (xf.contains("Courier New",Qt::CaseInsensitive)) deft="Courier New";
#endif
else deft=qApp->font().family();
QFont printFont (deft,8);
document->setDefaultFont(printFont);
document->print(&printer);
}

//********************************
void MainWindow::NouvelAlgo()
{
if (estModifie) 
	{
	switch(  QMessageBox::warning(this,QString::fromUtf8("AlgoBox : "),
					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant d'en créer un nouveau?"),
					QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
					0,
					2 ) )
		{
		case 0:
		SauverAlgo();
		break;
		case 1:
		break;
		case 2:
		default:
		return;
		break;
		}
	}
Init();
ActualiserArbre();
estVierge=true;
estModifie=false;
nomFichier="sanstitre";
setWindowTitle(QString("AlgoBox "ALGOBOXVERSION" : ")+nomFichier);
ui.EditorView->editor->document()->setModified(false);
}

void MainWindow::ItemVersXml(QTreeWidgetItem *item,QDomDocument doc ,QDomElement parent)
{
QDomElement element;
element=doc.createElement("item");
element.setAttribute("algoitem",item->text(0));
element.setAttribute("code",item->data(0,Qt::UserRole).toString());
parent.appendChild(element);
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	for (int i = 0; i < nb_branches; i++) 
		{
		ItemVersXml(item->child(i),doc,element);
		}
	}
}

void MainWindow::SauverAlgo()
{
if (nomFichier=="sanstitre" ) SauverSousAlgo();
else
    {
    if (!modeNormal)
      {
      QString rep_analyse=EditeurVersArbre();
      if (rep_analyse!="ok")
	{
	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'enregistrer l'algorithme.\nErreur détectée : ")+rep_analyse);
	return;
	}
      ui.EditorView->editor->document()->setModified(false);
      }
    QFile fichier(nomFichier);
    if (!fichier.open(QIODevice::WriteOnly))
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Impossible d'enregistrer le fichier"));
	    return;
	    }
    int nb_branches=ui.treeWidget->topLevelItemCount();
    if (nb_branches==0) return ;
    QDomDocument doc;
    QDomProcessingInstruction instr =  doc.createProcessingInstruction("xml","version=\"1.0\" encoding=\"UTF-8\"");
    doc.appendChild(instr);
    QDomElement root=doc.createElement("Algo");
    doc.appendChild(root);
    QDomElement element;
    element=doc.createElement("description");
    element.setAttribute("texte",ui.textEditDescription->toPlainText());
    QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
    if (curItem) element.setAttribute("courant",curItem->text(0));
    else element.setAttribute("courant","");
    root.appendChild(element);

    element=doc.createElement("fonction");
    if (ui.checkBoxFonction->isChecked()) element.setAttribute("fctetat","actif");
    else element.setAttribute("fctetat","inactif");
    QString code= ui.lineEditFonction->text();
    if (!code.isEmpty()) element.setAttribute("fctcode",code);
    else element.setAttribute("fctcode","");
    root.appendChild(element);
    
    element=doc.createElement("F2");
    if (ui.checkBoxF2->isChecked()) element.setAttribute("F2etat","actif");
    else element.setAttribute("F2etat","inactif");
    QString para= ui.lineEditParametresF2->text();
    if (!para.isEmpty()) element.setAttribute("F2para",para);
    else element.setAttribute("F2para","");
    QString defaut= ui.lineEditDefautF2->text();
    if (!defaut.isEmpty()) element.setAttribute("F2defaut",defaut);
    else element.setAttribute("F2defaut","");
    QString lignes="";
    for (int i = 0; i < ui.listWidgetF2->count(); ++i) lignes+=ui.listWidgetF2->item(i)->data(Qt::UserRole).toString()+"#";
    element.setAttribute("F2lignes",lignes);
    root.appendChild(element);
    
    element=doc.createElement("repere");
    if (ui.checkBoxRepere->isChecked()) element.setAttribute("repetat","actif");
    else element.setAttribute("repetat","inactif");
    code="";
    QString part=ui.lineEditXmin->text();
    if (!part.isEmpty()) code+=part.remove("#")+"#";
    else code+="-10#";
    part=ui.lineEditXmax->text();
    if (!part.isEmpty()) code+=part.remove("#")+"#";
    else code+="10#";
    part=ui.lineEditYmin->text();
    if (!part.isEmpty()) code+=part.remove("#")+"#";
    else code+="-10#";
    part=ui.lineEditYmax->text();
    if (!part.isEmpty()) code+=part.remove("#")+"#";
    else code+="10#";
    part=ui.lineEditGradX->text();
    if (!part.isEmpty()) code+=part.remove("#")+"#";
    else code+="2#";
    part=ui.lineEditGradY->text();
    if (!part.isEmpty()) code+=part.remove("#");
    else code+="2";
    element.setAttribute("repcode",code);
    root.appendChild(element);

    for (int i = 0; i < nb_branches; i++) ItemVersXml(ui.treeWidget->topLevelItem(i),doc,root);
    QTextStream out (&fichier);
    doc.save(out,4);
    fichier.close();
    QFileInfo fi(nomFichier);
    dernierRepertoire=fi.absolutePath();
    estModifie=false;
    AddRecentFile(nomFichier);
    }
setWindowTitle(QString("AlgoBox "ALGOBOXVERSION" : ")+nomFichier);
}

void MainWindow::SauverSousAlgo()
{
QString nouveauFichier;
if (nomFichier=="sanstitre" ) 
	{
	nouveauFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),dernierRepertoire,QString::fromUtf8("Algorithme (*.alg)"));
	}
else 
	{
	nouveauFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),nomFichier,QString::fromUtf8("Algorithme (*.alg)"));
	}
if (!nouveauFichier.isEmpty()) 
    {
    if (!nouveauFichier.contains('.')) nouveauFichier += ".alg";
    nomFichier=nouveauFichier;
    SauverAlgo();
    }
// else
//     {
//     QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Impossible d'enregistrer le fichier"));
//     }
setWindowTitle(QString("AlgoBox "ALGOBOXVERSION" : ")+nomFichier);
}

void MainWindow::XmlVersItem(QTreeWidgetItem *parentItem, QDomElement element)
{
QFont titleFont = qApp->font();
titleFont.setBold(true);
QFont commentFont = qApp->font();
commentFont.setItalic(true);

if (!element.hasAttribute("algoitem") || !element.hasAttribute("code")) return;
QTreeWidgetItem *newItem;
QString texte=element.attribute("algoitem");
if (texte=="VARIABLES")
    {
    newItem=variablesItem;
    }
else if (texte=="DEBUT_ALGORITHME")
    {
    newItem=debutItem;
    }
else if (texte=="FIN_ALGORITHME")
    {
    newItem=finItem;
    }
else 
  {
  if (parentItem) newItem = new QTreeWidgetItem(parentItem);
  else newItem = new QTreeWidgetItem(ui.treeWidget);
  newItem->setText(0,element.attribute("algoitem"));
  newItem->setData(0,Qt::UserRole,QString(element.attribute("code")));
  if (newItem->text(0).startsWith("//",Qt::CaseInsensitive)) newItem->setFont(0,commentFont);
  }
ui.treeWidget->setCurrentItem(newItem);
ActualiserArbre();
QDomElement enfant = element.firstChildElement();
while (!enfant.isNull()) 
	{
	XmlVersItem(newItem,enfant);
	enfant = enfant.nextSiblingElement();
	}
}

void MainWindow::Ouvrir(QString nouveauFichier)
{
QDomDocument doc;
QFileInfo fi(nouveauFichier);
if (!fi.exists()) return;
dernierRepertoire=fi.absolutePath();
QFile fichier(nouveauFichier);
if (!fichier.open(QIODevice::ReadOnly)) 
	{
	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Impossible de lire le fichier"));
	return;
	}
if (!doc.setContent(&fichier)) 
	{
	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Impossible de lire le fichier"));
	fichier.close();
	return;
	}
QDomElement root = doc.documentElement();
if (root.tagName() != "Algo")
	{
	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Ce fichier ne correspond pas à un algorithme"));
	fichier.close();
	return;
	}
nomFichier=nouveauFichier;
InitOuvrir();
QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
QString courant="";
QDomElement enfant = root.firstChildElement();
while (!enfant.isNull()) 
	{
	if (enfant.hasAttribute("texte") && enfant.hasAttribute("courant"))
	    {
	    ui.textEditDescription->setPlainText(enfant.attribute("texte"));
	    courant=enfant.attribute("courant");
	    }
	else if (enfant.hasAttribute("fctetat") && enfant.hasAttribute("fctcode"))
	    {
	    if (enfant.attribute("fctetat")=="actif") ui.checkBoxFonction->setChecked(true);
	    else ui.checkBoxFonction->setChecked(false);
	    ui.lineEditFonction->setText(enfant.attribute("fctcode"));
	    }
	else if (enfant.hasAttribute("F2etat") && enfant.hasAttribute("F2para") && enfant.hasAttribute("F2defaut"))
	    {
	    if (enfant.attribute("F2etat")=="actif") ui.checkBoxF2->setChecked(true);
	    else ui.checkBoxF2->setChecked(false);
	    ui.lineEditParametresF2->setText(enfant.attribute("F2para"));
	    ui.lineEditDefautF2->setText(enfant.attribute("F2defaut"));
	    QStringList lignes=enfant.attribute("F2lignes").split("#");
	    QStringList tagList;
	    QListWidgetItem *ligne;
	    for (int i = 0; i < lignes.count(); ++i)
		{
		tagList= lignes.at(i).split("@");
		if (tagList.count()==2)
		  {
		  ligne=new QListWidgetItem(ui.listWidgetF2);
		  ligne->setText("SI ("+tagList.at(0)+") RENVOYER "+tagList.at(1));
		  ligne->setData(Qt::UserRole,lignes.at(i));
		  }
		}
	    }	
	else if (enfant.hasAttribute("repetat") && enfant.hasAttribute("repcode"))
	    {
	    if (enfant.attribute("repetat")=="actif") ui.checkBoxRepere->setChecked(true);
	    else ui.checkBoxRepere->setChecked(false);
	    QStringList parametres=enfant.attribute("repcode").split("#");
	    if (parametres.count()==6)
		{
		ui.lineEditXmin->setText(parametres.at(0));
		ui.lineEditXmax->setText(parametres.at(1));
		ui.lineEditYmin->setText(parametres.at(2));
		ui.lineEditYmax->setText(parametres.at(3));
		ui.lineEditGradX->setText(parametres.at(4));
		ui.lineEditGradY->setText(parametres.at(5));
		}
	    }
	else XmlVersItem(0,enfant);
	enfant = enfant.nextSiblingElement();
	}
fichier.close();
estVierge=false;
estModifie=false;
setWindowTitle(QString("AlgoBox "ALGOBOXVERSION" : ")+nomFichier);
AddRecentFile(nouveauFichier);
if (!courant.isEmpty()) 
    {
    QList<QTreeWidgetItem *> listItems=ui.treeWidget->findItems(courant,Qt::MatchRecursive,0);
    if (listItems.count()>0) ui.treeWidget->setCurrentItem(listItems.at(0));
    }
ActualiserArbre();
if (!modeNormal)
    {
    //disconnect(ui.EditorView->editor, SIGNAL(textChanged()), this, SLOT(ActualiserStatut()));
    ui.EditorView->editor->setPlainText(ArbreVersCodeTexte());
    ui.EditorView->editor->document()->setModified(false);
    //connect(ui.EditorView->editor, SIGNAL(textChanged()), this, SLOT(ActualiserStatut()));
    EffaceArbre();
    }
QApplication::restoreOverrideCursor();
}

void MainWindow::ChargerAlgo()
{
if (estModifie) 
	{
	switch(  QMessageBox::warning(this,"AlgoBox : ",
					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de charger un autre algorithme?"),
					QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
					0,
					2 ) )
		{
		case 0:
		SauverAlgo();
		break;
		case 1:
		break;
		case 2:
		default:
		return;
		break;
		}
	}
QString nouveauFichier = QFileDialog::getOpenFileName(this,QString::fromUtf8("Ouvrir Fichier"),dernierRepertoire,"Algorithme (*.alg)");
if (nouveauFichier.isEmpty()) return;
Ouvrir(nouveauFichier);
}

void MainWindow::OuvrirNouvelAlgo(QString nouveauFichier)
{
if (estModifie) 
	{
	switch(  QMessageBox::warning(this,"AlgoBox : ",
					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de charger un autre algorithme?"),
					QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
					0,
					2 ) )
		{
		case 0:
		SauverAlgo();
		break;
		case 1:
		break;
		case 2:
		default:
		return;
		break;
		}
	}
Ouvrir(nouveauFichier);
}

void MainWindow::ActualiserStatut()
{
estVierge=false;
estModifie=true;
setWindowTitle(QString("AlgoBox "ALGOBOXVERSION" ")+QString::fromUtf8("[modifié]")+" : "+nomFichier);
}

void MainWindow::NouveauStatut(bool m)
{
if (m) ActualiserStatut();
else
  {
//  estVierge=false;
  estModifie=false;
  setWindowTitle(QString("AlgoBox "ALGOBOXVERSION" : ")+nomFichier);
  }
}
//************************
void MainWindow::Aide()
{
#if defined( Q_WS_X11 )
#ifdef USB_VERSION
QString docfile=QCoreApplication::applicationDirPath() + "/ressources/aidealgobox.html";
#else
QString docfile=PREFIX"/share/algobox/aidealgobox.html";
#endif
#endif

#if defined( Q_WS_MACX )
QString docfile=QCoreApplication::applicationDirPath() + "/../Resources/aidealgobox.html";
#endif
#if defined(Q_WS_WIN)
QString docfile=QCoreApplication::applicationDirPath() + "/ressources/aidealgobox.html";
#endif
QFileInfo fic(docfile);
if (fic.exists() && fic.isReadable() )
	{
        if (browserWindow)
          {
          browserWindow->close();
          }
	browserWindow=new Browser("file:///"+docfile, 0);
	browserWindow->raise();
	browserWindow->show();
	//QDesktopServices::openUrl("file:///"+docfile);
	}
else { QMessageBox::warning( this,"Erreur",QString::fromUtf8("Fichier d'aide non trouvé"));}
}

void MainWindow::Tutoriel()
{
QDesktopServices::openUrl(QUrl("http://www.xm1math.net/algobox/tutoalgobox/index.html"));
}

void MainWindow::APropos()
{
AproposDialog *abDlg = new AproposDialog(this);
abDlg->exec();
}

void MainWindow::SetInterfaceFont()
{
X11FontDialog *xfdlg = new X11FontDialog(this);
int ft=xfdlg->ui.comboBoxFont->findText (x11fontfamily , Qt::MatchExactly);
xfdlg->ui.comboBoxFont->setCurrentIndex(ft);
xfdlg->ui.spinBoxSize->setValue(x11fontsize);
if (xfdlg->exec())
	{
	x11fontfamily=xfdlg->ui.comboBoxFont->currentText();
	x11fontsize=xfdlg->ui.spinBoxSize->value();
	if (x11fontsize<6) x11fontsize=6;
	QFont x11Font (x11fontfamily,x11fontsize);
	QApplication::setFont(x11Font);
	}
}

void MainWindow::CouleurConsole()
{
ConsoleDialog *dlg = new ConsoleDialog(this);
if (blackconsole) dlg->ui.radioButtonBlack->setChecked(true);
else dlg->ui.radioButtonWhite->setChecked(true);
if (dlg->exec())
	{
	blackconsole=dlg->ui.radioButtonBlack->isChecked();
	}
}

void MainWindow::fileOpenRecent()
{
QAction *action = qobject_cast<QAction *>(sender());
if (action) 
    {
    if (estModifie) 
	    {
	    switch(  QMessageBox::warning(this,"AlgoBox : ",
					    QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de charger un autre algorithme?"),
					    QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
					    0,
					    2 ) )
		    {
		    case 0:
		    SauverAlgo();
		    break;
		    case 1:
		    break;
		    case 2:
		    default:
		    return;
		    break;
		    }
	    }
    QString f=action->data().toString();
    Ouvrir(f);
    }
}

void MainWindow::AddRecentFile(const QString &f)
{
if (recentFilesList.contains(f)) return;

if (recentFilesList.count() < 5) recentFilesList.prepend(f);
else
	{
	recentFilesList.removeLast();
	recentFilesList.prepend(f);
	}
UpdateRecentFile();
}

void MainWindow::UpdateRecentFile()
{
for (int i=0; i < recentFilesList.count(); i++)
	{
        recentFileActs[i]->setText(recentFilesList.at(i));
        recentFileActs[i]->setData(recentFilesList.at(i));
        recentFileActs[i]->setVisible(true);
	}
for (int j = recentFilesList.count(); j < 5; ++j) recentFileActs[j]->setVisible(false);
}

void MainWindow::ChargerExemple()
{
#if defined( Q_WS_X11 )
#ifdef USB_VERSION
QString exempleRepertoire=QCoreApplication::applicationDirPath() + "/ressources";
#else
QString exempleRepertoire=PREFIX"/share/algobox";
#endif
#endif

#if defined( Q_WS_MACX )
QString exempleRepertoire=QCoreApplication::applicationDirPath() + "/../Resources";
#endif
#if defined(Q_WS_WIN)
QString exempleRepertoire=QCoreApplication::applicationDirPath() + "/ressources";
#endif
QString f = QFileDialog::getOpenFileName(this,QString::fromUtf8("Ouvrir Fichier"),exempleRepertoire,"Algorithme (*.alg)");
if (!f.isEmpty()) Ouvrir(f);
}
//**********************************
void MainWindow::EditCopier()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem) return;
  clipboardItem=curItem->clone();
  }
else
  {
  ui.EditorView->editor->copy(); 
  }
}

void MainWindow::EditColler()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  QTreeWidgetItem *newItem=clipboardItem->clone();
  int idx;
  if (!curItem) return;
  if (clipboardItem) 
    {
    if (!clipboardItem->text(0).isEmpty()) 
      {
      if (curItem->parent()) 
	{
	idx = curItem->parent()->indexOfChild(curItem);
	curItem->parent()->insertChild(idx,newItem);
	ui.treeWidget->setCurrentItem(newItem);
	ExpandBranche(newItem);
	delete curItem;
	}
      }
    }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->paste(); 
  }
}

void MainWindow::EditCouper()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem) return;
  clipboardItem=curItem->clone();
  SupprimerLigne();
  }
else
  {
  ui.EditorView->editor->cut(); 
  }
}
//**********************************
void MainWindow::ToggleCadrePresentation()
{
if (!afficheCadrePresentation)
	{
	afficheCadrePresentation=true;
	ui.groupBoxDescription->show();
	}
else 
  	{
	afficheCadrePresentation=false;
	ui.groupBoxDescription->hide();
	}
ToggleAct->setChecked(afficheCadrePresentation);
}

void MainWindow::ActualiserMode()
{
QAction *action = qobject_cast<QAction *>(sender());
QString choix=action->text();
int query=1;
if (!estVierge)
  {
  query =QMessageBox::warning(this, "AlgoBox", QString::fromUtf8("Le changement de mode d'édition n'est possible qu'à partir d'un nouvel algorithme vierge."),QString::fromUtf8("Créer un nouvel algorithme vierge"), QString::fromUtf8("Abandonner") );
  if (query==1) return;
  NouvelAlgo();
  }
if (choix=="Mode &normal")
	{
	modeNormal=true;
	ui.stackedWidget->setCurrentWidget(ui.page_arbre);
	}
else 
  	{
	modeNormal=false;
	ui.stackedWidget->setCurrentWidget(ui.page_editeur);
	}
ActualiserArbre();
}
//**********************************
void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
if (event->mimeData()->hasFormat("text/uri-list")) event->acceptProposedAction();
}

void MainWindow::dropEvent(QDropEvent *event)
{
QRegExp rx("file://(.*\\.alg)");
QList<QUrl> uris=event->mimeData()->urls();
QString uri;
for (int i = 0; i < uris.size(); ++i)
	{
	uri=uris.at(i).toString();
	if (rx.exactMatch(uri)) OuvrirNouvelAlgo(rx.cap(1));
	}
event->acceptProposedAction();
}
//**********************************
// void MainWindow::ImporterCodeTexte()
// {
// if (estModifie) 
// 	{
// 	switch(  QMessageBox::warning(this,"AlgoBox : ",
// 					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de charger un autre algorithme?"),
// 					QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
// 					0,
// 					2 ) )
// 		{
// 		case 0:
// 		SauverAlgo();
// 		break;
// 		case 1:
// 		break;
// 		case 2:
// 		default:
// 		return;
// 		break;
// 		}
// 	}
// QString nouveauFichier = QFileDialog::getOpenFileName(this,QString::fromUtf8("Ouvrir Fichier"),dernierRepertoire,"Code Texte (*.txt)");
// if (nouveauFichier.isEmpty()) return;
// QFileInfo fi(nouveauFichier);
// if (!fi.exists()) return;
// dernierRepertoire=fi.absolutePath();
// QFile fichier(nouveauFichier);
// if (!fichier.open(QIODevice::ReadOnly)) 
// 	{
// 	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Impossible de lire le fichier"));
// 	return;
// 	}
// 	
// Init();
// 
// QTextCodec *codec = QTextCodec::codecForName("UTF-8");
// QStringList liste_Lignes;
// QTextStream in(&fichier);
// in.setCodec(codec);
// ui.EditorView->editor->setPlainText( in.readAll());
// ui.stackedWidget->setCurrentWidget(ui.page_editeur);
// }

QString MainWindow::EditeurVersArbre()
{
EffaceArbre();
QString texteEditeur=ui.EditorView->editor->toPlainText();
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QStringList liste_Lignes, num_Lignes;
QTextStream in(&texteEditeur,QIODevice::ReadOnly);
in.setCodec(codec);

bool erreur=false;
QString message_erreur="ok";
QString ligne,nomvariable,type,rang,message,contenu,x,y,couleur,xdep,ydep,xfin,yfin,condition,debut,fin,code;
int i=0;
QRegExp rxnumline("^([0-9]+)");
QRegExp rxvar("(.*)\\s+EST_DU_TYPE\\s+(.*)",Qt::CaseInsensitive);
QRegExp rxfixe("^(VARIABLES|DEBUT_ALGORITHME|FIN_ALGORITHME)",Qt::CaseInsensitive);
QRegExp rxpause("^PAUSE",Qt::CaseInsensitive);
QRegExp rxcommentaire("^//(.*)");
QRegExp rxlire("^LIRE\\s+(.+)",Qt::CaseInsensitive);
QRegExp rxliste("\\[(.*)\\]");
QRegExp rxaffichmessagenl("^AFFICHER\\*\\s+\"(.*)\"",Qt::CaseInsensitive);
QRegExp rxaffichmessage("^AFFICHER\\s+\"(.*)\"",Qt::CaseInsensitive);
QRegExp rxaffichvariablenl("^AFFICHER\\*\\s+(.*)",Qt::CaseInsensitive);
QRegExp rxaffichvariable("^AFFICHER\\s+(.*)",Qt::CaseInsensitive);
QRegExp rxaffectation("^(.*)\\s+PREND_LA_VALEUR\\s+(.*)",Qt::CaseInsensitive);
QRegExp rxpointcouleur("^TRACER_POINT_(Rouge|Vert|Bleu)\\s+\\((.*),(.*)\\)",Qt::CaseInsensitive);
QRegExp rxpoint("^TRACER_POINT\\s+\\((.*),(.*)\\)",Qt::CaseInsensitive);
QRegExp rxsegmentcouleur("^TRACER_SEGMENT_(Rouge|Vert|Bleu)\\s+\\((.*),(.*)\\)\\s*\\->\\s*\\((.*),(.*)\\)",Qt::CaseInsensitive);
QRegExp rxsegment("^TRACER_SEGMENT\\s+\\((.*),(.*)\\)\\s*\\->\\s*\\((.*),(.*)\\)",Qt::CaseInsensitive);
QRegExp rxtantque("^TANT_QUE\\s+\\((.*)\\)\\s+FAIRE",Qt::CaseInsensitive);
QRegExp rxdebuttantque("^DEBUT_TANT_QUE",Qt::CaseInsensitive);
QRegExp rxfintantque("^FIN_TANT_QUE",Qt::CaseInsensitive);
QRegExp rxpour("^POUR\\s+(.*)\\s+ALLANT_DE\\s+(.*)\\s+A\\s+(.*)",Qt::CaseInsensitive);
QRegExp rxdebutpour("^DEBUT_POUR",Qt::CaseInsensitive);
QRegExp rxfinpour("^FIN_POUR",Qt::CaseInsensitive);
QRegExp rxsi("^SI\\s+\\((.*)\\)\\s+ALORS",Qt::CaseInsensitive);
QRegExp rxdebutsi("^DEBUT_SI",Qt::CaseInsensitive);
QRegExp rxfinsi("^FIN_SI",Qt::CaseInsensitive);
QRegExp rxsinon("^SINON",Qt::CaseInsensitive);
QRegExp rxdebutsinon("^DEBUT_SINON",Qt::CaseInsensitive);
QRegExp rxfinsinon("^FIN_SINON",Qt::CaseInsensitive);
while (!in.atEnd()) 
  {
  ligne=in.readLine();
  i++;
  ligne=ligne.remove(rxnumline);
  ligne=ligne.trimmed();
  if (ligne.contains(QString(0x2022)))
    {
    erreur=true;
    message_erreur=QString::fromUtf8("Le code contient un champ • non complété")+" (ligne "+QString::number(i)+")";
    break;    
    }
  if (rxvar.indexIn(ligne)>-1)
    {
    nomvariable=rxvar.cap(1);
    type=rxvar.cap(2);
    nomvariable=FiltreNomVariable(nomvariable);
    if (NomInterdit(nomvariable))
	{
	erreur=true;
	message_erreur=QString::fromUtf8("Nom de variable interdit")+" (ligne "+QString::number(i)+")";
	break;
	}
    if ((type!="NOMBRE") && (type!="CHAINE") && (type!="LISTE"))
	{
	erreur=true;
	message_erreur=QString::fromUtf8("Type de variable non reconnu")+" (ligne "+QString::number(i)+")";
	break;
	}
    QTreeWidgetItem *newItem =new QTreeWidgetItem(variablesItem);
    newItem->setText(0,nomvariable+" EST_DU_TYPE "+type);
    code="1#"+type+"#"+nomvariable;
    newItem->setData(0,Qt::UserRole,QString(code));
    }
  else if ((rxfixe.indexIn(ligne)<0) && (!ligne.isEmpty())) 
    {
    liste_Lignes.append(ligne);
    num_Lignes.append(QString::number(i));
    }
  }
if (erreur)
  {
  EffaceArbre();
  ui.EditorView->editor->setCursorPosition(i-1 , 0);
  return message_erreur;
  }
ActualiserVariables();
ui.treeWidget->setCurrentItem(debutItem);
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
QTreeWidgetItem *newItem;
QFont commentFont = qApp->font();
commentFont.setItalic(true);
for (int numligne = 0; numligne < liste_Lignes.count(); numligne++)
  {
  ligne=liste_Lignes.at(numligne);
  if (rxpause.indexIn(ligne)>-1) //PAUSE
    {
    NouvelleLigne(ui.treeWidget->currentItem());
    curItem = ui.treeWidget->currentItem();
    if (curItem && curItem->parent())
      {
      code="18#pause";
      curItem->setText(0,QString::fromUtf8("PAUSE"));
      curItem->setData(0,Qt::UserRole,QString(code));
      ui.treeWidget->setCurrentItem(curItem);
      ActualiserArbre();
      }
    }
    else if (rxcommentaire.indexIn(ligne)>-1) //COMMENTAIRE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	QString commentaire=rxcommentaire.cap(1);
	commentaire.remove("#");
	if (!commentaire.isEmpty())
	  {
	  curItem->setText(0,"//"+commentaire);
	  code="19#"+commentaire;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  curItem->setFont(0,commentFont);
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserArbre();
	  }
	}  
      }
    else if (rxlire.indexIn(ligne)>-1) //LIRE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	nomvariable=rxlire.cap(1);
	rang="";
	if (rxliste.indexIn(nomvariable)>-1) 
	  {
	  rang=rxliste.cap(1);
	  nomvariable.remove(rxliste);
	  }
	if (!nomvariable.isEmpty())
	    {
            if (theModel->checkVariableExists(nomvariable))
		{
                type=""/*ListeTypesVariables.at(i)*/;
		if ((type=="LISTE") && (!rang.isEmpty())) 
		    {
		    curItem->setText(0,QString::fromUtf8("LIRE ")+nomvariable+"["+rang+"]");
		    code="2#"+nomvariable+"#"+rang;
		    }
		else 
		    {
		    curItem->setText(0,QString::fromUtf8("LIRE ")+nomvariable);
		    code="2#"+nomvariable+"#pasliste";
		    }
		curItem->setData(0,Qt::UserRole,QString(code));
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	  else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Nom de variable non reconnu.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxaffichmessagenl.indexIn(ligne)>-1) //AFFICHER MESSAGE NL
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	message=rxaffichmessagenl.cap(1);
	message.remove("#");
	message.remove("\"");
	code="";
	if (!message.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
	    code="4#"+message+"#1";
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Message vide.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }	  
    else if (rxaffichmessage.indexIn(ligne)>-1) //AFFICHER MESSAGE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	message=rxaffichmessage.cap(1);
	message.remove("#");
	message.remove("\"");
	code="";
	if (!message.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
	    code="4#"+message+"#0";
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Message vide.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxaffichvariablenl.indexIn(ligne)>-1) //AFFICHER VARIABLE NL
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	nomvariable=rxaffichvariablenl.cap(1);
	rang="";
	if (rxliste.indexIn(nomvariable)>-1) 
	  {
	  rang=rxliste.cap(1);
	  nomvariable.remove(rxliste);
	  }
	code="";
	if (!nomvariable.isEmpty())
	    {
            if (theModel->checkVariableExists(nomvariable))
		{
                QString type=""/*ListeTypesVariables.at(i)*/;
		code="3#"+nomvariable+"#1";
		if ((type=="LISTE") && (!rang.isEmpty())) 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable+"["+rang+"]");
		    code+="#"+rang;
		    }
		else 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable);
		    code+="#pasliste";
		    }
		curItem->setData(0,Qt::UserRole,QString(code));
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	  else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Nom de variable non reconnu.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }	  
    else if (rxaffichvariable.indexIn(ligne)>-1) //AFFICHER VARIABLE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	nomvariable=rxaffichvariable.cap(1);
	rang="";
	if (rxliste.indexIn(nomvariable)>-1) 
	  {
	  rang=rxliste.cap(1);
	  nomvariable.remove(rxliste);
	  }
	code="";
	if (!nomvariable.isEmpty())
	    {
            if (theModel->checkVariableExists(nomvariable))
		{
                QString type=""/*ListeTypesVariables.at(i)*/;
		code="3#"+nomvariable+"#0";
		if ((type=="LISTE") && (!rang.isEmpty())) 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable+"["+rang+"]");
		    code+="#"+rang;
		    }
		else 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable);
		    code+="#pasliste";
		    }
		curItem->setData(0,Qt::UserRole,QString(code));
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	  else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Nom de variable non reconnu.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }	  
    else if (rxaffectation.indexIn(ligne)>-1) //AFFECTATION
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{   
        if (!theModel->variablesExists())
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Aucune variable n'a encore été définie.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	nomvariable=rxaffectation.cap(1);
	contenu=rxaffectation.cap(2);
	rang="";
	if (rxliste.indexIn(nomvariable)>-1) 
	  {
	  rang=rxliste.cap(1);
	  nomvariable.remove(rxliste);
	  }
	contenu.remove("#");
	code="";
	if (!nomvariable.isEmpty())
	    {
	    if (!contenu.isEmpty())
		{
                if (theModel->checkVariableExists(nomvariable))
		    {
                    type=""/*ListeTypesVariables.at(i)*/;
		    code="5#"+nomvariable+"#"+contenu;
		    if ((type=="LISTE") && (!rang.isEmpty())) 
			{
			curItem->setText(0,nomvariable+"["+rang+"]"+QString::fromUtf8(" PREND_LA_VALEUR ")+contenu);
			code+="#"+rang;
			}
		    else 
			{
			curItem->setText(0,nomvariable+QString::fromUtf8(" PREND_LA_VALEUR ")+contenu);
			code+="#pasliste";
			}
		    curItem->setData(0,Qt::UserRole,QString(code));
		    }
		else
		    {
		    erreur=true;
		    message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		    break;
		    }
		ui.treeWidget->setCurrentItem(curItem);
		ActualiserArbre();
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Affectation vide")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de variable définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }	  
	}
      }
    else if (rxpointcouleur.indexIn(ligne)>-1) //POINT COULEUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	couleur=rxpointcouleur.cap(1);
	x=rxpointcouleur.cap(2);
	y=rxpointcouleur.cap(3);
	couleur.remove("#");
	x.remove("#");
	y.remove("#");
	if ((!x.isEmpty()) && (!y.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_POINT (")+x+","+y+")");
	    code="50#"+x+"#"+y+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxpoint.indexIn(ligne)>-1) //POINT
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{   
	x=rxpoint.cap(1);
	y=rxpoint.cap(2);
	couleur="Rouge"; 
	x.remove("#");
	y.remove("#");
	if ((!x.isEmpty()) && (!y.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_POINT (")+x+","+y+")");
	    code="50#"+x+"#"+y+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxsegmentcouleur.indexIn(ligne)>-1) //SEGMENT COULEUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	couleur=rxsegmentcouleur.cap(1);
	xdep=rxsegmentcouleur.cap(2);
	ydep=rxsegmentcouleur.cap(3);
	xfin=rxsegmentcouleur.cap(4);
	yfin=rxsegmentcouleur.cap(5);
	couleur.remove("#");
	xdep.remove("#");
	ydep.remove("#");
	xfin.remove("#");
	yfin.remove("#");
	if ((!xdep.isEmpty()) && (!ydep.isEmpty()) && (!xfin.isEmpty()) && (!yfin.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_SEGMENT (")+xdep+","+ydep+")->("+xfin+","+yfin+")");
	    code="51#"+xdep+"#"+ydep+"#"+xfin+"#"+yfin+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }      
	}
      }
    else if (rxsegment.indexIn(ligne)>-1) //SEGMENT
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{         
	xdep=rxsegment.cap(1);
	ydep=rxsegment.cap(2);
	xfin=rxsegment.cap(3);
	yfin=rxsegment.cap(4);
	couleur="Rouge"; 
	xdep.remove("#");
	ydep.remove("#");
	xfin.remove("#");
	yfin.remove("#");
	if ((!xdep.isEmpty()) && (!ydep.isEmpty()) && (!xfin.isEmpty()) && (!yfin.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_SEGMENT (")+xdep+","+ydep+")->("+xfin+","+yfin+")");
	    code="51#"+xdep+"#"+ydep+"#"+xfin+"#"+yfin+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }      
	}
      }
    else if (rxtantque.indexIn(ligne)>-1) //TANT_QUE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	condition=rxtantque.cap(1);
	condition.remove("#");
	code="";
	if (!condition.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("TANT_QUE (")+condition+") "+QString::fromUtf8("FAIRE"));
	    code="15#"+condition;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de condition définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxdebuttantque.indexIn(ligne)>-1) //DEBUT_TANT_QUE
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_TANT_QUE"));
	newItem->setData(0,Qt::UserRole,QString("16#debuttantque"));
	ui.treeWidget->setItemExpanded (curItem,true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfintantque.indexIn(ligne)>-1) //FIN_TANT_QUE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	    curItem->setText(0,QString::fromUtf8("FIN_TANT_QUE"));
	    curItem->setData(0,Qt::UserRole,QString("17#fintantque"));
	    ActualiserArbre();
	}
      }
    else if (rxpour.indexIn(ligne)>-1) //POUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	nomvariable=rxpour.cap(1);
	debut=rxpour.cap(2);
	debut.remove("#");
	fin=rxpour.cap(3);
	fin.remove("#");
	code="";
	if (!nomvariable.isEmpty())
	    {
	    if (!debut.isEmpty() && !fin.isEmpty())
		{
                if (theModel->checkVariableExists(nomvariable))
		    {
		    curItem->setText(0,QString::fromUtf8("POUR ")+nomvariable+" ALLANT_DE "+debut+ " A "+fin);
		    code="12#"+nomvariable+"#"+debut+"#"+fin;
		    curItem->setData(0,Qt::UserRole,QString(code));
		    }
		else
		    {
		    erreur=true;
		    message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		    break;
		    }
		ActualiserArbre();
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Les valeurs minimales et maximales du compteur ne sont pas définies")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de variable définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxdebutpour.indexIn(ligne)>-1) //DEBUT_POUR
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_POUR"));
	newItem->setData(0,Qt::UserRole,QString("13#debutpour"));
	ui.treeWidget->setItemExpanded (curItem,true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfinpour.indexIn(ligne)>-1) //FIN_POUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	    curItem->setText(0,QString::fromUtf8("FIN_POUR"));
	    curItem->setData(0,Qt::UserRole,QString("14#finpour"));
	    ActualiserArbre();
	}
      }
    else if (rxsinon.indexIn(ligne)>-1) //SINON
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	  newItem = new QTreeWidgetItem(curItem->parent(), curItem);
	  newItem->setText(0,QString::fromUtf8("SINON"));
	  code="9#sinon";
	  newItem->setData(0,Qt::UserRole,QString(code));
	  ui.treeWidget->setCurrentItem(newItem);
	  ActualiserArbre();
	}
      }
    else if (rxdebutsinon.indexIn(ligne)>-1) //DEBUT_SINON
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_SINON"));
	newItem->setData(0,Qt::UserRole,QString("10#debutsinon"));
	ui.treeWidget->setItemExpanded (curItem,true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfinsinon.indexIn(ligne)>-1) //FIN_SINON
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	curItem->setText(0,QString::fromUtf8("FIN_SINON"));
	curItem->setData(0,Qt::UserRole,QString("11#finsinon"));
	ActualiserArbre();
	}
      }      
    else if (rxsi.indexIn(ligne)>-1) //SI
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	condition=rxsi.cap(1);
	condition.remove("#");
	code="";
	if (!condition.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("SI (")+condition+") "+QString::fromUtf8("ALORS"));
	    code="6#"+condition;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de condition définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxdebutsi.indexIn(ligne)>-1) //DEBUT_SI
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_SI"));
	newItem->setData(0,Qt::UserRole,QString("7#debutsi"));
	ui.treeWidget->setItemExpanded (curItem,true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfinsi.indexIn(ligne)>-1) //FIN_SI
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	curItem->setText(0,QString::fromUtf8("FIN_SI"));
	curItem->setData(0,Qt::UserRole,QString("8#finsi"));
	ActualiserArbre();
	}
      }
    else
      {
      erreur=true;
      message_erreur=QString::fromUtf8("Instruction non reconnue")+" (ligne "+num_Lignes.at(numligne)+")";
      break;      
      }
  }
QRegExp rxerreurligne("ligne ([0-9]+)");
int erreurligne=1;
if (erreur)
  {
  EffaceArbre();
  if (rxerreurligne.indexIn(message_erreur)>-1) erreurligne=rxerreurligne.cap(1).toInt();
  ui.EditorView->editor->setCursorPosition(erreurligne-1 , 0);
  }
return message_erreur;
}

QString MainWindow::ArbreVersCodeTexte()
{
QString code="";
int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches>0)
    {
    indent=0;
    for (int i = 0; i < nb_branches; i++) 
	{
	code+=AlgoNoeudCode(ui.treeWidget->topLevelItem(i));
	}
    }
return code;
}

void MainWindow::VerifierCodeTexte()
{
QTreeWidgetItem *newItem;
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible de vérifier l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  VerifDialog *verifDlg = new VerifDialog(this);
  int nb_branches=ui.treeWidget->topLevelItemCount();
  if (nb_branches>0)
    {
    for (int i = 0; i < nb_branches; i++) 
	{
	newItem=ui.treeWidget->topLevelItem(i)->clone();
	verifDlg->ui.treeWidget->addTopLevelItem(newItem);
	verifDlg->ExpandBranche(newItem);
	}
    }
  verifDlg->exec();
  }  
}

void MainWindow::ExpandBranche(QTreeWidgetItem *item)
{
ui.treeWidget->expandItem(item);
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	for (int i = 0; i < nb_branches; i++) 
		{
		ExpandBranche(item->child(i));
		}
	}
}

void MainWindow::InsertOperation(QListWidgetItem *item)
{
if (item && item->font().bold())
    {
    QString role=item->data(Qt::UserRole).toString();
    QStringList tagList= role.split("#");
    int pos=ui.lineEditFonction->cursorPosition();
    int dx=tagList.at(1).toInt();
    ui.lineEditFonction->insert(tagList.at(0));
    ui.lineEditFonction->setCursorPosition(pos+dx);
    ui.lineEditFonction->setFocus();
    }
}

void MainWindow::AjouterF2()
{
QListWidgetItem *ligne;
ligne=new QListWidgetItem(ui.listWidgetF2);
ligne->setText("SI ("+ui.lineEditConditionF2->text()+") RENVOYER "+ui.lineEditRetourF2->text());
ligne->setData(Qt::UserRole,ui.lineEditConditionF2->text()+"@"+ui.lineEditRetourF2->text());
if (ui.listWidgetF2->count()>0)
	{
	ui.listWidgetF2->setCurrentItem(ui.listWidgetF2->item(ui.listWidgetF2->count()-1));
	ui.listWidgetF2->setItemSelected(ui.listWidgetF2->currentItem(), true);
	}
ui.lineEditConditionF2->setText("");
ui.lineEditRetourF2->setText("");
ActualiserStatut();
}

void MainWindow::HautF2()
{
int current=ui.listWidgetF2->currentRow();
if (current<=0) return;
QListWidgetItem *item=ui.listWidgetF2->item(current)->clone();
delete ui.listWidgetF2->item(current);
ui.listWidgetF2->insertItem(current-1,item);
ui.listWidgetF2->setCurrentItem(ui.listWidgetF2->item(current-1));
ui.listWidgetF2->setItemSelected(ui.listWidgetF2->item(current-1),true);
ActualiserStatut();
}

void MainWindow::BasF2()
{
int current=ui.listWidgetF2->currentRow();
if (current>=ui.listWidgetF2->count()-1 || current<0) return;
QListWidgetItem *item=ui.listWidgetF2->item(current)->clone();
delete ui.listWidgetF2->item(current);
ui.listWidgetF2->insertItem(current+1,item);
ui.listWidgetF2->setCurrentItem(ui.listWidgetF2->item(current+1));
ui.listWidgetF2->setItemSelected(ui.listWidgetF2->item(current+1),true);
ActualiserStatut();
}

void MainWindow::SupprimerF2()
{
int current=ui.listWidgetF2->currentRow();
if (current<0) return;
delete ui.listWidgetF2->currentItem();
if (current==ui.listWidgetF2->count())
	{
	ui.listWidgetF2->setCurrentItem(ui.listWidgetF2->item(current-1));
	ui.listWidgetF2->setItemSelected(ui.listWidgetF2->item(current-1),true);
	}
else
	{
	ui.listWidgetF2->setCurrentItem(ui.listWidgetF2->item(current));
	ui.listWidgetF2->setItemSelected(ui.listWidgetF2->item(current),true);
	}
ActualiserStatut();
}

void MainWindow::ModifierLigneF2(QListWidgetItem *item)
{
if (item)
  {
  QString role,condition,commande;
  QStringList tagList;
  role=item->data(Qt::UserRole).toString();
  tagList= role.split("@");
  if (tagList.count()==2) 
      {
      condition=tagList.at(0);
      commande=tagList.at(1);
      ModifierLigneDialog *dlg = new ModifierLigneDialog(this);
      dlg->ui.lineEditConditionF2->setText(condition);
      dlg->ui.lineEditRetourF2->setText(commande);
      if (dlg->exec())
	  {
	  item->setText("SI ("+dlg->ui.lineEditConditionF2->text()+") RENVOYER "+dlg->ui.lineEditRetourF2->text());
	  item->setData(Qt::UserRole,dlg->ui.lineEditConditionF2->text()+"@"+dlg->ui.lineEditRetourF2->text());
	  }
      }
  }
ActualiserStatut();
}
