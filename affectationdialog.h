/***************************************************************************
 *   copyright       : (C) 2009-2011 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef AFFECTATIONDIALOG_H
#define AFFECTATIONDIALOG_H

#include "ui_affectationdialog.h"
#include <QListWidget>
#include <QListWidgetItem>
#include <QFont>

class AffectationDialog : public QDialog  {
   Q_OBJECT
public:
	AffectationDialog(QWidget *parent=0,QString variables="", QString types="");
	~AffectationDialog();
	Ui::AffectationDialog ui;
private :
QStringList listeVariables, listeTypes, listeVarNombre, listeVarListe, listeVarChaine;
QFont fontCommande, fontCommentaire;
private slots:
void accept();
void ActualiserWidget(int index);
void InsertVariable(QListWidgetItem *item);
void InsertOperation(QListWidgetItem *item);
};


#endif
