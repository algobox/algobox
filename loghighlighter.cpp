/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QtGui>

#include "loghighlighter.h"

LogHighlighter::LogHighlighter(QTextDocument *parent, bool blackschema)
    : QSyntaxHighlighter(parent)
{
MessageFormat.setFontWeight(QFont::Bold);
PasAPasFormat.setFontWeight(QFont::Bold);
ConditionFormat.setFontWeight(QFont::Bold);

if (blackschema)
  {
  ConditionFormat.setForeground(QColor("#C06DBB"));
  MessageFormat.setForeground(QColor("#D7B54F"));
  PasAPasFormat.setForeground(QColor("#AACC00"));
  }
else
  {
  ConditionFormat.setForeground(QColor(0x00,0x80, 0x00));
  MessageFormat.setForeground(QColor(0x00, 0x00, 0xCC));
  PasAPasFormat.setForeground(QColor(0x80, 0x00, 0x00));
  }
}

void LogHighlighter::highlightBlock(const QString &text)
{

QString t;
QRegExp rxMessage("\\*\\*\\*(.*)\\*\\*\\*");
QRegExp rxPasAPas("#([0-9eE\\.\\-]+)\\s*(Nombres/chaines |Liste )(.*)\\(ligne\\s*([0-9eE\\.\\-]+)\\) -> ");
QRegExp rxCondition("(La condition|Entr|Sortie)");
if ((rxMessage.indexIn(text)!=-1) || text.startsWith("Trac") )
	{
	setFormat(0, text.length(),MessageFormat);
	}
else if (rxPasAPas.indexIn(text)!=-1) 
	{
	t=text;
	t.remove(rxPasAPas);
	setFormat(0, text.length()-t.length(),PasAPasFormat);
	}
else if (rxCondition.indexIn(text)!=-1) 
	{
	setFormat(0, text.length(),ConditionFormat);
	}
}
