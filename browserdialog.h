/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef BROWSERDIALOG_H
#define BROWSERDIALOG_H

#include "ui_browserdialog.h"
#include "algowebpage.h"
#include "loghighlighter.h"

class BrowserDialog : public QDialog  {
   Q_OBJECT
public:
	BrowserDialog(QWidget *parent=0, QString fichier="", bool blackconsole=true);
	~BrowserDialog();
	Ui::BrowserDialog ui;
	AlgoWebPage* page;
public slots:
void populateJavaScriptWindowObject();
void scriptAfficher(QString msg,bool newline);
void scriptAfficherVariables(QString msg,bool newline);
void scriptLaunched();
void LancerAlgo();
void ExecuterAlgo();
void ContinuerPasAPas();
void StopperPasAPas();
void scriptFinished();
void scriptEffacer();
void scriptDebutPause();
void scriptFinPause();
private slots:
void closeEvent(QCloseEvent *e);
void accept();
private:
LogHighlighter *highlighter, *highlighterPas;
};

#endif
