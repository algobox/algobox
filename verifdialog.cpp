/***************************************************************************
 *   copyright       : (C) 2006-2009 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "verifdialog.h"


VerifDialog::VerifDialog(QWidget *parent)
    :QDialog( parent)
{
ui.setupUi(this);
setModal(true);
ui.treeWidget->setColumnCount(1);
ui.treeWidget->header()->hide();
ui.treeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
ui.treeWidget->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
//ui.treeWidget->header()->setResizeMode(0, QHeaderView::Stretch);
ui.treeWidget->header()->setResizeMode(0, QHeaderView::ResizeToContents);
ui.treeWidget->header()->setStretchLastSection(false);
}

VerifDialog::~VerifDialog(){
}

void VerifDialog::ExpandBranche(QTreeWidgetItem *item)
{
ui.treeWidget->expandItem(item);
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	for (int i = 0; i < nb_branches; i++) 
		{
		ExpandBranche(item->child(i));
		}
	}
}
