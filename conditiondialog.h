/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONDITIONDIALOG_H
#define CONDITIONDIALOG_H

#include "ui_conditiondialog.h"

class ConditionDialog : public QDialog  {
   Q_OBJECT
public:
	ConditionDialog(QWidget *parent=0,QString variables="", QString types="");
	~ConditionDialog();
	Ui::ConditionDialog ui;
private :
QStringList listeVariables, listeTypes, listeVarNombre, listeVarListe, listeVarChaine;
private slots:
void InsertVariable(QListWidgetItem *item);
void InsertOperation(QListWidgetItem *item);
};


#endif
