/***************************************************************************
 *   copyright       : (C) 2009-2010 by Pascal Brachet                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef POURDIALOG_H
#define POURDIALOG_H

#include "ui_pourdialog.h"
#include <QListWidget>
#include <QListWidgetItem>

class PourDialog : public QDialog  {
   Q_OBJECT
public:
	PourDialog(QWidget *parent=0,QString variables="");
	~PourDialog();
	Ui::PourDialog ui;
private :
QStringList listeVariables;
private slots:
void ActualiserWidget(int index);
};


#endif
